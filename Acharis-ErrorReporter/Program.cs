﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Acharis.Core_Elements;
using Acharis_ErrorReportingBase;

namespace AcharisErrorReporter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string id = args[0];
                SerializableException x = ExceptionLoader.GetException(id);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new ErrorReport(x));
            }
        }
    }
}
