﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Timers;
using System.Diagnostics;
using Acharis_ErrorReportingBase;

namespace Acharis.Core_Elements
{
    public partial class ErrorReport : Form
    {
        private const string sendAddress = "graymond2007@gmail.com";
        private SerializableException e;
        
        public ErrorReport()
        {
            InitializeComponent();
            eTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
        }
        public ErrorReport(SerializableException e) : this()
        {
            this.e = e;
            eTimer.Interval = 1000;
            eTimer.Stop();

            textBox1.Text = e.ExceptionType + " occured.";
            richTextBox2.Text = "Stack Trace - exception traced to: \r\n" + e.StackTrace;
            richTextBox2.ReadOnly = true;
            richTextBox2.ScrollBars = RichTextBoxScrollBars.Both;
            richTextBox2.WordWrap = false;
            textBox1.ReadOnly = true;
            textBox1.TextAlign = HorizontalAlignment.Center;
            this.FormClosing += new FormClosingEventHandler(ErrorReport_FormClosing);
            Alive = true;

            //Application.DoEvents();
        }
        private bool Alive { get; set; }
        private void ErrorReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            Alive = false;
        }
        private System.Timers.Timer eTimer = new System.Timers.Timer();
        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }
        private void eThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Text = "Sent!";
            button1.Enabled = true;
            eTimer.Interval = 1000;
            eTimer.Start();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            userMessage = richTextBox1.Text;
            BackgroundWorker eThread = new BackgroundWorker();
            eThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(eThread_RunWorkerCompleted);
            eThread.DoWork += new DoWorkEventHandler(eThread_DoWork);
            eThread.RunWorkerAsync();
            button1.Enabled = false;
            button1.Text = "Sending...";
        }
        string userMessage = "";
        public void eThread_DoWork(object sender, DoWorkEventArgs eve)
        {
            BackgroundWorker send = sender as BackgroundWorker;
            try
            {
                DateTime inGMT8 = e.TimeStamp.ToUniversalTime().AddHours(8);
                string text = "<b><font size=4>" + e.ExceptionType + " occured</font></b>" +
                    "<br><font size = 4>At " + inGMT8.ToShortDateString() + " " + inGMT8.ToShortTimeString() + " on " + WindowsIdentity.GetCurrent().Name.ToString() + ".</font>" +
                    "<br><br><b>Message</b><br> " + e.Message + ((userMessage.Length > 1) ?
                    "<br><br><b>User Message</b><br> " + userMessage : "") +
                    "<br><br><b>Stack Trace</b><br>" + e.StackTrace +
                    "<br><br><br><b><font size=1>This is an automated e-mail. Please do not respond.</font></b>";

                string subject = e.GetType() + " From " + WindowsIdentity.GetCurrent().Name.ToString();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage("graymond2007@gmail.com", sendAddress, subject, text);
                mail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("graymond2007@gmail.com", "zonama sekot");
                client.EnableSsl = true;
                client.Send(mail);
            }
            catch
            { }
        }
    }
}
