﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Acharis_ErrorReportingBase
{
    public class ExceptionLoader
    {
        private static string precursor = Path.GetDirectoryName(System.Environment.GetEnvironmentVariable("ACH_ERROR_PATH", EnvironmentVariableTarget.Process) ?? Environment.CurrentDirectory);
        private static BinaryFormatter form = new BinaryFormatter();
        private const string extension = ".errordump";

        public static string WriteException(Exception ex)
        {
            return WriteException(new SerializableException(ex));
        }
        public static string WriteException(SerializableException ex)
        {
            string filename = precursor + "\\" + ex.UniqueID + extension;
            try
            {
                using (Stream s = new FileStream(filename, FileMode.OpenOrCreate))
                    form.Serialize(s, ex);
            }
            catch(Exception e) 
            { 
                System.Diagnostics.Trace.WriteLine("Unable to save error file " + filename + Environment.NewLine + e.ToString()); 
            }
            return ex.UniqueID.ToString();
        }
        public static SerializableException GetLastException()
        {
            List<SerializableException> ex = GetAll();
            if (ex.Count == 0)
                return null;

            SerializableException latest = ex[0];
            for (int i = 0; i < ex.Count; i++)
                if (ex[i].TimeStamp > latest.TimeStamp)
                    latest = ex[i];

            return latest;
        }
        public static SerializableException GetException(string name)
        {
            Guid id = Guid.Parse(name);
            List<SerializableException> ex = GetAll();
            for (int i = 0; i < ex.Count; i++)
                if (ex[i].UniqueID == id)
                    return ex[i];

            return null;
        }
        private static List<SerializableException> GetAll()
        {
            string[] files = Directory.GetFiles(precursor, "*" + extension, SearchOption.AllDirectories);
            List<SerializableException> exceptions = new List<SerializableException>();
            for (int i = 0; i < files.Length; i++)
            {
                using(Stream stream = File.Open(files[i], FileMode.Open))
                {
                    try
                    {
                        exceptions.Add((SerializableException)form.Deserialize(stream));
                    }
                    catch (Exception e) { System.Diagnostics.Trace.WriteLine("Unable to load file " + files[i] + Environment.NewLine + e.ToString()); }
                }
            }
            return exceptions;
        }
    }
}
