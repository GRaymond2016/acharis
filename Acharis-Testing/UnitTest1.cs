﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acharis;
using System.Threading;
using System.IO;


namespace Acharis_Testing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestInertialMovement()
        {
            InertialObject object1 = new InertialObject();
            object1.CurrentPosition = new Acharis.Game_Elements.PointF(0, 0);
            object1.Rotation = 90;
            object1.TargetRotation = 90;
            object1.Speed = 1.0f;
            object1.Target = new Acharis.Game_Elements.Point(1000, 0);

            StringBuilder rs = new StringBuilder();

            object1.LastMove = DateTime.UtcNow;
            for (int i = 0; i < 12; ++i)
            {
                Thread.Sleep(25);
                float delta = (DateTime.UtcNow - object1.LastMove).Milliseconds / (float)object1.MoveDelay;
                rs.AppendLine("Delta: " + delta + " Last Action: " + object1.LastMove.Ticks);
                object1.Move();
            }

            InertialObject object2 = new InertialObject();
            object2.CurrentPosition = new Acharis.Game_Elements.PointF(0, 0);
            object2.Rotation = 90;
            object2.TargetRotation = 90;
            object2.Speed = 1.0f;
            object2.Target = new Acharis.Game_Elements.Point(1000, 0);

            object2.LastMove = DateTime.UtcNow;
            for (int i = 0; i < 6; ++i)
            {
                Thread.Sleep(50);
                float delta = (DateTime.UtcNow - object2.LastMove).Milliseconds / (float)object2.MoveDelay;
                rs.AppendLine("Delta: " + delta + " Last Action: " + object2.LastMove.Ticks);
                object2.Move();
            }

            String output = rs.ToString();
            Assert.AreEqual(object1.CurrentPosition.X, object2.CurrentPosition.X);
        }
    }
}
