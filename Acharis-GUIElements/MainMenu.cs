﻿using System;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis_GUIElements.MainMenuComponents;
using Squid;

namespace Acharis_GUIElements
{
    public class MainMenu : RootWindow
    {
        MenuButton SinglePlayer = new MenuButton();
        MenuButton Multiplayer = new MenuButton();
        MenuButton Options = new MenuButton();
        MenuButton Exit = new MenuButton();

        SmallerButton GraphicsOptions = new SmallerButton();
        SmallerButton AudioOptions = new SmallerButton();

        public MainMenu(Point startingSize)
        {
            this.Size = startingSize;
            Background = AppConfig["MainMenu\\Background"];
            Initialize();//We call initialize here because Squid likes playing with sizes of the MainMenu window.
        }
        protected override void Initialize()
        {
            base.Initialize();

            if (Size.x == 0 || Size.y == 0)
                return;

            int fRef = Size.y / 2;
            int xP = 70;
            int diff = 40;

            SinglePlayer.Size = Multiplayer.Size = Options.Size = Exit.Size = new Point(400, 50);
            GraphicsOptions.Size = AudioOptions.Size = new Point((int)(Options.Size.x * 0.6), (int)(Options.Size.y * 0.6));

            int ss = SinglePlayer.Size.y;
            int runner = fRef - ((ss + diff) * 2);
            SinglePlayer.Position = new Point(xP, runner); runner += (ss + diff);
            Multiplayer.Position = new Point(xP, runner); runner += (ss + diff);

            Options.Position = new Point(xP, runner);
            GraphicsOptions.Position = new Point(xP + Options.Size.x + 20, runner + (int)(Options.Size.y * 0.2));
            AudioOptions.Position = new Point(xP + (Options.Size.x + GraphicsOptions.Size.x) + 10, runner + (int)(Options.Size.y * 0.2)); 
            runner += (ss + diff);

            Exit.Position = new Point(xP, runner); runner += (ss + diff);

            SinglePlayer.Anchor = Multiplayer.Anchor = Options.Anchor = 
                GraphicsOptions.Anchor = AudioOptions.Anchor = Exit.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            SinglePlayer.Parent = Multiplayer.Parent = Options.Parent =
                GraphicsOptions.Parent = AudioOptions.Parent = Exit.Parent = this;

            SinglePlayer.AutoSize = Multiplayer.AutoSize = Options.AutoSize =
                GraphicsOptions.AutoSize = AudioOptions.AutoSize = Exit.AutoSize = Squid.AutoSize.None;

            SinglePlayer.Text = "Singleplayer";
            Multiplayer.Text = "Multiplayer";
            Options.Text = "Options";
            GraphicsOptions.Text = "Graphics";
            AudioOptions.Text = "Audio";
            Exit.Text = "Exit";

            SinglePlayer.Style = Multiplayer.Style = Options.Style = Exit.Style = MenuButton.StyleName;
            GraphicsOptions.Style = AudioOptions.Style = SmallerButton.StyleName;

            MouseUpEventHandler state = new MouseUpEventHandler(MainMenuState);
            SinglePlayer.OnMouseUp += new MouseUpEventHandler(SinglePlayer_OnMousePress);
            SinglePlayer.OnMouseUp += state;
            Multiplayer.OnMouseUp += new MouseUpEventHandler(Multiplayer_OnMousePress);
            Multiplayer.OnMouseUp += state;
            Options.OnMouseUp += state;
            GraphicsOptions.OnMouseUp += new MouseUpEventHandler(GraphicsOptions_OnMousePress);
            AudioOptions.OnMouseUp += new MouseUpEventHandler(AudioOptions_OnMousePress);
            Exit.OnMouseUp += new MouseUpEventHandler(Exit_OnMousePress);

            GraphicsOptions.Visible = false;
            AudioOptions.Visible = false;
            
            Controls.Add(SinglePlayer);
            Controls.Add(Multiplayer);
            Controls.Add(Options);
            Controls.Add(Exit);
            Controls.Add(AudioOptions);
            Controls.Add(GraphicsOptions);
        }
        private void MainMenuState(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;
            if (sender == Options)
                GraphicsOptions.Visible = AudioOptions.Visible = !GraphicsOptions.Visible | !AudioOptions.Visible;
            else
                GraphicsOptions.Visible = AudioOptions.Visible = false;
        }
        private void SinglePlayer_OnMousePress(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;
        }
        private void Multiplayer_OnMousePress(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;

            Multiplayer nextScreen = new Multiplayer(this.Size);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
        }
        private void GraphicsOptions_OnMousePress(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;
        }
        private void AudioOptions_OnMousePress(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;
        }
        public void Exit_OnMousePress(Control sender, MouseEventArgs args)
        {
            if (TimeCheck()) return;

            (Parent as DefaultDesktop).CurrentState = GuiState.Exit;
        }
    }
}
