﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Core_Elements;
using k = Microsoft.Xna.Framework.Input;

namespace Acharis_GUIElements.MultiUseComponents
{
    public class ChatBox : Frame
    {
        private Control LastFocused;

        public ExtendedTextInput Input { get; private set; }
        public Label Output { get; private set; }
        public VScrollbar Scrollbar { get; private set; }
        public Frame Frame { get; private set; }

        public string PlayerName = GalaxyController.CurrentPlayer.PlayerName;
        private const string ChatRoom = "GalaxyMap";

        public ChatBox()
        {
            ChatInterface.SubscribeToChat(ChatRoom, new ChatInterface.ChatReceiver(Append));
        }
        ~ChatBox()
        {
            ChatInterface.UnsubcribeToChat(ChatRoom, new ChatInterface.ChatReceiver(Append));
        }
        protected override void Initialize()
        {
            base.Initialize();

            AllowFocus = false;
            AllowDrop = false;
            Scissor = true;

            OnGotFocus += new FocusEventHandler(delegate { Input.Focus(); });

            ControlStyle sl = new ControlStyle();
            sl.TextAlign = Alignment.TopLeft;
            sl.TextPadding = new Squid.Margin(2);
            sl.BackColor = Squid.ColorInt.FromArgb(150, 25, 25, 25);
            GuiHost.AddStyle("ChatOutput", sl);

            Input = new ExtendedTextInput();
            Input.Size = new Point(100, 30);
            Input.Dock = DockStyle.Bottom;
            Input.Style = "ExtendedTextInput";
            Input.Cursor = Cursors.Link;
            Input.OnKeyDown += new KeyEventHandler(Input_OnKeyEvent);

            Scrollbar = new VScrollbar();
            Scrollbar.Dock = DockStyle.Right;
            Scrollbar.Size = new Point(25, Size.y);
            Scrollbar.Margin = new Margin(0, 8, 8, 8);
            Scrollbar.Slider.Style = "vscrollTrack";
            Scrollbar.Slider.Button.Style = "vscrollButton";
            Scrollbar.ButtonUp.Style = "vscrollUp";
            Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
            Scrollbar.ButtonDown.Style = "vscrollDown";
            Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);

            Frame = new Frame();
            Frame.Dock = DockStyle.Fill;
            Frame.Scissor = true;

            Output = new Label();
            Output.BBCodeEnabled = true;
            Output.TextWrap = true;
            Output.AutoSize = Squid.AutoSize.Vertical;
            Output.Style = "ChatOutput";
            
            Elements.Add(Frame);
            Elements.Add(Scrollbar);
            Elements.Add(Input);

            Frame.Controls.Add(Output);
        }
        protected override void OnUpdate()
        {
            base.OnUpdate();

            if(Frame.Size.x > 0 && Output.Size.y > 0)
                Output.Size = new Point(Frame.Size.x - Scrollbar.Size.x, Output.Size.y);

            // move the label up/down using the scrollbar value
            if (Output.Size.y < Frame.Size.y - Input.Size.y) // no need to scroll
            {
                Scrollbar.Visible = false; // hide scrollbar
                Output.Position = new Point(0, Frame.Size.y - Output.Size.y - Input.Size.y); // set fixed position
            }
            else
            {
                Scrollbar.Scale = Math.Min(1, (float)(Frame.Size.y - Input.Size.y) / (float)Output.Size.y);
                Scrollbar.Visible = true; // show scrollbar
                Output.Position = new Point(0, (int)(((Frame.Size.y - Input.Size.y) - Output.Size.y) * Scrollbar.Value));
            }

            // we want to perform mouse scrolling if:
            // the mouse is scrolling and there is any control hovered
            if (GuiHost.MouseScroll != 0 && Desktop.HotControl != null)
            {
                // ok, lets check if the mouse is anywhere near us
                if (Hit(GuiHost.MousePosition.x, GuiHost.MousePosition.y))
                {
                    // now lets check if its really this window or anything in it
                    if (Desktop.HotControl == this || Desktop.HotControl.IsChildOf(this))
                        Scrollbar.Scroll(GuiHost.MouseScroll);
                }
            }

            if (LastFocused == Input)
            {
                foreach (KeyData data in GuiHost.KeyBuffer)
                {
                    if (data.Key == Keys.RETURN)
                    {
                        Input.Focus();
                        break;
                    }
                }
            }
        }
        protected override void OnLateUpdate()
        {
            base.OnLateUpdate();

            LastFocused = Desktop.FocusedControl;
        }
        public void Append(string text)
        {
            // check for null/empty
            if (string.IsNullOrEmpty(text))
                return;

            if (text.Trim().Length == 0)
                return;

            if (string.IsNullOrEmpty(Output.Text)) //We do this in here because it determines visible lines the first time text is added
                Output.Text = text;
            else
                Output.Text += Environment.NewLine + text;

            Scrollbar.Value = 1; // scroll down
        }
        void Input_OnKeyEvent(Control sender, KeyEventArgs args)
        {
            if (args.Key == Keys.RETURN)
            {
                ChatInterface.SendChatMessage(Input.Text, ChatRoom); // try to send the text
                Input.Text = string.Empty; // clear input field
            }
        }
    }
}
