﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;
using System.IO;

namespace Acharis_GUIElements
{
    public abstract class RootWindow : Window
    {
        //Two events fire when a button is clicked even if its the MouseUp event.
        protected static AcharisConfig AppConfig = new AcharisConfig();
        protected DateTime lastKeyPress = DateTime.Now;
        protected bool TimeCheck()
        {
            bool res = false;
            if (DateTime.Now.Subtract(lastKeyPress).TotalMilliseconds < 5)
                res = true;
            lastKeyPress = DateTime.Now;
            return res;
        }
        public void DoCustomDraw()
        {
            if (CanBDraw())
                BackDraw();
            else
                GuiHost.Renderer.DrawBox(this.Position.x, this.Position.y, this.Size.x, this.Size.y, Squid.ColorInt.RGBA(100, 100, 100, 200));
        }
        public void DoTopDraw()
        {
            //This should be overloaded if you need elements drawn on top of GUI.
            DrawTopElements();
        }
        protected bool DrawBack = false;
        private string m_Background = null;
        protected string Background { get { return m_Background; } set { m_Background = value; if (File.Exists(Background))DrawBack = true; } }
        protected virtual bool CanBDraw()
        {
            return Background != null;
        }
        protected virtual void BackDraw()
        {
            if (DrawBack)
            {
                int tex = GuiHost.Renderer.GetTexture(Background);
                GuiHost.Renderer.DrawTexture(tex, this.Position.x, this.Position.y, this.Size.x, this.Size.y, new UVCoords() { U2 = 1.0f, V2 = 1.0f }, 0);
            }
        }
        protected virtual void DrawTopElements()
        {
        }
    }
}
