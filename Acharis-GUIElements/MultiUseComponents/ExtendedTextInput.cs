﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using k = Microsoft.Xna.Framework.Input;
using Acharis.Core_Elements;
using System.Threading;
using System.Text.RegularExpressions;
using f = System.Windows.Forms;

namespace Acharis_GUIElements.MultiUseComponents
{
    public class ExtendedTextInput : TextBox
    {
        public bool focus = false;
        private static AcharisConfig config = new AcharisConfig();
        private ControlStyle mBStyle = new ControlStyle();

        public ExtendedTextInput()
        {
            Text = "";
        }
        protected override void Initialize()
        {
            base.Initialize();

            mBStyle.Default.Texture = config["SharedResources\\TextBoxDeselected"];
            mBStyle.Hot.Texture = config["SharedResources\\TextBox"]; //This is rollover
            mBStyle.Focused.Texture = config["SharedResources\\TextBox"]; //This is clicked
            mBStyle.TextAlign = Alignment.MiddleCenter;

            GuiHost.AddStyle("ExtendedTextInput", mBStyle);        
        }
    }//End Class
}//End Namespace
