﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis_GUIElements.MainMenuComponents;
using Acharis.Core_Elements;

namespace Acharis_GUIElements.MultiUseComponents
{
    public class PopupDialog : RootWindow
    {
        public enum Buttons : int
        {
            None = 0,
            Cancel = 1,
            Continue = 2,
            Retry = 4,
            Accept = 8,
            Ok = 16,
            Back = 32
        }


        public int MaxWidth { get; set; }
        public bool Exitable { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public Buttons SelectedButtons { get; set; }

        private Label title;
        private Label message;
        private SmallerButton[] buttons = new SmallerButton[0];

        public PopupDialog(string title, string message, Buttons buttons, Point workingSize, int maxWidth)
        {
            DrawBack = false;
            this.Size = workingSize;
            Title = title;
            Message = message;
            SelectedButtons = buttons;
            MaxWidth = maxWidth;

            ControlStyle itemStyle = new ControlStyle();
            itemStyle.Grid = new Margin(6);
            itemStyle.Pressed.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedPressed.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.Focused.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedFocused.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.Selected.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedHot.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.TextPadding = new Margin(10, 0, 10, 0);

            ControlStyle backStyle = new ControlStyle();
            backStyle.Texture = AppConfig["Popup\\FrameOpaque"];

            GuiHost.AddStyle("itemStyle", itemStyle);
            GuiHost.AddStyle("backStyle", backStyle);

            Initialize();
        }
        public override void Close()
        {
            DefaultDesktop.CloseDialog(this);
            base.Close();
        }
        protected override void Initialize()
        {
            base.Initialize();

            if (Size.x == 0 || Size.y == 0)
                return;

            Style = "backStyle";
            message = new Label();
            title = new Label();
            title.Text = Title;
            title.Dock = DockStyle.Top;
            title.OnMouseDown += delegate(Control sender, MouseEventArgs arg) { StartDrag(); };
            title.OnMouseUp += delegate(Control sender, MouseEventArgs arg) { StopDrag(); };
            title.Cursor = Cursors.Move;
            title.Style = "itemStyle";
            title.Margin = new Margin(20, 10, 20, 10);
            message.Text = Message;
            message.TextWrap = true;
            if (SelectedButtons != Buttons.None)
            {
                List<SmallerButton> buttons = new List<SmallerButton>();
                string[] names = Enum.GetNames(typeof(Buttons));
                for (int i = 0; i < names.Length; i++)
                {
                    Buttons button = (Buttons)Enum.Parse(typeof(Buttons), names[i]);
                    if (SelectedButtons.HasFlag(button) && button != Buttons.None)
                    {
                        SmallerButton butt = new SmallerButton();
                        butt.Text = names[i];
                        butt.Style = SmallerButton.StyleName;
                        buttons.Add(butt);
                        Controls.Add(butt);

                        switch (button)
                        {
                            case Buttons.Retry: break;
                            default: butt.OnMouseClick += new MouseClickEventHandler(delegate { Close(); }); break;
                        }
                    }
                }
                this.buttons = buttons.ToArray();
            }
            Controls.Add(title);
            Controls.Add(message);
            Resize();
        }
        public void SetHandlers(List<MouseUpEventHandler> actions)
        {
            if (buttons == null || actions.Count == 0 || actions.Count != buttons.Length)
                return;

            for (int i = 0; i < buttons.Length; i++)
                buttons[i].OnMouseUp += new MouseUpEventHandler(actions[i]);
        }
        private void Resize()
        {
            Point SWithPadding = Size;
            SWithPadding.x -= 50;
            SWithPadding.y -= 10;

            int buttonHeight = 40;
            title.Size = new Point(Size.x, 40);

            message.Position = new Point(30, 30);
            message.Size = new Point(SWithPadding.x, SWithPadding.y - buttonHeight);

            if (buttons.Length == 0)
                return;

            int white = 10, runningX = message.Position.x + white, buttonSize = (SWithPadding.x / buttons.Length);
            
            if (buttonSize > (SWithPadding.x / 2))
            {
                runningX = SWithPadding.x / 2;
                buttonSize /= 2;
            }
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Position = new Point(runningX, message.Size.y + 20);
                buttons[i].Size = new Point(buttonSize, buttonHeight);
                runningX += buttonSize + white;
            }

            Size = new Point(Size.x, buttons[0].Position.y + buttons[0].Size.y + (Size.y - SWithPadding.y));
        }
        protected override bool CanBDraw()
        {
            return Background != null;
        }
        protected override void BackDraw()
        {
            GuiHost.Renderer.DrawTexture(GuiHost.Renderer.GetTexture(Background), this.Position.x, this.Position.y, this.Size.x, this.Size.y, new UVCoords(), 0);
        }
    }
}
