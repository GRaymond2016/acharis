﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;

namespace Acharis_GUIElements.MultiUseComponents
{
    class PopupDialog : RootWindow
    {
        bool init = false;
        public enum Buttons
        {
            Cancel,
            Continue,
            Retry,
            Accept,
            Ok,
            Back
        }

        public int MaxWidth { get; set; }
        public bool Exitable { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public Buttons SelectedButtons { get; set; }

        private Label title;
        private Label message;
        private Button[] buttons;

        public PopupDialog(string title, string message, Buttons buttons, Point workingSize, int maxWidth)
        {
            this.Size = workingSize;
            Title = title;
            Message = message;
            SelectedButtons = buttons;
            MaxWidth = maxWidth;
            Initialize();
        }
        protected override void Initialize()
        {
            base.Initialize();
            if (init)
            {
                title = new Label();
                message = new Label();
                title.Text = Title;
                message.Text = Message;
                message.TextWrap = true;
                List<Button> buttons = new List<Button>();
                string[] names = Enum.GetNames(typeof(Buttons));
                for (int i = 0; i < names.Length; i++)
                    if (SelectedButtons.HasFlag((Buttons)Enum.Parse(typeof(Buttons), names[i])))
                    {
                        Button butt = new Button();
                        butt.Text = names[i];
                        buttons.Add(butt);
                    }
                this.buttons = buttons.ToArray();
                Resize();
            }
            init = false;
        }
        private void Resize()
        {
            title.Position = new Point(0, 0);
            title.Size = new Point(Size.x, 40);

            message.Position = new Point(0, 40);
            message.Size = new Point(Size.x, Size.y - title.Size.y - 80);

            int white = 10, runningX = white;
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Position = new Point(runningX, message.Size.y + 20);
                buttons[i].Size = new Point((runningX / buttons.Length), 60);
            }
        }
    }
}
