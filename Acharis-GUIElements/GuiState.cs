﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis_GUIElements
{
    public enum GuiState : int
    {
        MainMenu = 1,
        Multiplayer = 2,
        GalaxyMap = 4,
        ShipModification = 8,
        PlanetInfo = 16,
        PlanetMap = 32,
        Exit = 64,
        Connecting = 128,
        PopupDialog = 256
    }
}
