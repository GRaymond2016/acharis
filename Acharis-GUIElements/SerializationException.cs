﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Acharis_ErrorReportingBase
{
    [Serializable]
    public class SerializableException
    {
        public Guid UniqueID { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Source { get; set; }
        public string ExceptionType { get; set; }
        public SerializableException InnerException { get; set; }

        public SerializableException(System.Exception ex)
        {
            this.TimeStamp = DateTime.Now;
            this.Message = ex.Message;
            this.ExceptionType = ex.ToString();
            this.InnerException = (ex.InnerException != null) ? new SerializableException(ex.InnerException) : null;
            this.Source = ex.Source;
            this.StackTrace = ex.StackTrace;
        }

        public override string ToString()
        {
            return this.TimeStamp.ToString("yyyyMMDDhhmmss") + ": " + this.Message + this.StackTrace;
        }
    }
}
