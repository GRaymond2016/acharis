﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements;

namespace Acharis_GUIElements.GalaxyMapComponents
{
    class PlanetButton : Button
    {
        private const int SizeMulti = 15;
        private static int buttonIndex = 0;
        private static object buttonLock = new object();
        private static AcharisConfig config = new AcharisConfig();
        private ControlStyle mBStyle = new ControlStyle();
        public GalaxyPlanetBinding binding { get; private set; }
        private Squid.Point parentSize;
        public int Map { get { return binding.AssociatedMap; } }
        public List<Fleet> fleets = new List<Fleet>();

        public PlanetButton(GalaxyPlanetBinding bind, Squid.Point parentsize)
        {
            binding = bind;
            parentSize = parentsize;

            string filePath = bind.ThumbPath;
            mBStyle.Default.Texture = filePath;
            mBStyle.Pressed.Texture = filePath;
            mBStyle.Hot.Texture = filePath; //This means rollover image
            mBStyle.TextAlign = Alignment.MiddleCenter;

            lock (buttonLock)
            {
                GuiHost.AddStyle("planetButton" + buttonIndex, mBStyle);
                Style = "planetButton" + buttonIndex;
                buttonIndex++;
            }
            Cursor = Cursors.Select;
        }
        public void FormatMyself()
        {
            string lowerAch = binding.Anchor.ToLower();
            Squid.Point startingPoint = new Squid.Point(0, 0);

            if (lowerAch.Contains("center"))
            {
                startingPoint.x += parentSize.x / 2;
                startingPoint.y += parentSize.y / 2;
            }

            if (lowerAch.Contains("right"))
                startingPoint.x = parentSize.x;
            else if (lowerAch.Contains("left"))
                startingPoint.x = 0;

            if (lowerAch.Contains("top"))
                startingPoint.y = 0;
            else if (lowerAch.Contains("bottom"))
                startingPoint.y = parentSize.y;

            int averageSize = ((parentSize.y / SizeMulti) + (parentSize.x / SizeMulti)) / 2; //Get average size to maintain square whilst scaling
            int x = startingPoint.x + binding.Bounds.X, y = startingPoint.y + binding.Bounds.Y;
            Position = new Squid.Point(x, y);
            Size = new Squid.Point(averageSize, averageSize);
        }
    }
}
