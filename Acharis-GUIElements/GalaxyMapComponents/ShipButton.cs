﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements.Galaxy;
using Acharis.Game_Elements;
using Acharis.Game_Elements.Combat;

namespace Acharis_GUIElements.GalaxyMapComponents
{
    class ShipButton : Button, ISupplyDrawables
    {
        private ControlStyle mBStyle = new ControlStyle();
        private Acharis.Game_Elements.Rectangle m_bounds = null;
        private DrawableShip m_ship;
        public int ShipID { get { return m_ship.ID; } }
        public Acharis.Game_Elements.Rectangle DrawLinkedMapBounds { get; set; }
        public bool Toggled { get; set; }

        public ShipButton(Squid.Rectangle bounds, DrawableShip ship)
        {
            m_bounds = new Acharis.Game_Elements.Rectangle(bounds.x1, bounds.y1, bounds.Width, bounds.Height);
            m_ship = ship;
            Toggled = false;

            Position = new Squid.Point(bounds.x1, bounds.y1);
            Size = new Squid.Point(bounds.Width, bounds.Height);

            Text = Enum.GetName(typeof(ShipClass), ship.Class);

            Cursor = Cursors.Select;
        }
        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            AnimatedTexture tex = new AnimatedTexture()
            {
                Bounds = m_bounds,
                Texture = m_ship.Texture,
                Rotation = 90,
                Transparency = 200
            };
            draws.Enqueue(tex);
            if (Toggled)
            {
                Box map_bound = new Box()
                {
                    Bounds = DrawLinkedMapBounds,
                    Color = 0x8855ff55,
                    SizeScale = 1.0f
                };
                draws.Enqueue(map_bound);
          
                Box this_bound = new Box()
                {
                    Bounds = new Acharis.Game_Elements.Rectangle(Position.x, Position.y, Size.x, Size.y),
                    Color = 0x1055ff55,
                    SizeScale = 1.0f
                };
                draws.Enqueue(this_bound);
            }
        }
    }
}
