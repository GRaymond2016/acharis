﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis_GUIElements.GalaxyMapComponents;
using Acharis_GUIElements.MultiUseComponents;
using AGE = Acharis.Game_Elements;
using Acharis.Game_Elements.Galaxy;
using Acharis.Game_Elements;
using Acharis.Core_Elements.Network;
using Acharis_GUIElements.MainMenuComponents;
using Acharis.Core_Elements;
using Acharis.Game_Elements.Combat;

namespace Acharis_GUIElements
{
    public class GalaxyMap : RootWindow, AGE.ISupplyDrawables
    {
        class FleetAnnotations
        {
            public const string EnemyStyle = "EnemyAnnotationStyle";
            public const string AllyStype = "AllyAnnotationStyle";
            public Label EnemyAnnotation { get; set; }
            public Label AllyAnnotation { get; set; }
        }

        private static AGE.IPaintCanvas painter;

        private bool _updated;
        private Dictionary<int, FleetAnnotations> fleet_annotations = new Dictionary<int, FleetAnnotations>();
        private Dictionary<string, Button> fleet_selection = new Dictionary<string, Button>();

        private Button hovered_planet = null;
        private string hovered_planet_backing = null;
        private string fleet_symbol = null;

        private Button current_fleet = null;
        private Fleet current_selected = null;
        private string selected_fleet_backing = null;

        private List<PlanetButton> buttons;
        private List<PlanetInfoPopup> panels;
        private List<PlanetMap> planets = new List<PlanetMap>();
        private List<ShipButton> ship_buttons = new List<ShipButton>();
        private List<ShipButton> active_ship_buttons = new List<ShipButton>();

        private List<Squid.Control> racial_selection = new List<Squid.Control>();
        private List<BoundedComponent> racialBacking = new List<BoundedComponent>();

        private Acharis.Game_Elements.Galaxy.GalaxyMap boundGalaxyMap = new Acharis.Game_Elements.Galaxy.GalaxyMap();
        private int fleet_count = 1;

        public List<PlanetMap> Planets
        {
            get { return planets; }
        }
        public static AGE.IPaintCanvas Painter
        {
            set
            {
                painter = value;
            }
        }

        Label Money { get; set; }
        Label LastMoneyLabel { get; set; }
        ChatBox chat { get; set; }

        public GalaxyMap(Squid.Point parentBounds)
        {
            Size = parentBounds;
            Background = AppConfig["GalaxyMap\\Background"];

            ControlStyle headerStyle = new ControlStyle();
            headerStyle.Font = "Berlin Sans FB Demi";
            GuiHost.AddStyle("headerStyle", headerStyle);

            ControlStyle enemyStyle = new ControlStyle();
            enemyStyle.TextColor = Squid.ColorInt.RGBA(255, 0, 0, 255);
            enemyStyle.Font = "Arial14";
            GuiHost.AddStyle(FleetAnnotations.EnemyStyle, enemyStyle);

            ControlStyle allyStyle = new ControlStyle();
            allyStyle.TextColor = Squid.ColorInt.RGBA(255, 255, 0, 255);
            allyStyle.Font = "Arial14";
            GuiHost.AddStyle(FleetAnnotations.AllyStype, allyStyle);

            AddRacialFleetStyle(Race.Endlorian);
            AddRacialFleetStyle(Race.Human);
            AddRacialFleetStyle(Race.Tchuan);

            ControlStyle racialSelectionText = new ControlStyle();
            racialSelectionText.TextAlign = Alignment.MiddleCenter;
            racialSelectionText.Font = "AcharisFont";
            racialSelectionText.TextColor = Squid.ColorInt.RGBA(0, 0, 0, 255);
            GuiHost.AddStyle("RacialSelectionFont", racialSelectionText);

            ControlStyle backButtonStyle = new ControlStyle();
            backButtonStyle.Default.Tint = 0;
            backButtonStyle.Hot.Tint = 1;
            backButtonStyle.Pressed.Tint = 2;
            backButtonStyle.Default.Texture = backButtonStyle.Pressed.Texture = backButtonStyle.Hot.Texture = AppConfig["PlanetMap\\ForfeitButton"];
            backButtonStyle.TextAlign = Alignment.MiddleCenter;
            backButtonStyle.Grid = new Squid.Margin(0);
            backButtonStyle.Tiling = TextureMode.Grid;
            GuiHost.AddStyle("ForfeitButton", backButtonStyle);

            AddRacialButtonStyle(Race.Human);
            AddRacialButtonStyle(Race.Tchuan);
            AddRacialButtonStyle(Race.Endlorian);

            selected_fleet_backing = AppConfig["GalaxyMap\\FleetBackingImage"];
            hovered_planet_backing = AppConfig["GalaxyMap\\PlanetEntryIcon"];

            Initialize();
        }
        private void AddRacialFleetStyle(Race race)
        {
            ControlStyle buttonStyle = new ControlStyle();
            string tpath = AppConfig["GalaxyMap\\" + race.ToString() + "FleetSymbol"];
            buttonStyle.Checked.Texture = tpath;
            buttonStyle.CheckedHot.Texture = tpath;
            buttonStyle.CheckedPressed.Texture = tpath;
            buttonStyle.Default.Texture = tpath;
            buttonStyle.Pressed.Texture = tpath;
            buttonStyle.Hot.Texture = tpath;
            buttonStyle.Font = "Arial10";
            buttonStyle.TextColor = Squid.ColorInt.RGBA(50, 50, 50, 200);
            buttonStyle.Default.TextAlign = Alignment.MiddleCenter;
            buttonStyle.Default.Font = "Arial10";
            buttonStyle.TextAlign = Alignment.MiddleCenter;
            buttonStyle.Grid = new Squid.Margin(0);
            buttonStyle.Tiling = TextureMode.Grid;
            GuiHost.AddStyle(race.ToString() + "FleetButton", buttonStyle);
        }
        private void AddRacialButtonStyle(Race race)
        {
            string path = AppConfig["GalaxyMap\\" + race.ToString() + "RacialButton"];
            ControlStyle controlStyle = new ControlStyle();
            controlStyle.Texture = path;
            controlStyle.Default.Texture = path;
            controlStyle.Default.Tint = 0;
            controlStyle.Pressed.Texture = path;
            controlStyle.Pressed.Tint = 2;
            controlStyle.Hot.Texture = path; //This means rollover image
            controlStyle.Hot.Tint = 1;
            controlStyle.TextAlign = Alignment.MiddleCenter;
            controlStyle.Grid = new Squid.Margin(0);
            controlStyle.Tiling = TextureMode.Grid;

            GuiHost.AddStyle(race + "RacialButton", controlStyle);
        }
        private void AddMoneyIndicator()
        {
            Money = new Label();
            Money.Text = "Money: 0";
            Money.Position = new Squid.Point(20, 20);
            Money.Size = new Squid.Point(100, 60);
            Controls.Add(Money);

            GalaxyController.CurrentPlayer.money_change += new Player.MoneyUpdate(delegate(int amount) { Money.Text = "Money: " + amount; });

            LastMoneyLabel = Money;
        }
        private void AddCruiserButton(Player current)
        {
            const int cruiser_cost = 4000;

            Button cruiser = new Button();
            cruiser.Text = "Cruiser - " + cruiser_cost;
            cruiser.Size = new Squid.Point(100, 60);
            cruiser.Position = new Squid.Point(20, LastMoneyLabel.Position.y + LastMoneyLabel.Size.y + 20);
            cruiser.Style = MenuButton.StyleName;
            cruiser.OnMouseClick += new MouseClickEventHandler(
                delegate(Control c, MouseEventArgs a)
                {
                    if (a.Button == 0 && current.Money > cruiser_cost)
                    {
                        BuyMeAShip buy = new BuyMeAShip()
                        {
                            playerid = current.PlayerKey,
                            ship_class = AGE.Combat.ShipClass.Cruiser
                        };
                        GlobalNetwork.SendToAll(buy);
                    }
                });
            Controls.Add(cruiser);
            LastMoneyLabel = cruiser;
        }
        private void AddFrigateButton(Player current)
        {
            const int frigate_cost = 1000;
            Button frigate = new Button();
            frigate.Text = "Frigate - " + frigate_cost;
            frigate.Size = new Squid.Point(100, 60);
            frigate.Position = new Squid.Point(20, LastMoneyLabel.Position.y + LastMoneyLabel.Size.y + 20);
            frigate.Style = MenuButton.StyleName;
            frigate.OnMouseClick += new MouseClickEventHandler(
                delegate(Control c, MouseEventArgs a)
                {
                    if (a.Button == 0 && current.Money > frigate_cost)
                    {
                        BuyMeAShip buy = new BuyMeAShip()
                        {
                            playerid = current.PlayerKey,
                            ship_class = AGE.Combat.ShipClass.Frigate
                        };
                        GlobalNetwork.SendToAll(buy);
                    }
                });
            Controls.Add(frigate);
            LastMoneyLabel = frigate;
        }
        private void AddDestroyerButton(Player current)
        {
            const int destroyer_cost = 2000;

            Button destroyer = new Button();
            destroyer.Text = "Destroyer - " + destroyer_cost;
            destroyer.Size = new Squid.Point(100, 60);
            destroyer.Position = new Squid.Point(20, LastMoneyLabel.Position.y + LastMoneyLabel.Size.y + 20);
            destroyer.Style = MenuButton.StyleName;
            destroyer.OnMouseClick += new MouseClickEventHandler(
                delegate(Control c, MouseEventArgs a)
                {
                    if (a.Button == 0 && current.Money > destroyer_cost)
                    {
                        BuyMeAShip buy = new BuyMeAShip()
                        {
                            playerid = current.PlayerKey,
                            ship_class = AGE.Combat.ShipClass.Destroyer
                        };
                        GlobalNetwork.SendToAll(buy);
                    }
                });
            Controls.Add(destroyer);
            LastMoneyLabel = destroyer;
        }

        private void RefreshFleetInterface(GalaxyPlanetBinding binding)
        {
            if (!fleet_annotations.ContainsKey(binding.AssociatedMap))
                return;

            FleetAnnotations annotation = fleet_annotations[binding.AssociatedMap];
            annotation.AllyAnnotation.Text = binding.AssociatedFleetStats.AlliedFleets.ToString();
            if (binding.AssociatedFleetStats.YourFleets > 0)
            {
                annotation.EnemyAnnotation.Text = binding.AssociatedFleetStats.EnemyFleets.ToString();
                annotation.EnemyAnnotation.Visible = true;
            }
            else
                annotation.EnemyAnnotation.Visible = false;
        }
        private void SwapFleetIcon(PlanetButton button, Button fleetButton, int pos = -1)
        {
            int buttonhalfx = button.Size.x / 2, 
                fleethalfx = fleetButton.Size.x / 2, 
                y = button.Position.y - fleetButton.Size.y, 
                cetoff = (int)(fleetButton.Size.x * 1.5);

            pos = pos != -1 ? pos : button.fleets.Count;
            fleetButton.Position = new Squid.Point(button.Position.x + (buttonhalfx - fleethalfx) + (pos % 2 != 0 ? -2 : 2) * fleethalfx * (pos > 1 ? pos - 1 : pos), y);  
            if (pos > 2 || pos < 0)
                Logger.WriteLine("Swap fleet could not occur, fleets on particular button too great.", Logger.LogType.WARN);
        }
        protected void InitializeMap()
        {
            chat = new ChatBox();
            chat.Size = new Squid.Point(Size.x / 5, Size.y / 5);
            chat.Position = new Squid.Point(0, Size.y - chat.Size.y);

            this.OnMouseUp += new MouseUpEventHandler(delegate(Control control, MouseEventArgs args)
            {
                if (current_fleet != null && args.Button == 0)
                {
                    current_fleet.Checked = false;
                    current_fleet = null;
                }
            });

            Controls.Add(chat);

            if (buttons != null)
            {
                for (int i = 0; i < buttons.Count; i++)
                    Controls.Remove(buttons[i]);
            }
            buttons = new List<PlanetButton>();
            panels = new List<PlanetInfoPopup>();

            OnMousePress += new MouseDownEventHandler(
            delegate(Control sender, MouseEventArgs arg)
            {
                if (arg.Button == 1)
                    current_selected = null;
            });

            using (var players = GalaxyController.Players)
            {
                foreach (Player p in players.Get())
                {
                    p.Subscribe(delegate(Fleet update, Map lastMap)
                    {
                        PlanetButton addition = null, deletion = null;
                        Button fleetButton = null;
                        if (update.Owner == GalaxyController.CurrentPlayer)
                        {
                            if (!fleet_selection.ContainsKey(update.Name))
                            {
                                fleet_symbol = AppConfig["GalaxyMap\\" + update.Owner.PlayerRace.ToString() + "FleetSymbol"];

                                Button button = new Button();
                                button.Style = update.Owner.PlayerRace.ToString() + "FleetButton";
                                button.Text = fleet_count.ToString();
                                fleet_count++;
                                button.CheckOnClick = true;
                                button.Cursor = Cursors.Select;
                                button.Size = new Squid.Point(32, 32);
                                button.OnMouseClick += new MouseClickEventHandler(delegate
                                {
                                    current_fleet = button;
                                    current_selected = update;
                                });
                                fleet_selection.Add(update.Name, button);
                                Controls.Add(button);
                            }
                            for (int i = 0; i < buttons.Count; i++)
                            {
                                if (buttons[i].binding.AssociatedMap == update.Location.MapKey)
                                    addition = buttons[i];
                                else if (lastMap != null && buttons[i].binding.AssociatedMap == lastMap.MapKey)
                                    deletion = buttons[i];
                            }
                            fleetButton = fleet_selection[update.Name];
                            if (deletion != null)
                            {
                                deletion.fleets.Remove(update);
                                //Deletion has to find the fleet button associated with the planet.
                                for (int i = 0; i < deletion.fleets.Count; ++i)
                                    SwapFleetIcon(deletion, fleet_selection[deletion.fleets[i].Name], i);

                                lock (Controls)
                                {
                                    Controls.Remove(fleetButton);
                                }
                            }
                            //If we are newly adding a fleet we immediately add it here instead of waiting
                            //for a callback when the fleet arrives at its destination.
                            if (addition != null)
                            {
                                if (lastMap == null)
                                {
                                    SwapFleetIcon(addition, fleetButton);
                                    addition.fleets.Add(update);
                                }
                            }  
                        }
                        GalaxyPlanetBinding destinationFleet = null;
                        using (var dplanets = GalaxyController.Planets)
                        {
                            int changes = 0;
                            foreach (GalaxyPlanetBinding binding in dplanets.Get())
                            {
                                if (lastMap != null && binding.AssociatedMap == lastMap.MapKey)
                                {
                                    if (update.Owner.PlayerKey == p.PlayerKey)
                                        binding.AssociatedFleetStats.YourFleets -= 1;
                                    else if (update.Owner.PlayerTeam == p.PlayerTeam)
                                        binding.AssociatedFleetStats.AlliedFleets -= 1;
                                    else
                                        binding.AssociatedFleetStats.EnemyFleets -= 1;
                                    RefreshFleetInterface(binding);
                                    ++changes;
                                }
                                else if (binding.AssociatedMap == update.Location.MapKey)
                                {
                                    if (update.Owner.PlayerKey == p.PlayerKey)
                                    {
                                        if (lastMap == null)
                                            binding.AssociatedFleetStats.YourFleets += 1;
                                        destinationFleet = binding;
                                    }
                                    else if (update.Owner.PlayerTeam == p.PlayerTeam)
                                        binding.AssociatedFleetStats.AlliedFleets += 1;
                                    else
                                        binding.AssociatedFleetStats.EnemyFleets += 1;
                                    RefreshFleetInterface(binding);
                                    ++changes;
                                }

                                if (changes >= 2)
                                    break;
                            }
                        }
                    });
                }
            }
            using (var dplanets = GalaxyController.Planets)
            {
                for (int i = 0; i < dplanets.Get().Count; i++)
                {
                    PlanetButton butt = new PlanetButton(dplanets.Get()[i], this.Size);
                    PlanetInfoPopup pop = new PlanetInfoPopup(Size);

                    butt.FormatMyself();
                    pop.FormatMyself(new Squid.Rectangle(butt.Location, butt.Size), dplanets.Get()[i].AssociatedPlanet);

                    FleetAnnotations annot = new FleetAnnotations();
                    const int annotationHeight = 10, xmodifier = 16;
                    annot.AllyAnnotation = new Label()
                    {
                        Style = FleetAnnotations.AllyStype,
                        Position = new Squid.Point(butt.Location.x - butt.Size.x / xmodifier, (butt.Location.y + butt.Size.y / 2) - annotationHeight),
                        Size = new Squid.Point(butt.Size.x / xmodifier, annotationHeight),
                        Text = "0"
                    };
                    annot.EnemyAnnotation = new Label()
                    {
                        Style = FleetAnnotations.EnemyStyle,
                        Position = new Squid.Point(butt.Location.x - butt.Size.x / xmodifier, annot.AllyAnnotation.Position.y + annot.AllyAnnotation.Size.y + annotationHeight),
                        Size = new Squid.Point(butt.Size.x / xmodifier, annotationHeight),
                        Text = "0"
                    };

                    

                    fleet_annotations.Add(dplanets.Get()[i].AssociatedMap, annot);
                    Controls.Add(annot.AllyAnnotation);
                    annot.EnemyAnnotation.Visible = false;
                    Controls.Add(annot.EnemyAnnotation);
                    Map boundMap;
                    using (var mapl = GalaxyController.Maps)
                    {
                        var dict = mapl.Get();
                        boundMap = dict[dplanets.Get()[i].AssociatedMap];
                    }

                    PlanetMap pm = new PlanetMap(dplanets.Get()[i], boundMap);
                    pm.Visible = false;

                    pm.OnQuit +=
                        delegate
                        {
                            DefaultDesktop.ScreenOrder.Push(this);
                        };

                    butt.OnMouseEnter += new MouseEnterEventHandler(delegate { hovered_planet = butt; });
                    butt.OnMouseLeave += new MouseLeaveEventHandler(delegate { hovered_planet = null; });

                    butt.OnMouseUp += new MouseUpEventHandler(
                        delegate(Control sender, MouseEventArgs arg)
                        {
                            if (arg.Button == 1)
                            {
                                if (current_selected != null && current_selected.Location != boundMap)
                                {
                                    Map cloc = current_selected.Location;
                                    Fleet current_snapshot = current_selected;
                                    Button fleetButton = fleet_selection[current_snapshot.Name];
                                    current_selected.ChangeMap(boundMap, new Action( 
                                    delegate {
                                        Line ls = new Line();

                                        PlanetButton addition = null, deletion = null;
                                        foreach (PlanetButton planet in buttons)
                                        {
                                            if (planet.binding.AssociatedMap == cloc.MapKey)
                                            {
                                                ls.StartPoint = new Acharis.Game_Elements.Point(planet.Position.x + planet.Size.x / 2,
                                                                            planet.Position.y + planet.Size.y / 2);
                                                deletion = planet;
                                            }
                                            else if (planet.binding.AssociatedMap == boundMap.MapKey)
                                            {
                                                ls.EndPoint = new Acharis.Game_Elements.Point(planet.Position.x + planet.Size.x / 2,
                                                                            planet.Position.y + planet.Size.y / 2);
                                                addition = planet;
                                            }
                                        }
                                        ls.Color = GalaxyController.CurrentPlayer.PlayerColor;

                                        Transfer trans = new Transfer(fleet_symbol);
                                        trans.start_button = deletion;
                                        trans.end_button = addition;
                                        trans.start = DateTime.UtcNow;
                                        trans.line = ls;
                                        trans.time = DateTime.UtcNow.AddSeconds(10 + ls.StartPoint.DistanceTo(ls.EndPoint) / 16);
                                        trans.AddAction(current_snapshot.Transfer.result);
                                        trans.AddAction(delegate
                                        {
                                            addition.binding.AssociatedFleetStats.YourFleets += 1;
                                            RefreshFleetInterface(addition.binding);

                                            SwapFleetIcon(addition, fleetButton);
                                            addition.fleets.Add(current_snapshot);

                                            lock (Controls)
                                            {
                                                Controls.Add(fleetButton);
                                            }
                                        });

                                        lock (transferLinesLock)
                                        {
                                            transferLines.Add(trans);
                                        }
                                    }));

                                    current_selected = null;
                                    current_fleet = null;
                                }
                            }
                        });

                    long last = 0;
                    butt.OnMouseClick += new MouseClickEventHandler(
                        delegate(Control s1, MouseEventArgs a1)
                        {
                            if (last != 0 && DateTime.Now.Subtract(new DateTime(last)).TotalMilliseconds < Squid.GuiHost.DoubleClickSpeed)
                                DefaultDesktop.ScreenOrder.Push(pm);
                            else
                                pop.Visible = !pop.Visible;

                            last = DateTime.Now.Ticks;
                        });

                    buttons.Add(butt);
                    panels.Add(pop);
                    planets.Add(pm);
                    boundGalaxyMap.AddMap(boundMap, new Acharis.Game_Elements.Rectangle(butt.Position.x, butt.Position.y, butt.Size.x, butt.Size.y), dplanets.Get()[i].LinkDistance);
                }
            }
            //for (int i = 0; i < panels.Count; i++)
            //    Controls.Add(panels[i]);
            for (int i = 0; i < buttons.Count; i++)
                Controls.Add(buttons[i]);

            Button back = new Button();
            back.Style = "ForfeitButton";
            back.Position = new Squid.Point(0, 0);
            back.Size = new Squid.Point((int)(206*0.75), (int)((177/3)*0.75));
            back.OnMousePress += new MouseDownEventHandler(delegate
            {
                MainMenu nextScreen = new MainMenu(Size);
                DefaultDesktop.ScreenOrder.Push(nextScreen);
                GlobalNetwork.Stop();
            });
            Controls.Add(back);
        }
        protected override void Initialize()
        {
            base.Initialize();
            if (Size.x == 0 || Size.y == 0)
                return;

            _updated = false;

            racialBacking.Add(new Box()
            {
                Bounds = new AGE.Rectangle(Position.x, Position.y, Size.x, Size.y),
                Color = (uint) 200 << 24
            });
            CreateRacialButton(Race.Human, 1, 6);
            CreateRacialButton(Race.Tchuan, 3, 6);
            CreateRacialButton(Race.Endlorian, 5, 6);
        }
        private void CreateRacialButton(Race race, int index, int buttonNumber)
        {
            Button racialButton = new Button();
            racialButton.Style = race + "RacialButton";
            racialButton.Size = new Squid.Point(256, 256);
            racialButton.Position = new Squid.Point((Size.x / buttonNumber) * index - racialButton.Size.x / 2, Size.y / 2 - racialButton.Size.y / 2);
            racialButton.OnMouseUp += new MouseUpEventHandler(delegate { ChooseRace(race); });

            Label racialLabel = new Label();
            racialLabel.Text = race.ToString();
            racialLabel.Style = "RacialSelectionFont";
            racialLabel.Size = new Squid.Point(128, 512);
            racialLabel.Position = new Squid.Point((Size.x / buttonNumber) * index - racialLabel.Size.x / 2, (int)(Size.y / 2 - racialButton.Size.y * 1.5));

            racial_selection.Add(racialButton);
            racial_selection.Add(racialLabel);

            string banner = AppConfig["GalaxyMap\\" + race.ToString() + "RacialBanner"];
            racialBacking.Add(new MetaTexture()
            {
                Bounds = new AGE.Rectangle(racialButton.Position.x - 128, Position.y, 512, 1024),
                Texture = banner
            });
            
            Controls.Add(racialLabel);
            Controls.Add(racialButton);
        }
        private void ChooseRace(Race index)
        {
            Player player = GalaxyController.CurrentPlayer;
            player.PlayerRace = index;

            GlobalNetwork.SendToAll(player.PlayerStats);

            lock (Controls)
            {
                Controls.RemoveAll(x => racial_selection.Contains(x));
                InitializeMap();
            }
            racial_selection.Clear();
            racialBacking.Clear();
        }
        private void UpdateSelectedShip(PlanetButton butt)
        {
            IEnumerable<int> ids = active_ship_buttons.Select(x => x.ShipID);
            List<Ship> selected_ships = new List<Ship>();
            using (var shiplock = GalaxyController.Players)
            {
                foreach (int index in ids)
                {
                    int player = index / 1000;
                    Player pl = shiplock.Get().Where(x => x.PlayerKey == player).FirstOrDefault();
                    if (pl != null)
                    {
                        lock (pl.ship_lock)
                        {
                            foreach (Ship s in pl.Ships)
                            {
                                if (s.UniqueID == index)
                                    selected_ships.Add(s);
                            }
                        }
                    }
                }
            }
            using (var locker = GalaxyController.Maps)
            {
                var dict = locker.Get();
                if (!dict.ContainsKey(butt.Map))
                    return;
                Map pickedmap = dict[butt.Map];
                foreach (Map m in dict.Values)
                {
                    if (pickedmap.MapKey == m.MapKey)
                        continue;
                    foreach (Ship s in selected_ships)
                    {
                        if (s.AssociatedMap.MapKey != m.MapKey)
                            continue;
                        m.RemoveShip(s);

                        RegisterShipToMap sr = new RegisterShipToMap();
                        sr.mapId = m.MapKey;
                        sr.playerid = s.Owner.PlayerKey;
                        sr.register = false;
                        sr.shipid = s.UniqueID;

                        GlobalNetwork.SendToAll(sr);
                    }
                }
                for (int index = 0; index < selected_ships.Count; index++)
                {
                    Ship ship = selected_ships[index];
                    pickedmap.AddShip(ref ship);

                    RegisterShipToMap sr = new RegisterShipToMap();
                    sr.mapId = pickedmap.MapKey;
                    sr.playerid = ship.Owner.PlayerKey;
                    sr.register = true;
                    sr.shipid = ship.UniqueID;

                    GlobalNetwork.SendToAll(sr);
                }
        }
        }
        private void GalaxyController_map_added(Map mp)
        {
            mp.Subscribe(ShipChange);
        }
        private void ShipChange(Map context, Ship ship, bool added)
        {
            const int pad = 10;
            if (ship.Owner.PlayerKey == GalaxyController.CurrentPlayer.PlayerKey)
            {
                if (added)
                {
                    DrawableShip ds = new DrawableShip()
                    {
                        ID = ship.UniqueID,
                        Class = ship.Class,
                        Texture = ship.Filename
                    };
                    ShipButton last = ship_buttons.LastOrDefault();
                    Squid.Rectangle rect = new Squid.Rectangle(Position.x + (Size.x - 200), 0, Position.x + Size.x, 100);
                    if (last != null)
                    {
                        rect = new Squid.Rectangle(last.Position, last.Size);
                        
                        rect.y1 += last.Size.y + pad;
                        rect.y2 += last.Size.y + pad;
                    }

                    Acharis.Game_Elements.Rectangle bounds = null;
                    using (var dplanets = GalaxyController.Planets)
                    {
                        foreach(var plan in Controls)
                        {
                            PlanetButton butt = plan as PlanetButton;
                            if(butt != null && butt.Map == context.MapKey)
                            {
                                bounds = new AGE.Rectangle(butt.Position.x, butt.Position.y, butt.Size.x, butt.Size.y);
                                break;
                            }
                        }
                    }
                    ShipButton sb = new ShipButton(rect, ds);
                    sb.OnMouseClick += new MouseClickEventHandler(
                        delegate(Control c, MouseEventArgs a)
                        {
                            if (a.Button == 0)
                            {
                                if (active_ship_buttons.Contains(sb))
                                    active_ship_buttons.Remove(sb);
                                else
                                    active_ship_buttons.Add(sb);
                                sb.DrawLinkedMapBounds = bounds;
                                sb.Toggled = !sb.Toggled;
                            }
                        });
                    ship_buttons.Add(sb);
                    Controls.Add(sb);
                }
                else
                {
                    foreach(ShipButton sb in ship_buttons)
                    {
                        if (sb.ShipID == ship.UniqueID)
                        {
                            active_ship_buttons.Remove(sb);
                            ship_buttons.Remove(sb);
                            Controls.Remove(sb);
                            break;
                        }
                    }
                    Squid.Point running_pos = new Squid.Point(Size.x - 200, 0);
                    foreach (ShipButton sb in ship_buttons)
                    {
                        sb.Position = running_pos;
                        sb.Size = new Squid.Point(200, 100);
                        running_pos = new Squid.Point(sb.Position.x, sb.Position.y + 200 + pad);
                    }
                }
            }
        }

        private object transferLinesLock = new object();
        private List<Transfer> transferLines = new List<Transfer>();
        protected override void OnUpdate()
        {
            base.OnUpdate();
            lock (transferLinesLock)
            {
                foreach (Transfer trans in transferLines)
                {
                    if (trans.time < DateTime.UtcNow)
                    {
                        trans.Complete();
                    }
                }
                transferLines.RemoveAll(x => x.complete);
            }
            using (var maps = GalaxyController.Maps)
            {
                foreach (Map mp in maps.Get().Values)
                {
                    mp.Update();
                }
            }
            if (painter != null && !_updated)
            {
                painter.Update(this, null);
                _updated = true;
            }
        }
        protected override void BackDraw()
        {
            base.BackDraw();
            if (painter != null)
            {
                painter.Draw(null, ZOrder.Background);
                painter.Draw(null, ZOrder.Foreground);
                _updated = false;
            }
        }
        protected override void DrawTopElements()
        {
            base.DrawTopElements();

            if (painter != null)
            {
                painter.Draw(null, ZOrder.TopMost);
                _updated = false;
            }
        }
        public void AddMyDrawables(ref Queue<AGE.DrawableComponent> draws)
        {
            lock (transferLinesLock)
            {
                foreach (Transfer trans in transferLines)
                    trans.AddMyDrawables(ref draws);
            }
            boundGalaxyMap.AddMyDrawables(ref draws);
            foreach (ShipButton sb in ship_buttons)
            {
                sb.AddMyDrawables(ref draws);
            }

            if (racialBacking.Count > 0)
            {
                foreach(BoundedComponent comp in racialBacking)
                    draws.Enqueue(comp);
            }
            if (current_fleet != null)
            {
                draws.Enqueue(new MetaTexture()
                {
                    Texture = selected_fleet_backing,
                    Bounds = new AGE.Rectangle(current_fleet.Position.x - 8, current_fleet.Position.y - 8, current_fleet.Size.x + 16, current_fleet.Size.y + 16)
                });
            }
            if (hovered_planet != null)
            {
                float scale = hovered_planet.Size.x / 128.0f, width = 150 * scale, height = 200 * scale;
                draws.Enqueue(new MetaTexture()
                {
                    Texture = hovered_planet_backing,
                    Bounds = new AGE.Rectangle(hovered_planet.Position.x, (int)(hovered_planet.Position.y + hovered_planet.Size.y - height), (int)width, (int)height),
                    SizeScale = scale,
                    zorder = ZOrder.TopMost
                });
            }
        }
    }
}
