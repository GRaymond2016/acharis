﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis_GUIElements.MainMenuComponents;
using Acharis.Game_Elements.Combat;

namespace Acharis_GUIElements.GalaxyMapComponents
{
    class PlanetInfoPopup : RootWindow
    {
        private static AcharisConfig config = new AcharisConfig();
        private Point ParentSize;

        private Label PName;
        private Label Description;
        private Label Population;
        private Label Owner;
        private Label Income;
        private Label Type;
        private SmallerButton Enter;
        private SmallerButton ShipMarket;

        public PlanetInfoPopup(Point parentSize)
        {
            DrawBack = false;
            ParentSize = parentSize;
            ControlStyle style = new ControlStyle();
            style.Default.Texture = config["Popup\\FrameOpaque"];
            style.Hot.Texture = config["Popup\\FrameOpaque"];
            style.Pressed.Texture = config["Popup\\FrameOpaque"];
            style.Opacity = 0.4f;
            GuiHost.AddStyle("planetBack", style);
            Style = "planetBack";
        }
        public void FormatMyself(Rectangle planetBounds, Planet info)
        {
            int width = ParentSize.x / 6, height = (int)((double)ParentSize.y / 7);
            Size = new Point(width, height);
            Position = new Point(planetBounds.x1 + planetBounds.Width / 2, (planetBounds.y1 + (planetBounds.Height / 2)) - (height / 2));
            Visible = false;

            PName = new Label();
            Description = new Label();
            Population = new Label();
            Owner = new Label();
            Income = new Label();
            Type = new Label();
            Enter = new SmallerButton();
            ShipMarket = new SmallerButton();

            Enter.Style = ShipMarket.Style = SmallerButton.StyleName;

            int planetOverlap = planetBounds.Width / 2;
            Rectangle availBounds = new Rectangle(new Point(planetOverlap + 10, 10), new Point(width - planetOverlap - 15, height - 15));


            int buttonSegment = ((availBounds.Height / 3) / 2) + 5;
            int normalSegment = (availBounds.Height - buttonSegment);

            PName.Size = new Point(availBounds.Width / 2, (int)(normalSegment / 0.5));
            Description.Size = new Point(availBounds.Width / 2, (int)(normalSegment / 2.5));
            Owner.Size = Income.Size = Type.Size = Population.Size = new Point(availBounds.Width / 2, normalSegment / 4);
            Enter.Size = ShipMarket.Size = new Point((int)(availBounds.Width / 2), buttonSegment - 5);

            PName.Position = new Point(availBounds.x1, availBounds.y1);
            Description.Position = new Point(availBounds.x1, availBounds.y1 + PName.Size.y);
            
            int newX = PName.Position.x + PName.Size.x + 5;
            Population.Position = new Point(newX, availBounds.y1);
            Owner.Position = new Point(newX, availBounds.y1 + Population.Size.y);
            Income.Position = new Point(newX, availBounds.y1 + Owner.Size.y + Population.Size.y);
            Type.Position = new Point(newX, availBounds.y1 + Owner.Size.y + Income.Size.y + Population.Size.y);

            Enter.Position = new Point(((availBounds.Width / 3) / 2), Description.Position.y + Description.Size.y + 5);
            ShipMarket.Position = new Point(Enter.Position.x + Enter.Size.x + 10, Enter.Position.y);

            PName.TextWrap = Description.TextWrap = Population.TextWrap = Owner.TextWrap = Income.TextWrap = Type.TextWrap = true;
            PName.Text = "Planet Name" + Environment.NewLine + info.Name;
            Description.Text = "Description" + Environment.NewLine + info.Description;
            Population.Text = "Pop.: " + info.Population.ToString();

            Owner.Text = null;
            if (info.Owner > 0)
            {
                try
                {

                    string race = Enum.GetName(typeof(Race), info.Owner);
                    Owner.Text = "Owner: " + race;
                }
                catch
                {
                    Owner.Text = null;
                }
            }
            if(Owner.Text == null)
                Owner.Text = "Owner: " + "Neutral";

            Income.Text = "Income: " + info.Income.ToString();
            Type.Text = "Type: " + info.Type.ToString();

            Enter.Text = "Planet Map";
            ShipMarket.Text = "Buy Ships";

            Controls.Add(PName);
            Controls.Add(Description);
            Controls.Add(Population);
            Controls.Add(Owner);
            Controls.Add(Income);
            Controls.Add(Type);

            Enter.OnMouseDown += new MouseDownEventHandler(
                delegate
                {
                    List<PlanetMap> planets = (Parent as GalaxyMap).Planets;
                    for (int i = 0; i < planets.Count; i++)
                    {
                        if (planets[i].PlanetName != null && planets[i].PlanetName == info.Name)
                        {
                            Parent.Visible = false;
                            planets[i].Visible = true;
                            planets[i].BringToFront();
                        }
                        else
                            planets[i].Visible = false;
                    }
                });
            ShipMarket.OnMouseDown += new MouseDownEventHandler(
                delegate
                {
                    
                });

            Controls.Add(Enter);
            Controls.Add(ShipMarket);
        }
        protected override bool CanBDraw()
        {
            return Background != null;
        }
        protected override void BackDraw()
        {
            GuiHost.Renderer.DrawTexture(GuiHost.Renderer.GetTexture(Background), this.Position.x, this.Position.y, this.Size.x, this.Size.y, new UVCoords(), 0);
        }
    }
}
