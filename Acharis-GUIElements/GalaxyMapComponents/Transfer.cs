﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;
using Squid;
using Acharis.Core_Elements;

namespace Acharis_GUIElements.GalaxyMapComponents
{
    class Transfer : ISupplyDrawables
    {
        public Line line;
        public Button start_button;
        public Button end_button;
        public DateTime start;
        public DateTime time;
        public bool complete;
        public string sprite;

        private object final_list = new object();
        private List<Action> finalized = new List<Action>();

        public Transfer(string sprite)
        {
            this.sprite = sprite;
        }

        public void AddAction(Action a)
        {
            lock (final_list)
            {
                if (!complete)
                    finalized.Add(a);
            }
        }
        public void Complete()
        {
            lock (final_list)
            {
                foreach (Action a in finalized)
                {
                    try
                    {
                        a.Invoke();
                    }
                    catch (Exception e)
                    {
                        Logger.Write(e, Logger.LogType.ERROR);
                    }
                }
            }
            complete = true;
        }
        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            string time_s = (time - DateTime.UtcNow).ToString("m\\:ss");
            FloatingText text = new FloatingText();
            text.Text = time_s;
            text.Point = AttachmentPoint.Bottom;
            double duration = (time - start).TotalMilliseconds,
                   totaldur = (DateTime.UtcNow - start).TotalMilliseconds;
            float perccomplete = (float)(totaldur / duration);
            if (perccomplete < 99)
            {
                float rot = (float) MovementAlgorithm.CalculateRotationBetweenPoints(line.StartPoint, line.EndPoint);
                Acharis.Game_Elements.Point center = new Acharis.Game_Elements.Point(line.StartPoint.X, line.StartPoint.Y);
                center.TranslateByMagnitude(rot, line.StartPoint.DistanceTo(line.EndPoint) * perccomplete);
                MetaTexture meta = new MetaTexture()
                {
                    Bounds = new Acharis.Game_Elements.Rectangle(center.X, center.Y, 20, 20),
                    Texture = sprite,
                    SizeScale = 1.0f,
                    Rotation = rot
                };
                text.Attachment = meta;
                text.Color = line.Color;
                draws.Enqueue(line);
                draws.Enqueue(text);
                draws.Enqueue(meta);
            }
        }
    }
}
