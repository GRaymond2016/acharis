﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Game_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Core_Elements;
using Acharis_GUIElements.MultiUseComponents;
using Acharis.Core_Elements.Network;

namespace Acharis_GUIElements
{
    public class PlanetMap : RootWindow, ISupplyDrawables
    {
        private static List<PlanetMap> CurrentPlanets = new List<PlanetMap>(); 
        //Dependancies
        private CanvasBoundsController canvas = null;
        private static CanvasTransformer trans = new CanvasTransformer();
        private static IPaintShipCanvas painter;

        private static Random r = new Random();

        //Generic Variables
        public event Action OnQuit;
        private GalaxyPlanetBinding Binding;
        public Map boundMap { get; private set; }
        private bool _updated;
        private bool selected_ship;

        //Arrow key variables
        private TimeSpan outThresh = new TimeSpan(0, 0, 0, 0, 300);
        private TimeSpan inThresh = new TimeSpan(0, 0, 0, 0, 100);
        private int[] keyCount = new int[4];
        private DateTime[] lastTime = new DateTime[4];
        private bool[] activated = new bool[4];

        //Mouse selection variables
        private const int rotationDifference = 30;
        private Acharis.Game_Elements.Point start = new Acharis.Game_Elements.Point(-1, -1);
        private Acharis.Game_Elements.Point end = new Acharis.Game_Elements.Point(-1, -1);
        private Acharis.Game_Elements.Rectangle selectionRect = new Acharis.Game_Elements.Rectangle(-1, -1, -1, -1);
        private Acharis.Game_Elements.Point rightdownstart = new Acharis.Game_Elements.Point();
        private Button back = null;
        private Toolbar toolbar = null;
        private bool ctrlPressed = false;
        private bool shiftPressed = false;
        private bool lMouseUp = false;

        private Squid.Point lastSize = new Squid.Point(0, 0);

        //Properties
        public string PlanetName { get { return Binding.AssociatedPlanet.Name; } }
        private Acharis.Game_Elements.Rectangle CurrentBounds
        {
            get { return new Acharis.Game_Elements.Rectangle(Position.x, Position.y, Size.x, Size.y); }
        }
        private CanvasTransformer Transformer { get { SetTransBounds(); return trans; } }
        private void SetTransBounds()
        {
            trans.ViewableMapBounds = canvas.VisibleBounds;
            trans.DrawBounds = CurrentBounds;
            trans.TotalMapBounds = boundMap.Bounds;
        }
        public static IPaintShipCanvas Painter 
        { 
           set 
           { 
               painter = value;
           } 
        }

        public PlanetMap(GalaxyPlanetBinding info, Map associatedMap)
        {
            lock (CurrentPlanets) {
                CurrentPlanets.Add(this);
            }
            _updated = false;
            Binding = info;
            boundMap = associatedMap;

            toolbar = new Toolbar(boundMap.MapName + "PMToolbar", MultiUseComponents.Anchor.Bottom);

            if(boundMap == null)
                Logger.WriteLine("Cannot locate map: " + info.AssociatedMap, Logger.LogType.CRITICAL);

            canvas = new CanvasBoundsController(boundMap.Bounds.Width, boundMap.Bounds.Height, (double)GraphicsOptions.Resolution.Y / GraphicsOptions.Resolution.X);

            ControlStyle backButtonStyle = new ControlStyle();
            backButtonStyle.Default.Tint = 0;
            backButtonStyle.Hot.Tint = 1;
            backButtonStyle.Pressed.Tint = 2;
            backButtonStyle.Default.Texture = backButtonStyle.Pressed.Texture = backButtonStyle.Hot.Texture = AppConfig["PlanetMap\\BackButton"];
            backButtonStyle.TextAlign = Alignment.MiddleCenter;
            backButtonStyle.Grid = new Squid.Margin(0);
            backButtonStyle.Tiling = TextureMode.Grid;
            GuiHost.AddStyle("BackButton", backButtonStyle);
            
            back = new Button();
            back.Style = "BackButton";
            back.Size = new Squid.Point((int)(206 * 0.75), (int)((177 / 3) * 0.75));
            back.OnMousePress += new MouseDownEventHandler(delegate
            {
                if (OnQuit != null)
                    OnQuit();
            });
            Controls.Add(back);

            int fleetPos = -1;
            GalaxyController.CurrentPlayer.Subscribe(delegate(Fleet f)
            {
                Button fleet = new Button();
                fleet.Style = "SmallButton";
                fleet.Size = new Squid.Point(206, 177 / 3);
                fleet.Text = f.Name;
                fleet.OnMousePress += new MouseDownEventHandler(delegate
                {
                    PlanetMap pl = null;
                    lock (CurrentPlanets)
                    {
                        pl = CurrentPlanets.FirstOrDefault(x => x.boundMap.MapKey == f.Location.MapKey);
                    }
                    if (pl != null)
                        DefaultDesktop.ScreenOrder.Push(pl);
                });
                toolbar.AddControl(fleet, fleetPos);
            });

            Background = boundMap.Background;

            boundMap.ControlIncome = info.AssociatedPlanet.Income;

            AllowFocus = true;
            BringToFront();

            foreach (Control c in Controls)
            {
                c.OnKeyDown += new KeyEventHandler(KeyDown);
                c.OnKeyUp += new KeyEventHandler(KeyUp);
            }

            this.NoEvents = false;
            this.Modal = true;
            this.Resizable = false;
            this.State = ControlState.Selected;
            this.Visible = true;

            OnKeyDown += new KeyEventHandler(KeyDown);
            OnKeyUp += new KeyEventHandler(KeyUp);

            EventHandler mouseDown = new EventHandler(MouseDown);
            OnGotFocus += new FocusEventHandler(
                delegate 
                {
                    GuiHost.OnMouseDown += mouseDown; 
                });
            OnLostFocus += new FocusEventHandler(
                delegate 
                {
                    GuiHost.OnMouseDown -= mouseDown; 
                });
        }
        private void ResetActive()
        {
            for (int i = 0; i < activated.Length; i++)
                activated[i] = false;
        }
        private void DecrementKey(int i)
        {
            if (activated[i]) return;
            TimeSpan tt = DateTime.Now - lastTime[i];
            if (tt > outThresh && keyCount[i] > 1) keyCount[i] -= (int)(tt.Ticks / outThresh.Ticks);
            if (keyCount[i] < 1) keyCount[i] = 1;
        }
        private void IncrementKey(int i)
        {
            TimeSpan tt = DateTime.Now - lastTime[i];
            if (tt > inThresh)
                keyCount[i] += 10;
            lastTime[i] = DateTime.Now;
            activated[i] = true;
        }
        private int GetKey(int i)
        {
            return keyCount[i];
        }
        private void MouseDown(object c, EventArgs args)
        {
           switch (GuiHost.MouseButton(0))
           { 
                case ButtonState.Down:
                    {
                        start.X = GuiHost.MousePosition.x;
                        start.Y = GuiHost.MousePosition.y;
                        lMouseUp = false;
                        break;
                    }
                case ButtonState.Press:
                    {
                        end.X = GuiHost.MousePosition.x;
                        end.Y = GuiHost.MousePosition.y;

                        int tsx = Math.Min(start.X, end.X);
                        int tsy = Math.Min(start.Y, end.Y);
                        int tex = Math.Max(start.X, end.X);
                        int tey = Math.Max(start.Y, end.Y);

                        selectionRect = new Acharis.Game_Elements.Rectangle(tsx, tsy, tex - tsx, tey - tsy);
                        break;
                    }
                case ButtonState.Up:
                    {
                        lMouseUp = true;
                        break;
                    }
                default:
                    break;
            }
            if (selected_ship)
            {
                switch (GuiHost.MouseButton(1))
                {
                    case ButtonState.Down:
                        {
                            rightdownstart = new Acharis.Game_Elements.Point(GuiHost.MousePosition.x, GuiHost.MousePosition.y);
                            break;
                        }
                    case ButtonState.Up:
                        {
                            float rotation = -1;
                            if (Math.Abs(rightdownstart.X - GuiHost.MousePosition.x) > rotationDifference ||
                                Math.Abs(rightdownstart.Y - GuiHost.MousePosition.y) > rotationDifference)
                            {
                                Acharis.Game_Elements.Point rotPoint = new Acharis.Game_Elements.Point(GuiHost.MousePosition.x, GuiHost.MousePosition.y);
                                rotation = (float)MovementAlgorithm.CalculateRotationBetweenPoints(rightdownstart, rotPoint);
                            }
                            boundMap.MoveToPoint(Transformer.TransformPointToMap(rightdownstart), GuiHost.ShiftPressed, rotation);
                            rightdownstart = new Acharis.Game_Elements.Point();
                            break;
                        }
                    default:
                        break;
                }
            }
        }
        private void KeyUp(Control c, KeyEventArgs args)
        {
            for (int j = 0; j < GuiHost.KeyBuffer.Length; j++)
            {
                switch (GuiHost.KeyBuffer[j].Key)
                {
                    case Keys.LEFTCONTROL: goto case Keys.RIGHTCONTROL;
                    case Keys.RIGHTCONTROL:
                        ctrlPressed = false;
                        break;
                    case Keys.LEFTSHIFT: goto case Keys.RIGHTSHIFT;
                    case Keys.RIGHTSHIFT:
                        shiftPressed = false;
                        break;
                }
            }
        }
        private void KeyDown(Control c, KeyEventArgs args)
        {
            ResetActive();
            for (int j = 0; j < GuiHost.KeyBuffer.Length; j++)
            {
                switch (GuiHost.KeyBuffer[j].Key)   
                {
                    case Keys.ESCAPE:
                        if (OnQuit != null)
                            OnQuit();
                        break;
                    case Keys.NUMPAD4:
                        canvas.PanLeft(GetKey(0));
                        IncrementKey(0);
                        break;
                    case Keys.NUMPAD6:
                        canvas.PanRight(GetKey(1));
                        IncrementKey(1);
                        break;
                    case Keys.NUMPAD8:
                        canvas.PanUp(GetKey(2));
                        IncrementKey(2);
                        break;
                    case Keys.NUMPAD2:
                        canvas.PanDown(GetKey(3));
                        IncrementKey(3);
                        break;
                    case Keys.LEFTCONTROL: goto case Keys.RIGHTCONTROL;
                    case Keys.RIGHTCONTROL:
                        ctrlPressed = true;
                        break;
                    case Keys.LEFTSHIFT: goto case Keys.RIGHTSHIFT;
                    case Keys.RIGHTSHIFT:
                        shiftPressed = true;
                        break;
                }
            }
            for (int i = 0; i < 4; i++)
                DecrementKey(i);
        }
        protected override bool CanBDraw()
        {
            if (Background != null)
                return true;
            
            return false;
        }
        protected override void OnUpdate()
        {
            base.OnUpdate();

            if (lastSize != Size)
            {
                toolbar.Show(this, Align.Mid, 20);
                back.Position = new Squid.Point(0, Size.y - back.Size.y);
                lastSize = Size;
            }

            //Control Scroll level
            Acharis.Game_Elements.Point mousepos_view = new Acharis.Game_Elements.Point(GuiHost.MousePosition.x, GuiHost.MousePosition.y);
            Acharis.Game_Elements.Point mousepos_map = Transformer.TransformPointToMap(mousepos_view);
            if (Squid.GuiHost.MouseScroll == -1)
                canvas.ZoomIn(mousepos_map);
            else if (Squid.GuiHost.MouseScroll == 1)
                canvas.ZoomOut(mousepos_map);

            //Control selection of units
            if (selectionRect.X >= 0 && selectionRect.Y >= 0 && selectionRect.Width >= 0 && selectionRect.Height >= 0)
            {
                if (selectionRect.Width + selectionRect.Height > 20)
                    boundMap.SelectionBounds = Transformer.TransformRectToMap(selectionRect);
                else
                {
                    if (lMouseUp)
                    {
                        boundMap.SelectionPoint = Transformer.TransformPointToMap(selectionRect.Center);
                        boundMap.SelectInverseMode = ctrlPressed;
                    }
                }

                if (lMouseUp)
                    selectionRect = new Acharis.Game_Elements.Rectangle(-1, -1, -1, -1);
            }

            if (boundMap.CountSelectedShips > 0)
                selected_ship = true;
            else
            {
                selected_ship = false;
                rightdownstart = new Acharis.Game_Elements.Point();
            }

            if (painter != null && !_updated)
            {
                painter.Update(this, Transformer);
                _updated = true;
            }

            boundMap.Update();
        }   
        protected override void BackDraw()
        {
            base.BackDraw();

            if (painter != null)
            {
                painter.Draw(Transformer, ZOrder.Background);
                painter.Draw(Transformer, ZOrder.Foreground);
                _updated = false;
            }
        }

        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            if (this.Background != null)
            {
                CutBackground tex = new CutBackground();
                tex.Texture = this.Background;
                draws.Enqueue(tex);
            }
            if (selectionRect.Valid)
            {
                Box bt = new Box()
                {
                    Bounds = Transformer.TransformRectToMap(selectionRect),
                    //         A B G R
                    Color = unchecked((uint)((50 << 24) + (150 << 16) + (150 << 8) + 150)),
                    SizeScale = 1.0f
                };
                draws.Enqueue(bt);
            }
            if (rightdownstart.Valid)
            {
                uint col = unchecked((uint)((50 << 24) + (0 << 16) + (0 << 8) + 150));
                if (Math.Abs(rightdownstart.X - GuiHost.MousePosition.x) > rotationDifference ||
                    Math.Abs(rightdownstart.Y - GuiHost.MousePosition.y) > rotationDifference)
                    col = unchecked((uint)((50 << 24) + (0 << 16) + (150 << 8) + 0));
                Line lt = new Line()
                {
                    StartPoint = Transformer.TransformPointToMap(rightdownstart),
                    EndPoint = Transformer.TransformPointToMap(new Acharis.Game_Elements.Point(GuiHost.MousePosition.x, GuiHost.MousePosition.y)),
                    SizeScale = 1.0f,
                    Color = col
                };
                draws.Enqueue(lt);
            }
            boundMap.AddMyDrawables(ref draws);
        }
    }
}
