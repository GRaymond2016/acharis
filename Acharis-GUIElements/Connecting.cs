﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Network;
using System.Threading;
using Acharis.Game_Elements.GalaxyMapNamespace;
using achg = Acharis.Game_Elements;
using Acharis_GUIElements.MultiUseComponents;
using Acharis_GUIElements.MainMenuComponents;
using System.Security.Cryptography;

namespace Acharis_GUIElements
{
    class Connecting : RootWindow
    {
        private ConnectionStates m_currentState;
        private Exception e;
        private PopupDialog failed = null;

        private Thread thread;
        private string text;

        private Label loadingText = new Label();
        private Button backButton;

        private string User;
        private byte[] Pass;
        private string addr;

        private TimeSpan timeout = new TimeSpan(0, 0, 30);

        public Connecting(Squid.Point startingSize, string Address, string user, string pass)
        {
            this.Size = startingSize;
            Background = AppConfig["GalaxyMap\\Background"];

            addr = Address;
            User = user;
            if (User == "")
                User = GlobalNetwork.LocalName;
            byte[] tPass = Encoding.ASCII.GetBytes(pass);
            Pass = MD5.Create().ComputeHash(tPass);

            Initialize();

            thread = new Thread(new ThreadStart(DoConnect));
            thread.Start();
        }
        protected override void Initialize()
        {
            base.Initialize();

            if (Size.x == 0 || Size.y == 0)
                return;

            loadingText = new Label();
            backButton = new SmallerButton();

            loadingText.Position = new Point(0, Size.y - 40);
            loadingText.Size = new Point(Size.x, 40);

            backButton.Size = new Point(250, 50);
            backButton.Position = new Point(Size.x - backButton.Size.x - 10, Size.y - backButton.Size.y - 10);
            backButton.OnMouseUp += new MouseUpEventHandler(ReturnToMain);

            backButton.Style = SmallerButton.StyleName;

            backButton.Text = "Return to Main Menu";
            loadingText.Text = "Attempting to get a connection to the server";

            Controls.Add(backButton);
            Controls.Add(loadingText);
        }
        private void DoConnect()
        {         
            DateTime start = DateTime.Now;
            ConnectionController network = new ConnectionController();
            network.m_connection_event += new GlobalNetwork.NetworkEvent(ConnectionHandler);
            for (int i = 0; i < 5; i++)
            {
                text = "Retrying connection to " + addr + " - Attempt " + (i + 1);
                m_currentState = ConnectionStates.NotStarted;
                if (network.Connect(addr))
                {
                    while (!network.Connected && m_currentState == ConnectionStates.NotStarted)
                    {
                        if (DateTime.Now > start + timeout)
                        {
                            network.Close();
                            ErrorOccured("Timeout", "Timeout occured when attempting to connect to the server, please check your internet connection.");
                            return;
                        }
                        Thread.Sleep(50);
                    }
                    switch (m_currentState)
                    {
                        case ConnectionStates.Failed: continue;
                        case ConnectionStates.FailedT1: continue;
                        default: break;
                    }
                    break;
                }
                    
                Thread.Sleep(1000);
            }
            if (m_currentState == ConnectionStates.NotStarted)
            {
                network.Close();
                ErrorOccured("Connection Failed", "After 5 retried attempts the server was still not responding. The server may be down, or your connection to it could be unstable.");
                return;
            } 
            if (m_currentState == ConnectionStates.Failed)
            {
                network.Close();
                ErrorOccured(e.GetType().ToString(), e.Message);
                return;
            }

            network.ReleaseToGlobal();
            text = "Connected to server " + addr;

            CheckUser cu = new CheckUser();
            cu.password = Pass;
            cu.username = User;
            GlobalNetwork.SendToAll(cu);
            Logger.WriteLine("Connecting: Sent user authentication.", Logger.LogType.INFO);

            while (InitSync.Instance.FilesDownloaded != InitSync.Instance.TotalFiles || InitSync.Instance.TotalFiles == 0)
            {
                if (DateTime.Now > start + timeout)
                {
                    network.Close();
                    ErrorOccured("Timeout", "Timeout occured when attempting to connect to the server, please check your internet connection.");
                }

                if (InitSync.Instance.TotalFiles != 0)
                {
                    int perc = (int)Math.Round((double)InitSync.Instance.FilesDownloaded / (double)InitSync.Instance.TotalFiles);
                    text = "Downloading Files ... " + perc + "% Done";
                }
                else
                    text = "Handshaking...";

                System.Threading.Thread.Sleep(100);
            }
            Logger.WriteLine("Connecting: Broken loop, Files all downloaded.", Logger.LogType.INFO);

            achg.Player me = new achg.Player();
            me.PlayerName = GlobalNetwork.LocalName;
            me.PlayerColor = achg.PlayerColours.Player1;
            using (var playl = GalaxyController.Players)
            {
                playl.Get().Add(me);
            }
            GlobalNetwork.SendToAll(me.PlayerStats);

            text = "Loading Map...";
            InitSync.Instance.LoadMap(GalaxyController.GalaxyMap, false);

            RequestInitData req = new RequestInitData();
            GlobalNetwork.SendToAll(req);

            GalaxyMap nextScreen = new GalaxyMap(Size);
            network.m_connection_event -= new GlobalNetwork.NetworkEvent(ConnectionHandler);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
        }
        private void ConnectionHandler(ConnectionStates connected, string sender, Exception e)
        {
            m_currentState = connected;
            this.e = e;
        }
        private void ErrorOccured(string title, string message)
        {
            failed = new PopupDialog(title, message, PopupDialog.Buttons.Back, new Point(Size.x / 4, Size.y / 4), Size.y / 2);
            failed.SetHandlers(new List<MouseUpEventHandler>() { new MouseUpEventHandler(ReturnToMain) });
            failed.Position = new Point(Size.x / 2 - (failed.Size.x / 2), Size.y / 2 - (failed.Size.y / 2));
            failed.Parent = this;
        }
        private void ReturnToMain(Control sender, MouseEventArgs arg)
        {
            //On Back button clicked we return to main menu
            MainMenu nextScreen = new MainMenu(this.Size);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
        }
        protected override void OnUpdate()
        {
            base.OnUpdate();
            loadingText.Text = text;
            if (failed != null)
            {
                loadingText.Visible = false;
                backButton.Visible = false;
                Controls.Add(failed);
                failed.Visible = true;
                failed.BringToFront();
            }
        }
    }
}
