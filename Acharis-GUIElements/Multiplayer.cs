﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis_GUIElements.MainMenuComponents;
using Acharis_GUIElements.MultiUseComponents;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Network;
using System.Threading;
using System.Net.Sockets;

namespace Acharis_GUIElements
{
    public class Multiplayer : RootWindow
    {
        private ListBox Servers;
        private ExtendedTextInput PasswordEntry;
        private ExtendedTextInput UserEntry;
        private const int itemsizey = 50;
        public int panelSizex = 1000;
        public int panelSizey = 800;

        public Multiplayer(Point startingSize)
        {
            this.Size = startingSize;
            Background = AppConfig["MainMenu\\Background"];

            panelSizex = startingSize.x / 3;
            panelSizey = startingSize.y / 3;

            ControlStyle vscrollTrackStyle = new ControlStyle();
            vscrollTrackStyle.Grid = new Margin(0, 3, 0, 3);
            vscrollTrackStyle.Hot.Texture = AppConfig["SharedResources\\ScrollBar"];
            vscrollTrackStyle.BackColor = Squid.ColorInt.FromArgb(0, 0, 0, 0);

            ControlStyle vscrollButtonStyle = new ControlStyle();
            vscrollButtonStyle.Grid = new Margin(0, 4, 0, 4);
            vscrollButtonStyle.Texture = AppConfig["SharedResources\\ScrollBar"];
            vscrollButtonStyle.Hot.Texture = AppConfig["SharedResources\\ScrollBar"];
            vscrollButtonStyle.Pressed.Texture = AppConfig["SharedResources\\ScrollBar"];

            ControlStyle vscrollUp = new ControlStyle();
            vscrollUp.Default.Texture = AppConfig["SharedResources\\ScrollArrowUp"];
            vscrollUp.Hot.Texture = AppConfig["SharedResources\\ScrollArrowUpSel"];
            vscrollUp.Pressed.Texture = AppConfig["SharedResources\\ScrollArrowUpSel"];
            vscrollUp.Focused.Texture = AppConfig["SharedResources\\ScrollArrowUpSel"];

            ControlStyle vscrollDown = new ControlStyle();
            vscrollDown.Default.Texture = AppConfig["SharedResources\\ScrollArrowDown"];
            vscrollDown.Hot.Texture = AppConfig["SharedResources\\ScrollArrowDownSel"];
            vscrollDown.Pressed.Texture = AppConfig["SharedResources\\ScrollArrowDownSel"];
            vscrollDown.Focused.Texture = AppConfig["SharedResources\\ScrollArrowDownSel"];

            ControlStyle frameStyle2 = new ControlStyle();
            frameStyle2.Grid = new Margin(2);
            frameStyle2.Texture = AppConfig["Popup\\FrameOpaque"];
            frameStyle2.TextPadding = new Margin(2);

            ControlStyle backStyle = new ControlStyle();
            backStyle.Texture = AppConfig["Popup\\Frame"];

            ControlStyle itemStyle = new ControlStyle();
            itemStyle.Grid = new Margin(6);
            itemStyle.Pressed.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedPressed.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.Focused.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedFocused.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.Selected.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.SelectedHot.Texture = AppConfig["SharedResources\\TextBox"];
            itemStyle.TextPadding = new Margin(10, 0, 10, 0);

            GuiHost.AddStyle("itemStyle", itemStyle);
            GuiHost.AddStyle("backStyle", backStyle);
            GuiHost.AddStyle("vscrollTrack", vscrollTrackStyle);
            GuiHost.AddStyle("vscrollButton", vscrollButtonStyle);
            GuiHost.AddStyle("vscrollUp", vscrollUp);
            GuiHost.AddStyle("vscrollDown", vscrollDown);
            GuiHost.AddStyle("frame", frameStyle2);
            Initialize();
        }
        private object item_lock = new object();
        protected void addServer(string server)
        {
            ListBoxItem lm = new ListBoxItem();
            lm.Name = server;
            lm.Style = "itemStyle";
            lm.Text = server;
            lm.Size = new Point(Servers.Size.x, itemsizey);
            lm.Margin = new Margin(0, 0, 0, 2);
            lock (item_lock)
            {
                Servers.Items.Add(lm);
            }
        }
        protected override void Initialize()
        {
            base.Initialize();

            //This function is called twice, once before the constructor, with uninitialized size variables
            //and once at the end of the constructor, it should only construct after size is set by the constructor.
            if (Size.x == 0 || Size.y == 0)
                return;

            Servers = new ListBox();
            UserEntry = new ExtendedTextInput();
            PasswordEntry = new ExtendedTextInput();
            Label User = new Label();
            Label Password = new Label();
            SmallerButton Login = new SmallerButton();
            SmallerButton Register = new SmallerButton();
            SmallerButton Back = new SmallerButton();
            SmallerButton Refresh = new SmallerButton();

            Panel backdrop = new Panel();

            int leftBoundBrowse = (Size.x / 2) - (panelSizex / 2);
            int rightBoundBrowse = (Size.x / 2) + (panelSizex / 2), 
                leftBoundLogin = rightBoundBrowse;
            int rightBoundLogin = (Size.x / 2) + (panelSizex / 2);
            int upperBound = (Size.y / 2) - (panelSizey / 2);
            Point textSize = new Point(200, 20);
            Point inputSize = new Point(200, 20);
            int gaps = ((rightBoundLogin - leftBoundLogin) / 3);
            int buttonSizey = 30;

            backdrop.Size = new Point(panelSizex, panelSizey);
            backdrop.Position = new Point(leftBoundBrowse, upperBound);

            backdrop.Style = "backStyle";
            backdrop.AllowFocus = false;
            backdrop.NoEvents = true;

            Servers.Position = new Point(leftBoundBrowse + 30, upperBound + 40);
            Servers.Size = new Point(rightBoundBrowse - leftBoundBrowse - 60, panelSizey - 100);
            
            Servers.Parent = this;
            Servers.Style = "frame";
            Servers.Margin = new Margin(2, 2, 2, 2);
            Servers.ClipFrame.Margin = new Margin(8, 8, 0, 8);

            Servers.Scrollbar.Margin = new Margin(0, 8, 12, 8);
            Servers.Scrollbar.ButtonUp.Style = "vscrollUp";
            Servers.Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
            Servers.Scrollbar.ButtonUp.OnMouseDown += new MouseDownEventHandler(ButtonUp_OnMouseDown);
            Servers.Scrollbar.ButtonDown.Style = "vscrollDown";
            Servers.Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);
            Servers.Scrollbar.ButtonDown.OnMouseDown += new MouseDownEventHandler(ButtonDown_OnMouseDown);
            Servers.Scrollbar.Slider.Style = "vscrollTrack";
            Servers.Scrollbar.Slider.Button.Style = "vscrollButton";
            Servers.Scrollbar.OnValueChanged += new ValueChangedEventHandler(Scrollbar_OnValueChanged);

            Servers.Anchor = AnchorStyles.Left | AnchorStyles.Top;

            refreshServer(null, null);        

            Servers.Scrollbar.Scale = (float)(Servers.Items.Count * itemsizey) / (float)(Servers.Size.y + Servers.ClipFrame.Margin.Horizontal);
            Servers.Visible = true;

            Back.Size = new Point((int)(Servers.Size.x / 3), buttonSizey);
            Back.Position = new Point(backdrop.Position.x + 20, backdrop.Position.y + backdrop.Size.y - (Back.Size.y + 20));

            Refresh.Size = new Point((int)(Servers.Size.x / 3), buttonSizey);
            Refresh.Position = new Point(backdrop.Position.x + backdrop.Size.x / 2 - (Refresh.Size.x / 2), backdrop.Position.y + backdrop.Size.y - (Back.Size.y + 20));

            User.Position = new Point(leftBoundLogin, upperBound + panelSizey / 3);
            User.Size = textSize;
            Password.Position = new Point(leftBoundLogin, User.Position.y + 40);
            Password.Size = textSize;

            UserEntry.Position = new Point(User.Position.x + User.Size.x + 20, User.Position.y);
            UserEntry.Size = inputSize;
            PasswordEntry.Position = new Point(Password.Position.x + Password.Size.x + 20, Password.Position.y);
            PasswordEntry.Size = inputSize;

            Login.Size = new Point((int)(Servers.Size.x / 3), buttonSizey);
            Login.Position = new Point(backdrop.Position.x + (backdrop.Size.x - Login.Size.x - 40), backdrop.Position.y + backdrop.Size.y - (Login.Size.y + 20));
            
            Register.Position = new Point((UserEntry.Position.x + (UserEntry.Size.x / 2)) - gaps / 2, PasswordEntry.Position.y + PasswordEntry.Size.y + 20);
            Register.Size = new Point(gaps, 40);

            Login.Style = Register.Style = Back.Style = Refresh.Style = SmallerButton.StyleName;

            User.Text = "Username";
            Password.Text = "Password";
            Back.Text = "Back";
            Login.Text = "Join Server";
            Register.Text = "Register";
            Refresh.Text = "Refresh";

            Controls.Add(backdrop);
            Controls.Add(Servers);
            Controls.Add(Refresh);
            Controls.Add(Back);
            Controls.Add(Login);

            Refresh.OnMouseUp += new MouseUpEventHandler(refreshServer);
            Back.OnMouseUp += new MouseUpEventHandler(Back_OnMousePress);
            Login.OnMouseUp += new MouseUpEventHandler(Connect);
        }
        private void refreshServer(Control sender, MouseEventArgs args)
        {
            lock (item_lock)
            {
                Servers.Items.Clear();
            }
            GlobalNetwork.GetServersASync(addServer);
        }
        private void ConnectToDiscoverableServer()
        {
            try
            {
                List<string> pinged_servers = new List<string>();
                GlobalNetwork.PingForActiveServers(delegate(string s) { pinged_servers.Add(s); });
                if (pinged_servers.Count > 0)
                {
                    ConnectToSource(pinged_servers[0], "", "");
                }
            }
            catch
            {
                Logger.WriteLine("Could not do udp broadcast to network.", Logger.LogType.WARN);
            }
        }
        private void Scrollbar_OnValueChanged(Control sender)
        {
            // scroll the text area
            Servers.Scroll = Servers.Scrollbar.Value;
        }
        private void ButtonUp_OnMouseDown(Control sender, MouseEventArgs arg)
        {
            float alter = (float)itemsizey / (float)(Servers.Size.y + Servers.ClipFrame.Margin.Horizontal);
            Servers.Scrollbar.Value -= alter;
        }
        private void ButtonDown_OnMouseDown(Control sender, MouseEventArgs arg)
        {
            float alter = (float)itemsizey / (float)(Servers.Size.y + Servers.ClipFrame.Margin.Horizontal);
            Servers.Scrollbar.Value += alter;
        }
        private void Back_OnMousePress(Control sender, MouseEventArgs arg)
        {
            if (TimeCheck()) return;

            MainMenu nextScreen = new MainMenu(this.Size);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
        }
        private void Connect(Control sender, MouseEventArgs arg)
        {
            if (TimeCheck()) return;

            if (Servers.SelectedItem == null)
                return;

            Enabled = false;
            PopupDialog dialog = new PopupDialog("Connecting... ", "Connecting... ", PopupDialog.Buttons.None, new Point(100, 50), 200);
            DefaultDesktop.ShowDialog(dialog);
            new Thread(new ThreadStart(delegate
            {
                try
                {
                    ConnectToSource(GlobalNetwork.ResolveIPAddress(Servers.SelectedItem.Text), UserEntry.Text, PasswordEntry.Text);
                    dialog.Close();
                }
                catch (SocketException e)
                {
                    Logger.Write(e, Logger.LogType.INFO);
                    DefaultDesktop.ShowDialog(new PopupDialog("Failed to connect", "Could not connect to server: " + e.Message, PopupDialog.Buttons.Ok, new Point(300, 200), 400));
                }
            })).Start();
        }
        private void ConnectToSource(string source, string user, string pass)
        {
            Connecting nextScreen = new Connecting(Size, source, user, pass);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
        }
        protected override bool CanBDraw()
        {
            return Background != null;
        }
        protected override void BackDraw()
        {
            GuiHost.Renderer.DrawTexture(GuiHost.Renderer.GetTexture(Background), this.Position.x, this.Position.y, this.Size.x, this.Size.y, new UVCoords(), 0);
        }
    }
}
