﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;

namespace Acharis_GUIElements.MainMenuComponents
{
    public class MenuButton : Button
    {
        public const string StyleName = "MenuButton";

        private static AcharisConfig config = new AcharisConfig();
        protected override void Initialize()
        {
            base.Initialize();
            string filePath = config["MainMenu\\Button"];

            ControlStyle mBStyle = new ControlStyle();
            mBStyle.Texture = filePath;
            mBStyle.Default.Texture = filePath;
            mBStyle.Default.Tint = 0;
            mBStyle.Pressed.Texture = filePath;
            mBStyle.Pressed.Tint = 1;
            mBStyle.Hot.Texture = filePath; //This means rollover image
            mBStyle.Hot.Tint = 2; //We set the tint to show the renderer which part of the "split" image we want.
            mBStyle.TextAlign = Alignment.MiddleCenter;
            mBStyle.Grid = new Squid.Margin(0);
            mBStyle.Tiling = TextureMode.Grid;

            GuiHost.AddStyle(StyleName, mBStyle);

            Cursor = Cursors.Select;
        }
    }
}
