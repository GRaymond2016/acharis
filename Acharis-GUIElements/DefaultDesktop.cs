﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;
using Acharis.Core_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Collections.Concurrent;
using Acharis_GUIElements.MultiUseComponents;

namespace Acharis_GUIElements
{
    public class DefaultDesktop : Desktop
    {
        private static AcharisConfig config = new AcharisConfig();
        public GuiState CurrentState { get; set; }

        public Object controlChangeMutex = new Object(); //All control modification should be synchronised on this mutex.

        public static ConcurrentStack<Control> ScreenOrder = new ConcurrentStack<Control>();
        private static RootWindow CurrentWindow;
        private static ConcurrentQueue<PopupDialog> Dialogs = new ConcurrentQueue<PopupDialog>();

        public static void ShowDialog(PopupDialog dialog)
        {
            Dialogs.Enqueue(dialog);
        }
        public DefaultDesktop(Point startingSize) : base()
        {
            MainMenu startingPoint = new MainMenu(startingSize);

            CurrentWindow = startingPoint;
            lock (controlChangeMutex)
            {
                Controls.Add(startingPoint);
            }

            CurrentState = GuiState.MainMenu;
        }
        protected override void Initialize()
        {
            base.Initialize();

            Point cursorSize = new Point(32, 32);
            Point halfSize = cursorSize / 2;

            lock (controlChangeMutex)
            {
                AddCursor(Cursors.Default, new Cursor { Texture = config["Cursors\\Default"], Size = cursorSize, HotSpot = Point.Zero, });
                AddCursor(Cursors.Select, new Cursor { Texture = config["Cursors\\Select"], Size = cursorSize, HotSpot = Point.Zero });
                AddCursor(Cursors.Link, new Cursor { Texture = config["Cursors\\VSplit"], Size = cursorSize, HotSpot = Point.Zero });
            }

            Cursor = Cursors.Default;
            ShowCursor = true;
        }
        protected override void OnUpdate()
        {
            base.OnUpdate();

            lock (controlChangeMutex)
            {
                Control next;
                if (ScreenOrder.TryPop(out next))
                {
                    Controls.RemoveAll(x => x.GetType() == typeof(PopupDialog));
                    Controls.Remove(CurrentWindow);
                    Controls.Add(next);
                    next.Visible = true;
                    next.Position = this.Position;
                    next.Size = this.Size;
                    next.BringToFront();
                    next.Focus();

                    CurrentWindow = (RootWindow)(Window)next;
                    CurrentState = (GuiState)Enum.Parse(typeof(GuiState), CurrentWindow.GetType().Name, true); //Relatively expensive operation -- consider revision
                }
                PopupDialog dialog;
                while (Dialogs.TryDequeue(out dialog))
                {
                    CurrentWindow.Enabled = false;

                    dialog.Position = new Point(CurrentWindow.Position.x + CurrentWindow.Size.x / 2 - dialog.Size.x / 2, CurrentWindow.Position.y + CurrentWindow.Size.y / 2 - dialog.Size.y / 2);
                    Controls.Add(dialog);

                    dialog.Enabled = true;
                    dialog.Visible = true;
                    dialog.BringToFront();
                    dialog.Focus();
                }
            }
        }
        public static void CloseDialog(PopupDialog dialog)
        {
            dialog.Visible = false;

            CurrentWindow.Enabled = true;
            CurrentWindow.Visible = true;
            CurrentWindow.BringToFront();
            CurrentWindow.Focus();
        }
        public static void DoCustomDraw()
        {
            CurrentWindow.DoCustomDraw();
        }
        public static void DoTopDraw()
        {
            CurrentWindow.DoTopDraw();
        }
    }
}
