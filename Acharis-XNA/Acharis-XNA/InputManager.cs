﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Squid;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using Acharis.Core_Elements;
using Acharis_GUIElements;
using System.Diagnostics;

namespace Acharis_XNA
{
    class InputManager : GameComponent
    {
        private static Stopwatch _stopwatch = Stopwatch.StartNew();
        private MouseState MState;

        private long[] FirstDownTimes = new long[255];
        private long[] LastTimes = new long[255];

        public int Interval { get; set; }
        public int Delay { get; set; }

        private int LastScroll;
        private List<Func<Squid.KeyData, bool>> m_listeners = new List<Func<KeyData,bool>>();
    
        public void AddKeyOverride(Func<Squid.KeyData, bool> @event)
        {
            lock(m_listeners)
            {
                m_listeners.Add(@event);
            }
        }

        public InputManager(Game game)
            : base(game)
        {
            Interval = 150 * 1000;
            Delay = 1200 * 1000;
        }

        public override void Update(GameTime gameTime)
        {
            MState = Mouse.GetState();

            int wheel = MState.ScrollWheelValue > LastScroll ? -1 : (MState.ScrollWheelValue < LastScroll ? 1 : 0);
            LastScroll = MState.ScrollWheelValue;

            Squid.GuiHost.SetMouse(MState.X, MState.Y, wheel);
            Squid.GuiHost.SetButtons(MState.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed, 
                                    MState.RightButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed);

            if (Squid.GuiHost.OnMouseDown != null)
                Squid.GuiHost.OnMouseDown(null, null);

            KeyboardState state = Keyboard.GetState();
            List<Microsoft.Xna.Framework.Input.Keys> pressed = new List<Microsoft.Xna.Framework.Input.Keys>(state.GetPressedKeys());
            List<Squid.KeyData> buffer = new List<Squid.KeyData>();

            if (MState.X < Game.GraphicsDevice.Viewport.Bounds.X + 20)
                pressed.Add(Microsoft.Xna.Framework.Input.Keys.Left);
            if (MState.Y < Game.GraphicsDevice.Viewport.Bounds.Y + 20)
                pressed.Add(Microsoft.Xna.Framework.Input.Keys.Up);
            if (MState.X > Game.GraphicsDevice.Viewport.Bounds.Right - 20)
                pressed.Add(Microsoft.Xna.Framework.Input.Keys.Right);
            if (MState.Y > Game.GraphicsDevice.Viewport.Bounds.Bottom - 20)
                pressed.Add(Microsoft.Xna.Framework.Input.Keys.Down);

            for (int i = 0; i < pressed.Count; i++)
            {
                int key = Convert.ToInt32(pressed[i]);
                LastTimes[key] = _stopwatch.ElapsedTicks;
                if (FirstDownTimes[key] == 0)
                {
                    FirstDownTimes[key] = LastTimes[key];

                    Squid.KeyData data = new Squid.KeyData();
                    data.Scancode = GuiRenderer.VirtualKeyToScancode((int)key);
                    data.Pressed = true;

                    bool cont = true;
                    lock (m_listeners)
                    {
                        foreach (Func<Squid.KeyData, bool> ft in m_listeners)
                        {
                            cont &= ft.Invoke(data);
                        }
                    }

                    if (cont)
                        buffer.Add(data);
                }

                if (FirstDownTimes[key] < _stopwatch.ElapsedTicks - Delay)
                {
                    Squid.KeyData data = new Squid.KeyData();
                    data.Scancode = GuiRenderer.VirtualKeyToScancode((int)key);
                    data.Pressed = true;

                    bool cont = true;
                    lock (m_listeners)
                    {
                        foreach (Func<Squid.KeyData, bool> ft in m_listeners)
                        {
                            cont &= ft.Invoke(data);
                        }
                    }

                    if (cont)
                        buffer.Add(data);
                }
            }
            for(int i = 0; i < LastTimes.Length; i++)
                if (LastTimes[i] < _stopwatch.ElapsedTicks - Interval && FirstDownTimes[i] != 0)
                {
                    FirstDownTimes[i] = 0;
                    LastTimes[i] = 0;

                    Squid.KeyData data = new Squid.KeyData();
                    data.Scancode = GuiRenderer.VirtualKeyToScancode((int)i);
                    data.Released = true;

                    bool cont = true;
                    lock(m_listeners)
                    {
                        foreach(Func<Squid.KeyData, bool> ft in m_listeners)
                        {
                            cont &= ft.Invoke(data);
                        }
                    }

                    if(cont)
                        buffer.Add(data);
                }

            Squid.GuiHost.SetKeyboard(buffer.ToArray());
            base.Update(gameTime);
        }
    }
}
