﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements;

namespace Acharis_XNA
{
    //Deprecated - GR 31/01/13
    class GameLoop : GameComponent
    {
        private float rotUpdateDelay = 15;
        private float moveUpdateDelay = 25;
        private float minupdatetime;

        public GameLoop(Game game)
            : base(game)
        {
            Enabled = false;
            minupdatetime = Math.Min(rotUpdateDelay, moveUpdateDelay);
        }
    }
}
