using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Squid;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Network;
using Acharis_Base.Core_Elements.Network;
using System.Windows.Forms;
using Acharis_GUIElements.MultiUseComponents;
using Acharis_GUIElements;

namespace Acharis_XNA
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class AcharisGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ConsoleManager consManager = new ConsoleManager();

        public AcharisGame()
        {
            GalaxyController.RunningGame = this;

            Client.Handler = new MessageHandlers();

            graphics = new GraphicsDeviceManager(this);
            
            GraphicsOptions.updateFromFile();

            switch (GraphicsOptions.Mode)
            {
                case 0: 
                    graphics.IsFullScreen = true; 
                    break;
                case 1: 
                    graphics.IsFullScreen = false; 
                    ((Form)Form.FromHandle(this.Window.Handle)).FormBorderStyle = FormBorderStyle.None; 
                    break;
                case 2: 
                    graphics.IsFullScreen = false;
                    ((Form)Form.FromHandle(this.Window.Handle)).FormBorderStyle = FormBorderStyle.Fixed3D; 
                    break;
            }

            graphics.PreferMultiSampling = GraphicsOptions.MultiSampling;

            graphics.PreferredBackBufferHeight = GraphicsOptions.Resolution.Y;
            graphics.PreferredBackBufferWidth = GraphicsOptions.Resolution.X;

            graphics.ApplyChanges();

            Content.RootDirectory = DefaultConfig.precursor + "Resources";
        }

        ~AcharisGame()
        {
            GlobalNetwork.Shutdown = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            GuiRenderer render = new GuiRenderer(this);
            GuiHost.Renderer = render;

            InputManager manager = new InputManager(this);
            Components.Add(manager);

            manager.AddKeyOverride(consManager.Listener);

            DrawableDesktop desktop = new DrawableDesktop(this);
            Components.Add(desktop);

            manager.AddKeyOverride(delegate(Squid.KeyData key) 
            {
                if (key.Key == Squid.Keys.F12 && key.Released == true)
                {
                    render.ClearCache();
                }
                return true;
            });
            

            GameLoop gm = new GameLoop(this);
            Components.Add(gm);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            base.LoadContent();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GuiHost.TimeElapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            GraphicsDevice.Clear(Microsoft.Xna.Framework.Color.WhiteSmoke);

            base.Draw(gameTime);
        }
    }
}
