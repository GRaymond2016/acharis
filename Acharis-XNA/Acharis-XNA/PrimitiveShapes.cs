﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Collections;
using Microsoft.Xna.Framework;

namespace Acharis_XNA
{
    //This needs to be optimized for caching of these shapes.
    public class PrimitiveCircle : PrimitiveShape
    {
        protected const float max = 2 * (float)Math.PI;

        public float Radius { get; set; }
        public int Sides { get; set; }
        public int Width { get; set; }

        public PrimitiveCircle(Texture2D graphicsDevice)
            : base(graphicsDevice)
        { }
        public PrimitiveCircle(float radius, int sides, int width, Texture2D graphicsDevice)
            : base(graphicsDevice)
        {
            Radius = radius;
            Sides = sides;
            Width = width;
            CreateCircle();
        }
        /// <summary>
        /// Creates a circle starting from 0, 0.
        /// </summary>
        /// <param name="radius">The radius (half the width) of the circle.</param>
        /// <param name="sides">The number of sides on the circle (the more the detailed).</param>
        public void CreateCircle()
        {
            vectors.Clear();

            float step = max / (float)Sides;
            for (int i = 0; i < Width; i++, Radius++)
            {
                for (float theta = 0; theta < max; theta += step)
                {
                    vectors.Add(new Vector2(Radius * (float)Math.Cos((double)theta),
                        Radius * (float)Math.Sin((double)theta)));
                }
                // then add the first vector again so it's a complete loop
                vectors.Add(new Vector2(Radius * (float)Math.Cos(0),
                        Radius * (float)Math.Sin(0)));
            }
        }
    }
    public class PrimitivePie : PrimitiveCircle
    {
        private double ToRadian(double deg)
        {
            return deg * (Math.PI / 180);
        }
        public PrimitivePie(Texture2D graphicsDevice)
            : base(graphicsDevice)
        {
        }
        public PrimitivePie(float radius, int sides, int width, List<Acharis.Game_Elements.Point> removedSegs, Texture2D graphicsDevice)
            : base(radius, sides, width, graphicsDevice)
        {
            CreateFanningCircle(removedSegs);
        }
        public void CreateFanningCircle(List<Acharis.Game_Elements.Point> removedSegs)
        {
            vectors.Clear();

            float step = max / (float)Sides;
            for (float theta = 0; theta < max; theta += step)
            {
                bool ignore = false;
                for (int j = 0; j < removedSegs.Count; ++j)
                {
                    Acharis.Game_Elements.Point p = removedSegs.ElementAt(j);
                    if (theta >= ToRadian(p.X) && theta <= ToRadian(p.Y))
                        ignore = true;
                }
                if (!ignore)
                {
                    vectors.Add(new Vector2(0, 0));
                    vectors.Add(new Vector2(Radius * (float)Math.Sin(theta), Radius * (float)Math.Cos((double)theta)));
                }
            }
        }
    }
    public class PrimitiveShape
    {
        protected Texture2D pixel;
        protected ArrayList vectors;

        /// <summary>
        /// Gets/sets the colour of the primitive line object.
        /// </summary>
        public Color Colour;

        /// <summary>
        /// Gets/sets the position of the primitive line object.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Gets/sets the render depth of the primitive line object (0 = front, 1 = back)
        /// </summary>
        public float Depth;

        /// <summary>
        /// Gets the number of vectors which make up the primtive line object.
        /// </summary>
        public int CountVectors
        {
            get
            {
                return vectors.Count;
            }
        }

        /// <summary>
        /// Creates a new primitive line object.
        /// </summary>
        /// <param name="graphicsDevice">The Graphics Device object to use.</param>
        public PrimitiveShape(Texture2D basis)
        {
            // create pixels
            pixel = basis;

            Colour = Color.White;
            Position = new Vector2(0, 0);
            Depth = 1;

            vectors = new ArrayList();
        }

        /// <summary>
        /// Adds a vector to the primive live object.
        /// </summary>
        /// <param name="vector">The vector to add.</param>
        public void AddVector(Vector2 vector)
        {
            vectors.Add(vector);
        }

        /// <summary>
        /// Insers a vector into the primitive line object.
        /// </summary>
        /// <param name="index">The index to insert it at.</param>
        /// <param name="vector">The vector to insert.</param>
        public void InsertVector(int index, Vector2 vector)
        {
            vectors.Insert(index, vectors);
        }

        /// <summary>
        /// Removes a vector from the primitive line object.
        /// </summary>
        /// <param name="vector">The vector to remove.</param>
        public void RemoveVector(Vector2 vector)
        {
            vectors.Remove(vector);
        }

        /// <summary>
        /// Removes a vector from the primitive line object.
        /// </summary>
        /// <param name="index">The index of the vector to remove.</param>
        public void RemoveVector(int index)
        {
            vectors.RemoveAt(index);
        }

        /// <summary>
        /// Clears all vectors from the primitive line object.
        /// </summary>
        public void ClearVectors()
        {
            vectors.Clear();
        }

        /// <summary>
        /// Renders the primtive line object.
        /// </summary>
        /// <param name="spriteBatch">The sprite batch to use to render the primitive line object.</param>
        public void Render(SpriteBatch spriteBatch)
        {
            if (vectors.Count < 2)
                return;

            for (int i = 1; i < vectors.Count; i++)
            {
                Vector2 vector1 = (Vector2)vectors[i - 1];
                Vector2 vector2 = (Vector2)vectors[i];

                // calculate the distance between the two vectors
                float distance = Vector2.Distance(vector1, vector2);

                // calculate the angle between the two vectors
                float angle = (float)Math.Atan2((double)(vector2.Y - vector1.Y),
                    (double)(vector2.X - vector1.X));

                // stretch the pixel between the two vectors
                spriteBatch.Draw(pixel,
                    Position + vector1,
                    null,
                    Colour,
                    angle,
                    Vector2.Zero,
                    new Vector2(distance, 1),
                    SpriteEffects.None,
                    Depth);
            }
        }

    }
}
