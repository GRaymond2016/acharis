using System;
using Acharis_ErrorReportingBase;
using System.Diagnostics;
using Acharis.Core_Elements;
using Microsoft.Win32;
using Acharis.Core_Elements.Network;

namespace Acharis_XNA
{
    class XNANotInstalledException : Exception
    {
        public XNANotInstalledException(string message) : base(message) { }
    }

#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            try
            {
                using (AcharisGame game = new AcharisGame())
                {
                    game.Exiting += new EventHandler<EventArgs>(game_Exiting);
                    game.Run();
                }
            }
            catch(Exception e)
            {
                ExceptionThrown(e);
            }
            GlobalNetwork.Stop();
        }
        public static void game_Exiting(object sender, EventArgs args)
        {
            GlobalNetwork.Stop();
        }
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            if (args.IsTerminating)
                ExceptionThrown((Exception)args.ExceptionObject);
        }
        private static void ExceptionThrown(Exception args)
        {
            string exe = System.Environment.GetEnvironmentVariable("ACH_ERROR_PATH", EnvironmentVariableTarget.Process) ?? "";
            exe += "Acharis Error Reporter.exe";
            string id = ExceptionLoader.WriteException(args);
            try
            {
                Process.Start(exe, id);
            }
            catch (Exception ex)
            {
                Logger.WriteLine("Could not launch " + exe + "." +
                    Environment.NewLine + ex.ToString() +
                    Environment.NewLine + "An exception was not recorded: " + args.ToString(), Logger.LogType.CRITICAL);
            }
        }

    }
#endif
}

