﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Core_Elements;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Content;

namespace Acharis_XNA
{
    /// <summary>
    /// This class is heavily taken from the samples supplied with SQUID. Minor alterations include using filestreams to retrieve textures.
    /// </summary>
    class GuiRenderer : DefaultConfig, Squid.ISquidRenderer
    {
        [DllImport("user32.dll")]
        private static extern int GetKeyboardLayout(int dwLayout);
        [DllImport("user32.dll")]
        private static extern int GetKeyboardState(ref byte pbKeyState);
        [DllImport("user32.dll", EntryPoint = "MapVirtualKeyEx")]
        private static extern int MapVirtualKeyExA(int uCode, int uMapType, int dwhkl);
        [DllImport("user32.dll")]
        private static extern int ToAsciiEx(int uVirtKey, int uScanCode, ref byte lpKeyState, ref short lpChar, int uFlags, int dwhkl);

        private static int KeyboardLayout;
        private static byte[] KeyStates;

        private Dictionary<int, SpriteFont> Fonts = new Dictionary<int, SpriteFont>();
        private Dictionary<string, int> FontLookup = new Dictionary<string, int>();
        private Dictionary<int, Texture2D> Textures = new Dictionary<int, Texture2D>();
        private Dictionary<string, int> TextureLookup = new Dictionary<string, int>();
        private Dictionary<string, Squid.Font> FontTypes = new Dictionary<string, Squid.Font>();
        private Squid.Font DefaultFont = new Squid.Font();

        private List<int> split = new List<int>();

        private SpriteBatch Batch;

        private int FontIndex;
        private int TextureIndex;
        private Texture2D BlankTexture;

        private RasterizerState Rasterizer;
        private SamplerState Sampler;
        private CanvasDrawer Drawer;

        public void ClearCache()
        {
            Textures.Clear();
            TextureLookup.Clear();
        }
        public GuiRenderer(Game game)
        {
            Batch = new SpriteBatch(game.GraphicsDevice);

            Drawer = new CanvasDrawer(Batch);
            Acharis_GUIElements.PlanetMap.Painter = Drawer;
            Acharis_GUIElements.GalaxyMap.Painter = Drawer;

            BlankTexture = new Texture2D(game.GraphicsDevice, 1, 1);
            BlankTexture.SetData<Color>(new Color[] { new Color(255, 255, 255, 255) });

            FontTypes.Add("Arial8", new Squid.Font { Name = "Arial10", Family = "Arial", Size = 8, Bold = true, International = true });
            FontTypes.Add(Squid.Font.Default, new Squid.Font { Name = "Arial10", Family = "Arial", Size = 10, Bold = true, International = true });
            FontTypes.Add("Arial12", new Squid.Font { Name = "Arial10", Family = "Arial", Size = 12, Bold = true, International = true });
            FontTypes.Add("Arial14", new Squid.Font { Name = "Arial14", Family = "Arial", Size = 14, Bold = true, International = true });
            FontTypes.Add("AcharisFont", new Squid.Font { Name = "AcharisFont", Family = "Bickham Script Pro Regular", Size = 14, Bold = true, International = true });

            KeyboardLayout = GetKeyboardLayout(0);
            KeyStates = new byte[0x100];

            Rasterizer = new RasterizerState();
            Rasterizer.ScissorTestEnable = true;

            Sampler = new SamplerState();
            Sampler.Filter = TextureFilter.PointMipLinear;
        }
        public static int VirtualKeyToScancode(int key)
        {
            return MapVirtualKeyExA(key, 0, KeyboardLayout);
        }
        public bool TranslateKey(int code, ref char character)
        {
           return GuiRenderer.sTranslateKey(code, ref character);
        }
        public static bool sTranslateKey(int code, ref char character)
        {
            short lpChar = 0;

            int result = ToAsciiEx(MapVirtualKeyExA(code, 1, KeyboardLayout), code, ref KeyStates[0], ref lpChar, 0, KeyboardLayout);
            if (result == 1)
            {
                character = (char)((ushort)lpChar);
                return true;
            }

            return false;
        }
        private Color ColorFromtInt32(int color)
        {
            Byte[] bytes = BitConverter.GetBytes(color);

            return new Color(bytes[2], bytes[1], bytes[0], bytes[3]);
        }
        public int GetTexture(string name)
        {
            if (TextureLookup.ContainsKey(name))
                return TextureLookup[name];

            Texture2D texture;
            try
            {
                using (Stream st = new FileStream(name, FileMode.Open))
                    texture = Texture2D.FromStream(GalaxyController.RunningGame.GraphicsDevice, st);
            }
            catch
            {
                Logger.WriteLine("Could not find texture: " + name, Logger.LogType.CRITICAL);
                texture = BlankTexture;
            }
 
            TextureIndex++;
            if (name.Contains("Split"))
                split.Add(TextureIndex);

            TextureLookup.Add(name, TextureIndex);
            Textures.Add(TextureIndex, texture);

            return TextureIndex;
        }
        public int GetFont(string name)
        {
            if (FontLookup.ContainsKey(name))
                return FontLookup[name];

            if (!FontTypes.ContainsKey(name))
                return -1;

            Squid.Font type = FontTypes[name];
            SpriteFont font = null;
            try
            {
                font = GalaxyController.RunningGame.Content.Load<SpriteFont>(type.Name);
            }
            catch
            {
                Logger.WriteLine("Could not load font: " + type.Name, Logger.LogType.ERROR);
                font = GalaxyController.RunningGame.Content.Load<SpriteFont>(FontTypes[Squid.Font.Default].Name);
            }
            FontIndex++;

            FontLookup.Add(name, FontIndex);
            Fonts.Add(FontIndex, font);

            return FontIndex;
        }
        public Squid.Point GetTextSize(string text, int font)
        {
            if (string.IsNullOrEmpty(text))
                return new Squid.Point();
            if (!Fonts.ContainsKey(font))
                font = Fonts.First().Key;
            SpriteFont f = Fonts[font];
            Vector2 size = f.MeasureString(text);
            return new Squid.Point((int)size.X, (int)size.Y);
        }
        public Squid.Point GetTextureSize(int texture)
        {
            Texture2D tex = Textures[texture];
            return new Squid.Point(tex.Width, tex.Height);
        }
        public void DrawBox(int x, int y, int w, int h, int color)
        {
            Rectangle destination = new Rectangle(x, y, w, h);
            Batch.Draw(BlankTexture, destination, destination, ColorFromtInt32(color));
        }
        public void DrawText(string text, int x, int y, int font, int color)
        {
            if (!Fonts.ContainsKey(font)) 
                return;

            SpriteFont f = Fonts[font];
            Batch.DrawString(f, text, new Vector2(x, y), ColorFromtInt32(color));
        }
        public void DrawTexture(int texture, int x, int y, int w, int h, Squid.UVCoords uv, int color)
        {
            if (!Textures.ContainsKey(texture))
                return;

            Texture2D tex = Textures[texture];

            Rectangle destination = new Rectangle(x, y, w, h);
            Rectangle source = new Rectangle();

            source.X = (int)((float)(tex.Width) * uv.U1);
            source.Width = (int)((float)((tex.Width) * uv.U2)) - source.X;

            //This allows any image that is a "split" image (1 file, 3 images - used for buttons, default, rollover, down)
            //To be drawn with just its current state selected. The Tint trait is used to allow this. State 0,1,2 for Tint.
            if (split.Contains(texture))
            {
                int m = (tex.Height / 3);
                source.Y = m * color;
                source.Height = (int)(m * uv.V2);
            }
            else
            {
                source.Y = (int)((float)(tex.Height) * uv.V1);
                source.Height = (int)((float)((tex.Height) * uv.V2)) - source.Y;
            }

            Batch.Draw(tex, destination, source, Color.White);
        }
        public void Scissor(int x1, int y1, int x2, int y2)
        {
            int width = x2 - x1, height = y2 - y1;
            Rectangle viewable = GalaxyController.RunningGame.GraphicsDevice.Viewport.Bounds;

            int xa = (x1 < viewable.X) ? viewable.X : x1;
            int ya = (y1 < viewable.Y) ? viewable.Y : y1;
            int xb = (x2 > viewable.X + viewable.Width) ? x2 = viewable.X + viewable.Width : x2;
            int yb = (y2 > viewable.Y + viewable.Height) ? y2 = viewable.Y + viewable.Height : y2;

            Rectangle rect = new Rectangle(xa, ya, xb - xa, yb - ya);
            GalaxyController.RunningGame.GraphicsDevice.ScissorRectangle = rect;
        }
        public void StartBatch()
        {
            Batch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, Sampler, null, Rasterizer);
        }
        public void EndBatch(bool final)
        {
            Batch.End();
        }
        public void Dispose() { }
    }
}
