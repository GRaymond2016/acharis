﻿using System.Collections.Generic;
using System.IO;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Network;
using Acharis_GUIElements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sq = Squid;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis_GUIElements.MultiUseComponents;
using System;
using Squid;

namespace Acharis_XNA
{
    class DrawableDesktop : DrawableGameComponent
    {
        object draw_lock = new object();
        AcharisConfig config = new AcharisConfig();
        Dictionary<GuiState, Texture2D> Backdrops = new Dictionary<GuiState, Texture2D>();
        DefaultDesktop guiDesktop;
        SpriteBatch backdrops;
        bool oneError = false;
        bool oneDisplayed = false;
        string lastErrorMessage = "No error occured.";

        private void ReturnToMain(Control sender, MouseEventArgs arg)
        {
            //On Back button clicked we return to main menu
            MainMenu nextScreen = new MainMenu(guiDesktop.Size);
            DefaultDesktop.ScreenOrder.Push(nextScreen);
            oneError = false;
            oneDisplayed = false;
        }
        public void NetworkDisconnect(ConnectionStates state, string sender, Exception e)
        {
            if (!oneError)
            {
                oneError = true;
                lastErrorMessage = e.Message;
            }
        }
        ~DrawableDesktop()
        {
            GlobalNetwork.m_disconnection_event -= new GlobalNetwork.NetworkEvent(NetworkDisconnect);
        }
        public DrawableDesktop(Game game) : base(game)
        {
            guiDesktop = new DefaultDesktop(new Sq::Point(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height));
            GlobalNetwork.m_disconnection_event += new GlobalNetwork.NetworkEvent(NetworkDisconnect);
        }
        public override void Initialize()
        {
            base.Initialize();

            backdrops = new SpriteBatch(GraphicsDevice);
            guiDesktop.ShowCursor = true;
        }
        public override void Update(GameTime gameTime)
        {
            if (Present(GuiState.Exit))
            {
                GlobalNetwork.Shutdown = true;
                this.Game.Exit();
            }

            if (!Present(GuiState.MainMenu) && Present(GuiState.Multiplayer) && Present(GuiState.PopupDialog) &&
                oneError == true && oneDisplayed == false)
            {
                PopupDialog failed = new PopupDialog("Connection Failed", lastErrorMessage, PopupDialog.Buttons.Back,
                                        new Squid.Point(guiDesktop.Size.x / 4, guiDesktop.Size.y / 4), guiDesktop.Size.y / 2);
                failed.SetHandlers(new List<MouseUpEventHandler>() { new MouseUpEventHandler(ReturnToMain) });
                failed.Position = new Squid.Point(guiDesktop.Size.x / 2 - (failed.Size.x / 2), guiDesktop.Size.y / 2 - (failed.Size.y / 2));
                failed.Parent = guiDesktop;

                // TODO separate view modification from game state to unwind convolutions such as this.
                lock (guiDesktop.controlChangeMutex)
                {
                    guiDesktop.Controls.Add(failed);
                }
                failed.Visible = true;
                failed.BringToFront();

                oneDisplayed = true;
            }
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            lock (draw_lock)
            {
                lock (guiDesktop.controlChangeMutex)
                {
                    if (guiDesktop.Size.x != Game.GraphicsDevice.Viewport.Width || guiDesktop.Size.y != Game.GraphicsDevice.Viewport.Height)
                        guiDesktop.Size = new Squid.Point(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);

                    try
                    {
                        guiDesktop.Update();
                    }
                    catch (Exception e)
                    {
                        Logger.Write(e, Logger.LogType.CRITICAL);
                    }

                    GuiHost.Renderer.StartBatch();
                    try
                    {
                        DefaultDesktop.DoCustomDraw(); // We do a custom draw which draws Backgrounds / Elements.
                    }
                    catch (Exception e)
                    {
                        Logger.Write(e, Logger.LogType.CRITICAL);
                    }
                    GuiHost.Renderer.EndBatch(true);

                    try
                    {
                        guiDesktop.Draw(); //We then do a full draw which will draw GUI elements with Squid.
                    }
                    catch (Exception e)
                    {
                        GuiHost.Renderer.EndBatch(false);
                        Logger.Write(e, Logger.LogType.CRITICAL);
                    }

                    GuiHost.Renderer.StartBatch();
                    try
                    {
                        DefaultDesktop.DoTopDraw(); // We do a custom draw which draws Backgrounds / Elements.
                    }
                    catch (Exception e)
                    {
                        Logger.Write(e, Logger.LogType.CRITICAL);
                    }
                    GuiHost.Renderer.EndBatch(true);

                    base.Draw(gameTime);
                }
            }
        }
        private bool Present(GuiState tester)
        {
            if ((guiDesktop.CurrentState & tester) == tester)
                return true;
            return false;
        }
    }
}
