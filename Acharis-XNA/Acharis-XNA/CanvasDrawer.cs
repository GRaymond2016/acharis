﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;
using Microsoft.Xna.Framework.Graphics;
using mcx = Microsoft.Xna.Framework;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.IO;
using System.Xml.Linq;
using Acharis.Core_Elements;

namespace Acharis_XNA
{
    enum FrameType
    {
        Calculated,
        Fixed
    }
    static class RectangleExt
    {
        public static mcx.Rectangle ToMCXRect(this Rectangle rectangle)
        {
            return new mcx.Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }
    }
    class AnimatedTextureMetaData
    {
        public int FrameHeight { get; set; }
        public int FrameCount { get; set; }
        public int FrameDelayInMs { get; set; }
        public FrameType FrameType { get; set; }

        public bool Populate(XDocument doc)
        {
            XElement main = doc.Element("Animation");
            if (main != null)
            {
                XElement count = main.Element("FrameCount"), 
                         delay = main.Element("FrameIntervalInMs");
                if (count != null && delay != null)
                {
                    FrameCount = int.Parse(count.Value);
                    FrameDelayInMs = int.Parse(delay.Value);

                    XElement height = main.Element("FrameHeight");
                    if (height != null)
                    {
                        FrameType = FrameType.Fixed;
                        FrameHeight = int.Parse(height.Value);
                    }
                    else
                        FrameType = FrameType.Calculated;
                    return true;
                }
            }
            return false;
        }
    }
    //ShipDrawer should control all drawing of non gui objects.
    //It controls multiple canvases, so essentially you can have multiple maps as different "canvases", but both can display on the same screen
    //Independant of each other (or on multiple monitors too). There are two aspects to a canvas: - Bounds - Visible Bounds. 
    //Everything is drawn on the bounds, and then when a "capture" happens, aka the user wants a segment of the map, visible bounds is their bounds.
    //Scaling happens to the bounds size, and then is scaled based on zoom level for the player.
    class CanvasDrawer : IPaintShipCanvas
    {
        private Dictionary<string, Texture2D> Textures = new Dictionary<string, Texture2D>();
        private Dictionary<string, AnimatedTextureMetaData> AnimationData = new Dictionary<string, AnimatedTextureMetaData>();
        private SpriteBatch Batch = null;
        private SpriteFont font;
        private Texture2D BlankTex;
        private mcx.Color m_tCol;
        private mcx.Color m_sCol;
        private mcx.Color m_pCol;
        private Queue<DrawableComponent> m_todraw = new Queue<DrawableComponent>();
        private Dictionary<ZOrder, Queue<PrimitiveShape>> m_prims = new Dictionary<ZOrder, Queue<PrimitiveShape>>();

        public CanvasDrawer(SpriteBatch Batch)
        {
            this.Batch = Batch;
            font = GalaxyController.RunningGame.Content.Load<SpriteFont>("Arial10");
            BlankTex = new Texture2D(Batch.GraphicsDevice, 1, 1);
            BlankTex.SetData<mcx.Color>(new mcx.Color[] { mcx.Color.White });
        }
        public void ClearCache()
        {
            Textures.Clear();
            AnimationData.Clear();
        }
        public uint TextColour
        {
            get { return DecodeColour(m_tCol); }
            set { m_tCol = GetColour(value); }
        }
        public uint SelectionColour
        {
            get { return DecodeColour(m_sCol); }
            set { m_sCol = GetColour(value); }
        }
        public uint PathColour
        {
            get { return DecodeColour(m_pCol); }
            set { m_pCol = GetColour(value); }
        }
        private mcx.Color GetColour(uint col)
        {
            return new mcx.Color((int)(col & 0xff), (int)(col >> 8 & 0xff), (int)(col >> 16 & 0xff), (int)(col >> 24 & 0xff));
        }
        private uint DecodeColour(mcx.Color col)
        {
            return (uint)col.R + (uint)(col.G << 8) + (uint)(col.B << 16) + (uint)(col.A << 24);
        }
        private Rectangle FindBounds(BoundedComponent tex, Texture2D baseTex, int frames)
        {
            if (tex.Bounds != null || tex == null || tex.Attachment == null)
                return tex.Bounds;

            if (tex.Attachment.Bounds == null)
                return FindBounds(tex.Attachment, baseTex, frames);

            int halfx = (int)((baseTex.Bounds.Width / 2) * tex.SizeScale), halfy = (int)(((baseTex.Bounds.Height / frames) / 2) * tex.SizeScale);
            Point center = tex.Attachment.Bounds.Center;
            return new Rectangle(center.X - halfx, center.Y - halfy, halfx * 2, halfy * 2);
        }
        public void Update(ISupplyDrawables draw, CanvasTransformer trans)
        {
            m_todraw.Clear();
            m_prims.Clear();
            draw.AddMyDrawables(ref m_todraw);

            foreach(DrawableComponent comp in m_todraw)
            {
                switch (comp.Type)
                {
                    case DrawType.Box:
                        if (trans != null)
                        {
                            Box bx = comp as Box;
                            bx.TransformedBounds = trans.TransformRectToDraw(bx.Bounds);
                        }
                        break;
                    case DrawType.Animation:
                    case DrawType.Texture:
                        {
                            MetaTexture mtex = comp as MetaTexture;
                            if (!Path.IsPathRooted(mtex.Texture))
                            {
                                string precursor = System.Environment.GetEnvironmentVariable("ACH_RES_PATH", EnvironmentVariableTarget.User) ?? "";
                                mtex.Texture = precursor + mtex.Texture;
                            }

                            Texture2D texture = null;
                            if (!Textures.ContainsKey(mtex.Texture))
                            {
                                
                                try
                                {
                                    using (Stream st = new FileStream(mtex.Texture, FileMode.Open))
                                        texture = Texture2D.FromStream(Batch.GraphicsDevice, st);
                                    texture.Name = Path.GetFileName(mtex.Texture);
                                    Textures[mtex.Texture] = texture;
                                }
                                catch (IOException)
                                {
                                    Logger.WriteLine("Could not find file: " + mtex.Texture, Logger.LogType.ERROR);
                                }
                            } 
                            else 
                                texture = Textures[mtex.Texture];

                            AnimatedTextureMetaData tex = new AnimatedTextureMetaData() { FrameCount = 1, FrameDelayInMs = 500, FrameType = FrameType.Calculated };
                            if (comp.Type == DrawType.Animation)
                            {
                                if (!AnimationData.ContainsKey(mtex.Texture))
                                {
                                    string meta = mtex.Texture.Replace(Path.GetExtension(mtex.Texture), ".xml");
                                    bool notfound = true;
                                    try
                                    {
                                        if (File.Exists(meta))
                                        {
                                            using (FileStream fs = File.OpenRead(meta))
                                            {
                                                XDocument data = XDocument.Load(fs);
                                                if (tex.Populate(data))
                                                {
                                                    AnimationData[mtex.Texture] = tex;
                                                    notfound = false;
                                                }
                                            }
                                        }
                                    }
                                    catch (IOException)
                                    {
                                        Logger.WriteLine("Could not find file: " + meta, Logger.LogType.ERROR);
                                    }

                                    if (notfound == true)
                                        AnimationData[mtex.Texture] = tex;
                                }
                                else
                                    tex = AnimationData[mtex.Texture];
                            }

                            if (texture != null)
                                mtex.Bounds = FindBounds(mtex, texture, tex.FrameCount);

                            if (trans != null)
                                mtex.TransformedBounds = trans.TransformRectToDraw(mtex.Bounds);
                            else
                                mtex.TransformedBounds = mtex.Bounds;

                            break;
                        }
                    case DrawType.Circle:
                        Circle cc = comp as Circle;
                        PrimitiveCircle pm;
                        Point p = cc.Bounds.Center;
                        if (cc.Attachment == null)
                        {
                            pm = new PrimitiveCircle(Math.Max(cc.Bounds.Width, cc.Bounds.Height) * cc.SizeScale / 2.0f, cc.Points, cc.Width, BlankTex);
                            if (trans != null)
                                p = trans.TransformPointToDraw(cc.Bounds.Center);
                        }
                        else
                        {
                            pm = new PrimitiveCircle(Math.Max(cc.Attachment.Bounds.Width, cc.Attachment.Bounds.Height) * cc.Attachment.SizeScale / 2.0f, cc.Points, cc.Width, BlankTex);
                            p = cc.Attachment.Bounds.Center;
                        }
                        pm.Position = new mcx.Vector2(p.X, p.Y);
                        pm.Colour = GetColour(cc.Color);
                        if (!m_prims.ContainsKey(comp.zorder))
                            m_prims.Add(comp.zorder, new Queue<PrimitiveShape>());
                        m_prims[comp.zorder].Enqueue(pm);
                        break;
                    case DrawType.Pie:
                        PieShape shape = comp as PieShape;
                        PrimitivePie pie;
                        Point center = shape.Bounds.Center;
                        if (shape.Attachment == null)
                        {
                            if (trans != null)
                                shape.Bounds = trans.TransformRectToDraw(shape.Bounds);

                            pie = new PrimitivePie(Math.Max(shape.Bounds.Width, shape.Bounds.Height) * shape.SizeScale / 2.0f, shape.Points, 1, shape.AnglesToRemove, BlankTex);
                            center = shape.Bounds.Center;
                        }
                        else
                        {
                            pie = new PrimitivePie(Math.Max(shape.Attachment.Bounds.Width, shape.Attachment.Bounds.Height) * shape.Attachment.SizeScale / 2.0f, shape.Points, 1, shape.AnglesToRemove, BlankTex);
                            center = shape.Attachment.Bounds.Center;
                        }
                        pie.Position = new mcx.Vector2(center.X, center.Y);
                        pie.Colour = GetColour(shape.Color);
                        if (!m_prims.ContainsKey(comp.zorder))
                            m_prims.Add(comp.zorder, new Queue<PrimitiveShape>());
                        m_prims[comp.zorder].Enqueue(pie);
                        break;
                    case DrawType.Line:
                        Line ln = comp as Line;
                        PrimitiveShape line = new PrimitiveShape(BlankTex);
                        if (trans != null)
                        {
                            ln.StartPoint = trans.TransformPointToDraw(ln.StartPoint);
                            ln.EndPoint = trans.TransformPointToDraw(ln.EndPoint);
                        }
                        line.AddVector(new mcx.Vector2(ln.StartPoint.X, ln.StartPoint.Y));
                        line.AddVector(new mcx.Vector2(ln.EndPoint.X, ln.EndPoint.Y));
                        line.Colour = GetColour(ln.Color);
                        if (!m_prims.ContainsKey(comp.zorder))
                            m_prims.Add(comp.zorder, new Queue<PrimitiveShape>());
                        m_prims[comp.zorder].Enqueue(line);
                        break;
                    case DrawType.Back:
                        CutBackground cb = comp as CutBackground;
                        if (!Textures.ContainsKey(cb.Texture))
                        {
                            Texture2D texture = null;
                            try
                            {
                                using (Stream st = new FileStream(cb.Texture, FileMode.Open))
                                    texture = Texture2D.FromStream(Batch.GraphicsDevice, st);
                            }
                            catch (IOException)
                            {
                                Logger.WriteLine("Could not find file: " + cb.Texture, Logger.LogType.ERROR);
                            }
                            if (texture != null)
                                Textures[cb.Texture] = texture;
                        }
                        break;
                    default: break;
                }
            }
        }
        public void Draw(CanvasTransformer trans, ZOrder order)
        {
            foreach (DrawableComponent draw in m_todraw.Where(x => x.Type == DrawType.Back))
            {
                DrawBack(draw as CutBackground, trans); break;
            }
            if (m_prims.ContainsKey(order))
                foreach (PrimitiveShape ps in m_prims[order])
                    DrawPrimitive(ps);
            for (int i = 0; i < m_todraw.Count;)
            {
                DrawableComponent cm = m_todraw.Dequeue();
                if (cm.zorder != order)
                {
                    m_todraw.Enqueue(cm);
                    ++i;
                    continue;
                }
                switch (cm.Type)
                {
                    case DrawType.Text: DrawText(cm as FloatingText); break;
                    case DrawType.Animation: DrawAnimation(cm as AnimatedTexture); break;
                    case DrawType.Texture: DrawTexture(cm as MetaTexture); break;
                    case DrawType.Box: DrawBox(cm as Box); break;
                    case DrawType.Back: break;
                    default: break;
                }
            }
        }
        private void DrawBack(CutBackground back, CanvasTransformer trans)
        {
            if (!Textures.ContainsKey(back.Texture))
                return;

            Texture2D tex = Textures[back.Texture];
            mcx.Rectangle drawPoint = new mcx.Rectangle(trans.DrawBounds.X, trans.DrawBounds.Y, trans.DrawBounds.Width, trans.DrawBounds.Height);

            double xzoomlevel = (float)trans.TotalMapBounds.Width / tex.Bounds.Width,
                   yzoomlevel = (float)trans.TotalMapBounds.Height / tex.Bounds.Height;

            //Our source rect is a subset of the background image, we essentially want to take the "visible rect" from the texture, where the textures full size is synonymous with the map size.
            mcx.Rectangle src = new mcx.Rectangle
                (
                    (int) Math.Round(trans.ViewableMapBounds.X / xzoomlevel),
                    (int) Math.Round(trans.ViewableMapBounds.Y / yzoomlevel),
                    (int) Math.Round(trans.ViewableMapBounds.Width / xzoomlevel),
                    (int) Math.Round(trans.ViewableMapBounds.Height / yzoomlevel)
                );
            Batch.Draw(tex, drawPoint, src, mcx.Color.White);
        }
        private void DrawPrimitive(PrimitiveShape c)
        {
            c.Render(Batch);
        }
        private void DrawText(FloatingText tx)
        {
            if (tx.Attachment != null)
            {
                const int padding = 2;
                mcx.Color col = GetColour(tx.Color);
                mcx.Vector2 tsize = font.MeasureString(tx.Text);

                Rectangle attachmentRect = tx.Attachment.TransformedBounds.ScaleByFactor(tx.Attachment.SizeScale);
                int ycent = 0;
                switch (tx.Point)
                {
                    case AttachmentPoint.Bottom: ycent = attachmentRect.Bottom; break;
                    case AttachmentPoint.Center: ycent = attachmentRect.Center.Y - (int)(tsize.Y / 2); break;
                    case AttachmentPoint.Top: ycent = attachmentRect.Y; break;
                }
                Batch.DrawString(font, tx.Text, new mcx.Vector2(attachmentRect.Center.X - tsize.X / 2, ycent + padding), col);
            }
        }
        private void DrawBox(Box bx)
        {
            if (bx.TransformedBounds != null)
            {
                bx.TransformedBounds.Width = (int)(bx.TransformedBounds.Width * bx.SizeScale);
                bx.TransformedBounds.Height = (int)(bx.TransformedBounds.Height * bx.SizeScale);
                Batch.Draw(BlankTex, bx.TransformedBounds.ToMCXRect(), GetColour(bx.Color));
            }
        }
        private void MissingResource(BoundedComponent component)
        {
            DrawBox(new Box()
            {
                Bounds = component.Bounds,
                Color = DecodeColour(mcx.Color.Red)
            });
        }
        private void DrawAnimation(AnimatedTexture texture)
        {
            if (texture.TransformedBounds != null)
            {
                if (!Textures.ContainsKey(texture.Texture))
                {
                    MissingResource(texture);
                    return;
                }

                Texture2D tex = Textures[texture.Texture];
                AnimatedTextureMetaData ani = AnimationData[texture.Texture];

                if (!AnimationData.ContainsKey(texture.Texture))
                    MissingResource(texture);

                long playtimems = (DateTime.Now.Ticks - texture.StartRender) / TimeSpan.TicksPerMillisecond;
                long frcount = playtimems / ani.FrameDelayInMs;
                long cpoint = (long)((((float)frcount / ani.FrameCount) - frcount / ani.FrameCount) * ani.FrameCount);

                mcx.Rectangle src;
                if (ani.FrameType == FrameType.Calculated)
                {
                    int size = (tex.Bounds.Height / ani.FrameCount);
                    src = new mcx.Rectangle(tex.Bounds.X, (int)(size * cpoint), tex.Bounds.Width, size);
                }
                else
                    src = new mcx.Rectangle(tex.Bounds.X, (int)(cpoint * ani.FrameHeight), tex.Bounds.Width, ani.FrameHeight);

                DrawGenericTexture(texture.TransformedBounds.ToMCXRect(), src, tex, texture.Rotation, texture.Transparency, texture.SizeScale);
            }
        }
        private void DrawTexture(MetaTexture texture)
        {
            if (texture.TransformedBounds != null)
            {
                if (!Textures.ContainsKey(texture.Texture))
                {
                    MissingResource(texture);
                    return;
                }

                Texture2D tex = Textures[texture.Texture];
                DrawGenericTexture(texture.TransformedBounds.ToMCXRect(), tex.Bounds, tex, texture.Rotation, texture.Transparency, texture.SizeScale);
            }
        }
        private void DrawGenericTexture(mcx.Rectangle destRect, mcx.Rectangle sourceRect, Texture2D texture, float rotation, byte transparency, double sizeScale)
        {
            int xdiff = (int)Math.Round(destRect.Width * sizeScale),
                ydiff = (int)Math.Round(destRect.Height * sizeScale);

            destRect.X = destRect.X + destRect.Width / 2 - xdiff / 2;
            destRect.Y = destRect.Y + destRect.Height / 2 - ydiff / 2;
            destRect.Width = xdiff;
            destRect.Height = ydiff;
            
            float xfac = (float) destRect.Width / sourceRect.Width, yfac = (float) destRect.Height / sourceRect.Height;
            destRect.X += (int) Math.Round(sourceRect.Width * xfac / 2.0f);
            destRect.Y += (int) Math.Round(sourceRect.Height * yfac / 2.0f);

            mcx.Vector2 origin = new mcx.Vector2(sourceRect.Width / 2, sourceRect.Height / 2); //FIXME: Hard coded origin, make configurable, consider % as opposed to point
            mcx.Color col = mcx.Color.White;
            if (transparency != 0)
            {
                col = mcx.Color.FromNonPremultiplied(255, 255, 255, transparency);
            }
            Batch.Draw(texture, destRect, sourceRect, col, (float)(rotation * (Math.PI / 180.0)), origin, SpriteEffects.None, 0);
        }

    }
}
