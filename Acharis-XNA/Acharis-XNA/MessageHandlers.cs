﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Configuration;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis_GUIElements;
using Acharis_GUIElements.MultiUseComponents;
using Microsoft.Win32;
using Squid;
using System;
using Acharis.Game_Elements.Combat;

namespace Acharis_Base.Core_Elements.Network
{
    class MessageHandlers : CoreMessageHandlers
    {
        static MessageHandlers()
        {
            GlobalNetwork.m_disconnection_event += new NetworkBase.NetworkEvent(Disconnect);
        }
        private static void Disconnect(ConnectionStates state, String error, Exception ex)
        {
            if (state == ConnectionStates.Ended)
            {
                ConnectionEnded("Disconnected", ex.Message);
            }
        }
        private static void ConnectionEnded(String title, String error)
        {
            Acharis.Game_Elements.Point size = GraphicsOptions.Resolution;
            PopupDialog failed = new PopupDialog(title, error, PopupDialog.Buttons.Back, new Squid.Point(size.X / 4, size.Y / 4), size.Y / 2);
            failed.SetHandlers(new List<MouseUpEventHandler>() { 
                            new MouseUpEventHandler(
                                delegate
                                {
                                    DefaultDesktop.ScreenOrder.Push((Control)new MainMenu(new Squid.Point(size.X, size.Y)));
                                    GlobalNetwork.Shutdown = true;
                                })});
            failed.Position = new Squid.Point(size.X / 2 - (failed.Size.x / 2), size.Y / 2 - (failed.Size.y / 2));
            DefaultDesktop.ShowDialog(failed);
        }
        protected bool Handler(CheckUser cu, Client sender)
        {
            if (!sender.Server)
                return false;
            if (!cu.accept)
            {
                ConnectionEnded("Connection Lost", "Your connection to the server was denied because the username/passord combination supplied was invalid.");
                sender.DisconnectClient("Incorrect credentials were supplied");
                return false;
            }

            //Do some handshaking or authentication here
            return false;
        }
    }
}
