﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.IO;
using Acharis.Game_Elements.Combat;
using Acharis.Game_Elements.Development;

namespace Acharis_XNA
{
    class ConsoleManager
    {
        private bool inConsoleMode = false;
        private string consolecmd = "";
        public bool Listener(Squid.KeyData key)
        {
            if (inConsoleMode)
            {
                if (key.Key == Squid.Keys.RETURN || key.Key == Squid.Keys.NUMPADENTER)
                {
                    inConsoleMode = false;
                    HandleCommand(consolecmd);
                }
                if (key.Released)
                {
                    char outp = (char)0;
                    if (GuiRenderer.sTranslateKey(key.Scancode, ref outp) && outp != '`')
                        consolecmd += (char)outp;
                }
                return false;
            }
            else
                if (key.Key == Squid.Keys.GRAVE)
                {
                    inConsoleMode = true;
                    consolecmd = "";
                }

            return true;
        }
        bool ai_added = false;
        public void HandleCommand(string command)
        {
            if (command.StartsWith("ai"))
            {
                string[] split = command.Split(' ');
                uint mapind = 0;
                if (split.Length > 2 && split[1] == "addship" && uint.TryParse(split[2], out mapind))
                {
                    using(LockingWrapper<List<Player>> player_lock = GalaxyController.Players)
                    using (var mml = GalaxyController.Maps)
                    {
                        Map map = mml.Get()[(int)mapind];
                        foreach (Player pl in player_lock.Get())
                        {
                            if (pl.AutomatedPlayer)
                            {
                                debug_AddingShips.AddAIShip(map, pl);
                            }
                        }
                    }
                }

            }
            if (command.StartsWith("addship"))
            {
                string[] split = command.Split(' ');

                using (var mml = GalaxyController.Maps)
                {
                    uint mapindex = 0, raceindex = 0, classindex = 0;
                    if (split.Length < 3 || !uint.TryParse(split[1], out mapindex) || mapindex >= mml.Get().Count)
                        return;

                    bool gotit = true;
                    if (split.Length > 3)
                    {
                        gotit &= uint.TryParse(split[2], out raceindex);
                        gotit &= uint.TryParse(split[3], out classindex);
                    }
                    if (!gotit)
                        return;

                    Array races = Enum.GetValues(typeof(Race));
                    Array classes = Enum.GetValues(typeof(ShipClass));
                    Race myRace = (Race) races.GetValue(raceindex);
                    ShipClass myClass = (ShipClass) classes.GetValue(classindex);

                    Map map = mml.Get()[(int)mapindex];
                    debug_AddingShips.AddShip(map, myRace, myClass, GalaxyController.CurrentPlayer);
                    if (!ai_added)
                    {
                        raceindex++;
                        if (raceindex >= races.Length)
                            raceindex = 0;

                        Ship ai_temp = null;
                        Race ai_race = (Race)races.GetValue(raceindex);
                        ShipClass ai_class = ShipClass.Cruiser;
                        while (ai_temp != null)
                        {
                            ai_class = (ShipClass)classes.GetValue(classindex);
                            ai_temp = ShipFactory.GetShip(myRace, myClass);
                            classindex++;
                            if (classindex >= classes.Length)
                                classindex = 0;
                        }

                        using(LockingWrapper<List<Player>> player_lock = GalaxyController.Players)
                        {
                            foreach(Player pl in player_lock.Get())
                            {
                                if (pl.AutomatedPlayer)
                                {
                                    debug_AddingShips.AddAIShip(map, ai_race, ai_class, pl);
                                    return;
                                }
                            }

                            Player ai = new Player()
                            {
                                PlayerColor = PlayerColours.Player8,
                                PlayerKey = 0xFF,
                                PlayerName = "Computer - Neutral"
                            };
                            GalaxyController.m_internalPlayers.Add(ai);
                            GlobalNetwork.SendToAll(ai.PlayerStats);
                            debug_AddingShips.AddAIShip(map, ai_race, ai_class, ai);
                        }
                        ai_added = true;
                    }
                }
            }
        }
    }
}
