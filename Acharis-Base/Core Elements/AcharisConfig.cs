﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace Acharis.Core_Elements
{
    public class AcharisConfig
    {
        //Configuration
        private XDocument GetNode(XDocument doc, string direct,ref string value)
        {
            XElement root = doc.Root;
            string[] split = direct.Split('\\');
            try
            {
                for (int i = 0; i < split.Length; i++)
                {
                    root = root.Element(split[i]);
                    if (root == null)
                    {
                        root.Add(new XElement(split[i]));
                        root = root.Element(split[i]);
                    }
                }
            }
            catch
            {
                return null;
            }

            if (value != null)
                root.Value = value;
            else
                value = root.Value;

            return doc;
        }
        public void CommitChanges()
        {
            DefaultConfig.Commit();
        }
        public string this[string config]
        {
            get 
            {
                string endResult = null;
                GetNode(DefaultConfig.GetState(), config, ref endResult);
                if (endResult == null || endResult == "")
                {
                    WriteToLog("No value for configuration " + config, LogType.CRITICAL);
                    throw new InvalidOperationException("No value for configuration " + config);
                }
                return endResult;
            }
            set 
            {
                string endResult = value;
                XDocument root = GetNode(DefaultConfig.GetState(), config, ref endResult);
                if (root == null)
                {
                    WriteToLog("No value for configuration " + config, LogType.CRITICAL);
                    throw new InvalidOperationException("No value for configuration " + config);
                }
                DefaultConfig.SetState(root);
            }
        }
        
        //Logging
        public enum LogType
        {
            WARN,ERROR,CRITICAL
        }
        private static object m_lock = new object();
        public static void WriteToLog(string message, LogType lt)
        {
            lock (m_lock)
            {
                string name = System.Enum.GetName(lt.GetType(), lt);
                if (!Directory.Exists(@"Resources\Logs\"))
                    Directory.CreateDirectory(@"Resources\Logs\");
                FileStream fs = new FileStream(@"Resources\Logs\" + name + ".log", FileMode.Append);

                message = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + message;
                fs.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                fs.Write(Encoding.ASCII.GetBytes(Environment.NewLine), 0, Environment.NewLine.Length);

                fs.Flush();
                fs.Close();
            }
        }
    }
}
