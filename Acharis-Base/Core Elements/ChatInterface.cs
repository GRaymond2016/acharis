﻿using System.Collections.Generic;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;

namespace Acharis.Core_Elements
{
    public static class ChatInterface
    {
        private static Dictionary<string, List<string>> registeredChatInterfaces = new Dictionary<string, List<string>>();
        private static Dictionary<string, List<ChatReceiver>> subscribers = new Dictionary<string, List<ChatReceiver>>();
        public delegate void ChatReceiver(string s);

        public static void RegisterString(string name)
        {
            if(!registeredChatInterfaces.ContainsKey(name))
                registeredChatInterfaces.Add(name, new List<string>());
        }

        public static void ReceiveChatMessage(ChatMessage tr)
        {
            string s = tr.playerSource + ": " + tr.message;
            if (tr.playerSource == "system")
                s = tr.message;

            if (!registeredChatInterfaces.ContainsKey(tr.exitPoint))
                registeredChatInterfaces[tr.exitPoint] = new List<string>();

            registeredChatInterfaces[tr.exitPoint].Add(s);

            if (subscribers.ContainsKey(tr.exitPoint))
            {
                foreach (ChatReceiver cr in subscribers[tr.exitPoint]) cr(s);
            }
        }

        public static void SendChatMessage(string sr, string cinterface)
        {
            ChatMessage ch = new ChatMessage();
            ch.message = sr;
            ch.playerSource = GalaxyController.CurrentPlayer.PlayerName;
            ch.exitPoint = cinterface;

            GlobalNetwork.SendToAll(ch);
            ReceiveChatMessage(ch);
        }

        public static void SubscribeToChat(string cw, ChatReceiver dr)
        {
            if (!subscribers.ContainsKey(cw))
                subscribers.Add(cw, new List<ChatReceiver>());
            subscribers[cw].Add(dr);
        }

        public static void UnsubcribeToChat(string cw, ChatReceiver dr)
        {
            if (!subscribers.ContainsKey(cw)) return;
            subscribers[cw].Remove(dr);
        }

        public static List<string> RetrieveTextBlock(string channel, string start, int count)
        {
            List<string> list = registeredChatInterfaces[channel];
            int startI = list.IndexOf(start);

            return list.GetRange(startI, count);
        }
    }
}
