﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements;

namespace Acharis.Core_Elements
{
    public class FileIO
    {
        public static object GetElement(Enum s)
        {
            return FilePaths.GetState().Root.Element(s.GetType().Name).Element(Enum.GetName(s.GetType(),s)).Value;
        }
        private static String extent = "";
        private class Arrays
        {
            public Arrays(string t, int len){tp = t; this.len = len;}
            public String tp;
            public int len;
        }
        public enum LogType
        {
            WARN,ERROR,CRITICAL
        }
        private static object m_lock = new object();
        public static void WriteToLog(string message, LogType lt)
        {
            lock (m_lock)
            {
                string name = System.Enum.GetName(lt.GetType(), lt);
                if (!Directory.Exists(@"Resources\Logs\"))
                    Directory.CreateDirectory(@"Resources\Logs\");
                FileStream fs = new FileStream(@"Resources\Logs\" + name + ".log", FileMode.Append);

                message = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + message;
                fs.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                fs.Write(Encoding.ASCII.GetBytes(Environment.NewLine), 0, Environment.NewLine.Length);

                fs.Flush();
                fs.Close();
            }
        }
        private static void checkLength(String location,String whole, out List<Arrays> trm)
        {
            trm = new List<Arrays>();
            try
            {
                int nex = 0;
                do
                {
                    nex = whole.IndexOf("[Length]", nex);
                    if (nex == -1) break;

                    string[] fm = whole.Remove(0, nex).Split(new char[] { '\"' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    string[] type = fm.Last().Split(';');

                    int res = int.MaxValue;
                    if (!int.TryParse(type[0], out res)) continue;

                    string ty = type[1].Substring(0, type[1].IndexOf('\"'));
                    trm.Add(new Arrays(ty, res));

                    nex++;
                }
                while (true);
            }
            catch(Exception e)
            {
                WriteToLog(e.ToString() + ": " + e.Message, LogType.ERROR);
            }

        }
        public static int[] stringToInts(string el)
        {
            try
            {
                string[] res = el.Split(',');
                return res.Select(x => int.Parse(x)).ToArray();
            }
            catch
            {
                throw new ArgumentException();
            }
        }
        public static Point[] readPoints(String location)
        {
            String whole = readWhole(location);

            List<Arrays> tm;
            checkLength(location,whole,out tm);
            var ar = tm.Where(x => x.tp == "Point");
            if (ar.Count() == 0) return new Point[0];
            Arrays pointArray = ar.First();
            Point[] result = new Point[pointArray.len];

            int start = whole.IndexOf(pointArray.len + ";" + pointArray.tp);
            string[] all = whole.Substring(start).Split(new char[]{'{','}'},pointArray.len * 3);
            int overall = 0;
            for (int i = 1; overall < pointArray.len; i += 2)
            {
                try
                {
                    if (all[i].Contains('[')) break;

                    int[] els = stringToInts(all[i]);
                    result[overall++] = new Point(els[0],els[1]);
                }
                catch
                {
                    WriteToLog("Format Exception in file: " + location, LogType.ERROR);
                }
            }
            return result;
        }
        public static String[] readArray(String location)
        {
            String whole = readWhole(location);

            List<Arrays> tm;
            checkLength(location, whole, out tm);
            var ar = tm.Where(x => x.tp == "String");
            if (ar.Count() == 0) return new string[0];
            Arrays stringArray = ar.First();
            String[] result = new String[stringArray.len];

            int start = whole.IndexOf('{',whole.IndexOf(stringArray.len + ";" + stringArray.tp));
            var mp = whole.Substring(start).Split(new char[] { ',' }, stringArray.len).Select(x => x.Trim(new char[] { '}', '{', ' ' })).ToArray();
            for(int i = 0; i < mp.Count(); i++)
            {
                if (mp[i].StartsWith(Environment.NewLine))
                    mp[i] = mp[i].Substring(Environment.NewLine.Length);
                if (mp[i].EndsWith(Environment.NewLine))
                    mp[i] = mp[i].Substring(0, mp[i].Length - Environment.NewLine.Length);
            }
            return mp;
        }
        private static String readWhole(String location)
        {
            if(location.StartsWith("\\"))
                location = Environment.CurrentDirectory + location;

            if (!location.EndsWith("\\") || !location.EndsWith("/"))
                extent = "\\index.ini";
            else
                extent = "index.ini";

            if (location.EndsWith(".ini") || location.EndsWith(".txt") || location.Contains('.'))
                extent = "";

            String wholeText = "";
            StreamReader read = null;
            try
            {
                read = new StreamReader(location + extent);
                wholeText = read.ReadToEnd();
            }
            catch(Exception e)
            { FileIO.WriteToLog("Cannot read whole file " + location + ". An Exception of type " + e.ToString() + "occured.", LogType.WARN); }
            finally
            { if (read != null)read.Close(); }

            return wholeText;
        }
        public static String[] readToArray(String location, String[] identifiers, Size[] caps)
        {
            String[] card = new String[caps.Length];
            for(int i = 0;i != card.Length;i++)
            {
                card[i] = "Range: " + caps[i].Width.ToString() + " - " + caps[i].Height.ToString();
            }
            return readToArray(location, identifiers, card);
        }
        public static String[] readToArray(String location, String[] identifiers, String[] caps)
        {
            String[] card = attachPadding(identifiers);
            int l = 0;
            foreach (String t in identifiers)
            {
                l = (t.Length > l)?t.Length:l;
            }
            for (int i = 0; i != caps.Length;i++ )
            {
                StringBuilder b = new StringBuilder(card[i]);
                while (b.Length < l) { b.Append(" "); }
                b.Append(" \t\\\\" + caps[i]);
                card[i] = b.ToString();
            }
            return readWrite(location,identifiers,card);
        }
        public static String[] readToArray(String location, String[] identifiers)
        {
            return readWrite(location, identifiers, attachPadding(identifiers));
        }
        private static String[] attachPadding(String[] identifiers)
        {
            String[] caps = new String[identifiers.Length];
            for (int i = 0; i != caps.Length; i++)
            {
                caps[i] = "[" + identifiers[i] + "]=\"\"";
            }
            return caps;
        }
        public static string readElement(string file, string el)
        {
            String wholeText = readWhole(file);
            string newString = wholeText.Substring(wholeText.IndexOf("[" + el + "]"));
            string[] els = newString.Split(new char[] { '"' }, 3);
            return els[1];
        }
        public static void writeWithArray(String location, String[] identifiers, String[] array)
        {
            String wholeText = readWhole(location);

            if (wholeText.Length < 1) return;

            StringBuilder b = new StringBuilder(wholeText);
            StreamWriter write = null;
            try
            {

                for (int i = 0; i != identifiers.Length; i++)
                {
                    if (!wholeText.Contains(identifiers[i]))
                    {
                        b.AppendLine("[" + identifiers[i] + "]=\"" + array[i] + "\"");
                        continue;
                    }
                    try
                    {
                        int searchIndex = wholeText.IndexOf(identifiers[i]);
                        int start = wholeText.IndexOf('"', searchIndex); start++;
                        int end = wholeText.IndexOf('"', start);
                        b.Remove(start, end - start);
                        b.Insert(start, array[i]);
                    }
                    catch { }
                    wholeText = b.ToString();
                }
                write = new StreamWriter(location + extent);
                write.Write(b.ToString());
            }
            catch { FileIO.WriteToLog("Unable to write to location " + location + ((identifiers.Length != array.Length) ? ". Length Mismatch." : "."), LogType.WARN); }
            finally { if (write != null)write.Close(); }

        }
        private static String[] readWrite(String location, String[] identifiers, String[] writen)
        {
            String[] array = new String[identifiers.Length];

            String wholeText = readWhole(location);
            StreamWriter write = new StreamWriter(location + extent,true);

            int ret = 0;
            try
            {
                
                for (int i = 0; i != identifiers.Length; i++)
                {
                    if (!wholeText.Contains(identifiers[i]))
                    { 
                        array[i] = ""; 
                        write.WriteLine(writen[i]);
                        continue; 
                    }
                    try
                    {
                        int searchIndex = wholeText.IndexOf(identifiers[i]);
                        int start = wholeText.IndexOf('"', searchIndex); start++;
                        int end = wholeText.IndexOf('"', start);

                        array[i] = wholeText.Substring(start, end - start);
                        ret++;
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
                array = null;
            }
            finally
            {
                write.Close();
            }
            return array;
        }
    }
}
