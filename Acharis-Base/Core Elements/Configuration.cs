﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Acharis.Core_Elements
{
    public class Configuration
    {
        public enum Graphics
        {
            Bounds,
            Position,
            AntiAlias,
            MultiSample,
            Mode,
            MonitorIndex
        }
        public enum Audio
        {
            SFX,
            Voice,
            Music
        }
        public enum Paths
        {
            PlanetDescriptions,
            IncludeGalaxyMaps,
            PlanetLayouts
        }
        public enum Cursors
        {
            Default,
            Hand,
            Move,
            Select,
            SizeNS,
            SizeWE,
            HSplit,
            VSplit,
            SizeNESW,
            SizeNWSE,
            TextSelect,
            TextSelectSelected,
            Carrot,
            CarrotSelected
        }
        public enum SharedResources
        {
            Button,
            TextBox,
            TextBoxDeselected,
            ScrollArrowUpSel,
            ScrollArrowDownSel,
            ScrollArrowUp,
            ScrollArrowDown
        }
        public enum MainMenu
        {
            Background,
            Button,
            SmallButton
        }
        public enum Popup
        {
            Frame,
            FrameOpaque
        }
        public enum GalaxyMap
        {
            Background
        }
    }
}
