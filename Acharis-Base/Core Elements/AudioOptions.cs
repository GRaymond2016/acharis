﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Core_Elements
{
    public class AudioOptions
    {
        private static string c_SFX = "Audio\\SFX";
        private static string c_Voice = "Audio\\Voice";
        private static string c_Music = "Audio\\Music";

        static AcharisConfig config = new AcharisConfig();
        static int SFX = 100;
        static int voice = 100;
        static int music = 100;

        static public int SFXVolume { get { return SFX; } set { SFX = value; } }
        static public int VoiceVolume { get { return voice; } set { voice = value; } }
        static public int MusicVolume { get { return music; } set { music = value; } }

        static public void setAll(int s, int v, int m)
        {
            SFX = s;
            voice = v;
            music = m;
        }
        public static void updateFromFile()
        {
            try
            {
                SFX = int.Parse(config[c_SFX]);
            }
            catch { SFX = 100; }
            try
            {
                voice = int.Parse(config[c_Voice]);
            }
            catch { voice = 100; }
            try
            {
                music = int.Parse(config[c_Music]);
            }
            catch { music = 100; }
        }
        public static void saveToFile()
        {
            config[c_SFX] = SFX.ToString();
            config[c_Voice] = voice.ToString();
            config[c_Music] = music.ToString();
            config.CommitChanges();
        }
    }
}
