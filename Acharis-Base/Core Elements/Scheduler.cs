﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Acharis.Core_Elements
{
    //The idea here is to extend the function map class and define a set of methods with intervals.
    //You then call intialize on your function map and it will schedule all the functions specified with intervals.
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class Interval : System.Attribute
    {
        private long ms;
        public Interval(long ms)
        {
            this.ms = ms;
        }
        public long MillisecondInterval()
        {
            return ms;
        }
    }
    public abstract class FunctionMap
    {
        private Scheduler m_sched = new Scheduler();
        public void Initialize()
        {
            PopulateScheduler(m_sched);
        }
        public void Begin()
        {
            m_sched.Start();
        }
        public void End()
        {
            m_sched.Stop();
        }
        private void PopulateScheduler(Scheduler che)
        {
            var methods = this.GetType().GetMethods();
            for (int i = 0; i < methods.Length; i++)
            {
                if (methods[i].GetParameters().Length == 0 && methods[i].ReturnType == typeof(void))
                {
                    int index = i;
                    Action cl = new Action(
                        delegate
                        {
                            if (methods.Length > index)
                            {
                                methods[index].Invoke(this, new object[0]);
                            }
                        });
                    object[] array = methods[i].GetCustomAttributes(typeof(Interval), true);
                    if (array.Length > 0)
                    {
                        Interval interval = array[0] as Interval;
                        che.AddScheduledTask(cl, interval.MillisecondInterval());
                    }
                    else
                    {
                        var cmeth = typeof(FunctionMap).GetMethods();
                        if(cmeth.Count(x => x.Name == methods[i].Name) == 0)
                            Logger.WriteLine("Could not get custom attribute from function " + methods[i].Name, Logger.LogType.ERROR);
                    }
                }
            }
        }
    }
    public class Scheduler
    {
        private class Task
        {
            public long lastCall;
            public long interval;
            public Action task;
        }
        private volatile bool stop = true;
        private Thread runningThread = null;
        private List<Task> tasks = new List<Task>();
        private object task_lock = new object();

        private void DoWork()
        {
            while (!stop)
            {
                long current = DateTime.Now.Ticks;
                lock (task_lock)
                {
                    foreach (Task t in tasks)
                    {
                        if (t.lastCall + t.interval < current)
                        {
                            t.task();
                            t.lastCall = current;
                        }
                    }
                }
                Thread.Sleep(10);
            }
        }
        public void AddScheduledTask(Action task, long millisecInterval)
        {
            if (!stop)
                throw new Exception("Cannot add a task whilst scheduler is running.");
            lock (task_lock)
            {
                tasks.Add(new Task() { lastCall = 0, interval = millisecInterval * TimeSpan.TicksPerMillisecond, task = task });
            }
        }
        public void Start()
        {
            stop = false;
            runningThread = new Thread(new ThreadStart(DoWork));
            runningThread.Start();
        }
        public void Stop()
        {
            stop = true;
        }
    }
}
