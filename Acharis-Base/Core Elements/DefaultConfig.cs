﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using System;

namespace Acharis.Core_Elements
{
    /// <summary>
    /// This is not a good way to do this, please refactor into XML.
    /// </summary>
    internal class DefaultConfig
    {
        private static string filename = "Configuration.xml";
        private static XDocument xdoc = null;
        static DefaultConfig()
        {
            try
            {
                if (File.Exists(filename))
                    using (FileStream fs = File.OpenRead(filename))
                    {
                        xdoc = XDocument.Load(fs);
                    }
                else
                    throw new FileNotFoundException("Could not find Configuration.xml");
            }
            catch (Exception e)
            {
                AcharisConfig.WriteToLog("An error occured whilst trying to load the config file. It exists but an exception of type " + e.ToString() + " occured.", AcharisConfig.LogType.WARN);
                xdoc = GetDefault();
                xdoc.Save(filename);
            }
        }
        public static XDocument GetState()
        {
            return xdoc;
        }
        public static void SetState(XDocument doc)
        {
            xdoc = doc;
        }
        public static void Commit()
        {
            xdoc.Save(filename);
        }
        private static XDocument GetDefault()
        {
            return new XDocument(
            new XElement
            (
                "Configurations",
                new XAttribute("watched","false"),
                new XElement("Graphics",
                    new XElement("Bounds",
                        new XAttribute("x","800"),
                        new XAttribute("y","600")),
                    new XElement("Position",
                        new XAttribute("x", "800"),
                        new XAttribute("y", "600")),
                    new XElement("AntiAlias", "True"),
                    new XElement("MultiSample", "True"),
                    new XElement("Mode", "1"),
                    new XElement("MonitorIndex", "0")),
                new XElement("Audio",
                    new XElement("SFX",""),
                    new XElement("Voice", ""),
                    new XElement("Music", "")),
                new XElement("Paths",
                    new XElement("PlanetDescriptions",
                        new XAttribute("path", @"Resources\Planet Desc\")),
                    new XElement("IncludedGalaxyMaps",
                        new XAttribute("path", @"Resources\Galaxy Map\")),
                    new XElement("PlanetLayouts",
                        new XAttribute("path", @"Resources\Static Maps\"))),
                new XElement("Cursors",
                    new XElement("Default", "deafult.png"),
                    new XElement("TextSelect", "textcursor1.png"),
                    new XElement("TextSelectSelected", "textcursor2.png"),
                    new XElement("Carrot", "carrot1.png"),
                    new XElement("CarrotDeselected", "carrot2.png")),
                new XElement("SharedResources",
                    new XAttribute("path", @"Resources\GUI\"),
                    new XElement("Button", 
                        new XAttribute("path",@"Resources\Main Menu\"),
                        "MainMenuButtonSplit.png"),
                    new XElement("TextBox","textbox1.png"),
                    new XElement("TextBoxDeselected", "textbox1DESELECT.png"),
                    new XElement("ScrollArrowUpSel", "arrow1up.png"),
                    new XElement("ScrollArrowDownSel","arrow2up.png"),
                    new XElement("ScrollArrowUp","arrow1.png"),
                    new XElement("ScrollArrowDown","arrow2.png"),
                    new XElement("ScrollBar","scrollbar.png"),
                    new XElement("ChatWindow","ChatMockup3.png")),
                new XElement("MainMenu",
                    new XAttribute("path", @"Resources\Main Menu\"),
                    new XElement("Background","amibackground2.png"),
                    new XElement("Button","MainMenuButtonSplit.png"),
                    new XElement("SmallButton", "SmallButtonSplit.png")),
                new XElement("Popup",
                    new XAttribute("path", @"Resources\Galaxy Map\"),
                    new XElement("Frame", "PopupFrame.png"),
                    new XElement("FrameOpaque", "framePopupOpaque.png")),
                new XElement("Multiplayer"),
                new XElement("GalaxyMap",
                    new XElement("Background",
                        new XAttribute("path", @"Resources\MapGen\"),
                        "MenuBackground.png")),
                new XElement("PlanetMap"),
                new XElement("PlanetMapData"),
                new XElement("PlanetMapFolders"),
                new XElement("PlanetData")
            ));
        }
    }
}
