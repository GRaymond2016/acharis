﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace Acharis.Core_Elements
{
    public class AcharisConfig : DefaultConfig
    {
        //Configuration
        private XDocument GetNode(XDocument doc, string direct, ref string value)
        {
            XElement root = doc.Root;
            string[] split = direct.Split('\\');
            try
            {
                for (int i = 0; i < split.Length; i++)
                {
                    root = root.Element(split[i]);
                    if (root == null)
                    {
                        root.Add(new XElement(split[i]));
                        root = root.Element(split[i]);
                    }
                }
            }
            catch
            {
                return null;
            }

            if (value != null)
                root.Value = value;
            else
                value = DeterminePrec(root.Parent, root.Name.LocalName);

            return doc;
        }
        public void CommitChanges()
        {
            DefaultConfig.Commit();
        }
        public string this[string config]
        {
            get 
            {
                string endResult = null;
                GetNode(DefaultConfig.GetState(), config, ref endResult);
                if (endResult == null || endResult == "")
                {
                    Logger.WriteLine("No value for configuration " + config, Logger.LogType.CRITICAL);
                    return default(string);
                }
                return endResult;
            }
            set 
            {
                string endResult = value;
                XDocument root = GetNode(DefaultConfig.GetState(), config, ref endResult);
                if (root == null)
                {
                    Logger.WriteLine("No value for configuration " + config, Logger.LogType.CRITICAL);
                    throw new InvalidOperationException("No value for configuration " + config);
                }
                DefaultConfig.SetState(root);
            }
        }
    }
}
