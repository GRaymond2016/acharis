﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements;

namespace Acharis.Core_Elements.Configuration
{
    public class Objective
    {
        public int ObjectiveKey;
        public Point Location;
        public int Radius;
    }
    internal class MapConfigurer : DefaultConfig
    {
        private static AcharisConfig config = new AcharisConfig();

        public class StaticObject
        {
            public Point Location;
            public string ImageFileName;
        }
        public class MapTemplate
        {
            public String Name;
            public String BackgroundPath;
            public int Key;
            public Microsoft.Xna.Framework.Point Bounds;
            public List<StaticObject> Objects;
            public List<Objective> Objectives;
            public string ObjectiveNeutral;
            public string ObjectiveOwned;
            public string ObjectiveEnemy;
        }

        private static List<MapTemplate> resolvedMapTemplates = new List<MapTemplate>();
        private static MapTemplate AddMap(string name)
        {
            try
            {
                string pm = config["Paths\\PlanetLayouts"];
                if (!name.Contains("\\"))
                    name = pm + name;
                name += ".pmap";
                XDocument doc = XDocument.Load(name);
                MapTemplate mc = new MapTemplate();
                mc.Name = doc.Root.Attribute("name").Value;
                mc.Key = int.Parse(doc.Root.Attribute("key").Value);

                var coll = resolvedMapTemplates.Where(x => x.Key == mc.Key).FirstOrDefault();
                if (coll != null)
                    return coll;

                mc.BackgroundPath = DeterminePrec(doc.Root, "Background");
                mc.ObjectiveNeutral = config["PlanetMap\\ObjectiveMarkerNeutral"];
                mc.ObjectiveEnemy = config["PlanetMap\\ObjectiveMarkerEnemy"];
                mc.ObjectiveOwned = config["PlanetMap\\ObjectiveMarkerOwned"];

                XElement bounds = doc.Root.Element("Bounds");
                mc.Bounds = new Microsoft.Xna.Framework.Point(
                                        int.Parse(bounds.Attribute("x").Value),
                                        int.Parse(bounds.Attribute("y").Value));
                mc.Objects = new List<StaticObject>();
                XElement statics = doc.Root.Element("StaticItems");
                if (statics != null)
                {
                    foreach (XElement el in statics.Elements("Item"))
                    {
                        try
                        {
                            StaticObject ob = new StaticObject();
                            ob.Location = new Point(
                                                int.Parse(el.Attribute("x").Value),
                                                int.Parse(el.Attribute("y").Value));
                            XAttribute path = el.Attribute("path");
                            XAttribute file = el.Attribute("file");
                            ob.ImageFileName = precursor;

                            if (path != null)
                                ob.ImageFileName += path.Value + file.Value;
                            else
                                ob.ImageFileName += file.Value;

                            mc.Objects.Add(ob);
                        }
                        catch (Exception e)
                        {
                            Logger.WriteLine("Unable to process for " + el.ToString() + " invalid form!" + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                        }
                    }
                }
                mc.Objectives = new List<Objective>();
                XElement objectives = doc.Root.Element("Objectives");
                if (objectives != null)
                {
                    int obKey = 0;
                    foreach (XElement el in objectives.Elements("Item"))
                    {
                        try
                        {
                            Objective objective = new Objective()
                            {
                                Location = new Point()
                                {
                                    X = int.Parse(el.Attribute("x").Value),
                                    Y = int.Parse(el.Attribute("y").Value)
                                },
                                Radius = int.Parse(el.Attribute("radius").Value),
                                ObjectiveKey = obKey++
                            };
                            mc.Objectives.Add(objective);
                        }
                        catch (Exception e)
                        {
                            Logger.WriteLine("Unable to process objective in map " + el.ToString() + " invalid form!" + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                        }
                    }
                }
                resolvedMapTemplates.Add(mc);
                return mc;
            }
            catch (Exception e)
            {
                Logger.WriteLine("Unable to process file " + name + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
            }
            return null;
        }
        public static MapTemplate GetMap(string name)
        {
            MapTemplate em = AddMap(name);
            return em;
        }
    }
}
