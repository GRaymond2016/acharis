﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using Microsoft.Xna.Framework;

namespace Acharis.Core_Elements.Configuration
{
    public class GalaxyConfigurer : DefaultConfig
    {
        private static AcharisConfig config = new AcharisConfig();
        private static List<GalaxyTemplate> resolvedTemplates = new List<GalaxyTemplate>();
        public class GalaxyTemplate
        {
            public string Name;
            public string UniqueID;
            public List<GalaxyMapTemplate> maps;
            public string XMLNode;
        }
        public class GalaxyMapTemplate
        {
            public string MapFileName;
            public string PlanetDescFileName;
            public string ThumbPath;
            public string Anchor;
            public Point Bounds;
            public int LinkDistance;
        }
        static GalaxyConfigurer()
        {
            string path = config["Paths\\IncludedGalaxyMaps"];
            string[] files = Directory.GetFiles(path, "*.gmap");

            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    XDocument doc = XDocument.Load(files[i]);
                    foreach (XElement map in doc.Root.Elements("Map"))
                    {
                        resolvedTemplates.Add(DecodeMap(map));
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Unable to process file " + files[i] + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                }
            }
        }

        public static GalaxyTemplate GetMap(string name)
        {
            return resolvedTemplates.Where(x => x.Name == name).FirstOrDefault();
        }
        public static List<string> GetMapNames()
        {
            return resolvedTemplates.Select(x => x.Name).ToList();
        }
        public static List<string> GetFiles(string name)
        {
            string path = config["Paths\\PlanetDescriptions"];
            GalaxyTemplate maps = GetMap(name);
            if (maps == null)
            {
                //TODO: MAP NOT FOUND
                Logger.WriteLine("Map :" + name + " was not found.", Logger.LogType.CRITICAL);
            }
            List<string> subset = new List<string>();
            for(int i = 0;i < maps.maps.Count; i++)
            {
                if(!Path.IsPathRooted(maps.maps[i].PlanetDescFileName))
                    subset.Add(path + maps.maps[i].PlanetDescFileName);
                else
                    subset.Add(maps.maps[i].PlanetDescFileName);
            }
            return subset;
        }
        public static bool CompareMap(GalaxyTemplate tem)
        {
            try
            {
                Logger.WriteLine("Comparing map " + tem.Name + " to collection.", Logger.LogType.DEBUG);
                for (int i = 0; i < resolvedTemplates.Count; i++)
                {
                    if (resolvedTemplates[i].UniqueID == tem.UniqueID)
                        return true;
                }
            }
            catch(Exception e)
            {
                Logger.WriteLine(e.ToString(), Logger.LogType.ERROR);
            }
            return false;
        }
        public static GalaxyTemplate DecodeMap(XElement map)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                GalaxyTemplate gt = new GalaxyTemplate();
                gt.XMLNode = map.ToString();
                gt.Name = map.Attribute("name").Value;
                gt.maps = new List<GalaxyMapTemplate>();
                sb.Append(gt.Name);

                int ld = 300;
                XElement el = map.Element("LinkDistance");
                if (el != null)
                    ld = int.Parse(el.Value);
               
                foreach (XElement planet in map.Elements("Planet"))
                {
                    try
                    {
                        GalaxyMapTemplate gmt = new GalaxyMapTemplate();
                        gmt.Anchor = planet.Element("Anchor").Value;
                        gmt.PlanetDescFileName = DeterminePrec(planet, "PlanetFile");
                        gmt.MapFileName = DeterminePrec(planet, "MapFile");
                        gmt.ThumbPath = DeterminePrec(planet, "ThumbPath");

                        gmt.Bounds = new Point(int.Parse(planet.Element("XOffset").Value),
                                                int.Parse(planet.Element("YOffset").Value));

                        sb.Append(Path.GetFileName(gmt.PlanetDescFileName));
                        sb.Append(Path.GetFileName(gmt.MapFileName));
                        gmt.LinkDistance = ld;
                        gt.maps.Add(gmt);
                    }
                    catch (Exception e)
                    {
                        Logger.WriteLine("Unable to process Planet Element " + planet.ToString() + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                    }
                }


                gt.UniqueID = sb.ToString();
                return gt;
            }
            catch (Exception e)
            {
                Logger.WriteLine("Unable to process Map Element " + map.ToString() + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                return null;
            }
        }
    }
}
