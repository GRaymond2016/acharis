﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using System;
using System.Collections.Generic;

namespace Acharis.Core_Elements
{
    public class DefaultConfig
    {
        public static string precursor { get; private set; }
        private static string filename { get; set; }
        private static XDocument xdoc = null;
        static DefaultConfig()
        {
            precursor = System.Environment.GetEnvironmentVariable("ACH_RES_PATH", EnvironmentVariableTarget.User) ?? "";
            filename = precursor + "Resources\\Configuration.xml";
            try
            {
                if (File.Exists(filename))
                    using (FileStream fs = File.OpenRead(filename))
                    {
                        xdoc = XDocument.Load(fs);
                    }
                else
                    throw new FileNotFoundException("Could not find " + filename);
            }
            catch (Exception e)
            {
                Logger.WriteLine("An error occured whilst trying to load the config file. It exists but an exception of type " + e.ToString() + " occured.", Logger.LogType.CRITICAL);
                throw new InvalidOperationException("Could not find configuration: " + filename);
            }
        }
        public static Dictionary<EnumType, string> GetMatchedEnum<EnumType>(XElement parent, string node) where EnumType : struct, IConvertible
        {
            Dictionary<EnumType, string> retValue = new Dictionary<EnumType, string>();
            if (!typeof(EnumType).IsEnum)
            {
                Logger.WriteLine("Invalid Argument: type given to GetMatchedEnum was not an enumeration.", Logger.LogType.ERROR);
                return retValue;
            }
            
            Dictionary<string, string> collective = GetCollective(parent, node);
            foreach (KeyValuePair<string, string> name in collective)
            {
                EnumType result;
                if (Enum.TryParse<EnumType>(name.Key, true, out result))
                    retValue.Add(result, name.Value);
            }
            return retValue;
        }
        private static Dictionary<string, string> GetCollective(XElement parent, string node)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            XElement iter = parent.Element(node);
            foreach (XElement el in iter.Elements())
            {
                map.Add(el.Name.LocalName, DeterminePrec(iter, el.Name.LocalName));
            }
            return map;
        }
        protected static string DeterminePrec(XElement parent, string node)
        {
            XElement el = parent.Element(node);
            if (el == null)
            {
                Logger.WriteLine("Could not find node " + node + " in xml document.", Logger.LogType.INFO);
                return "";
            }
            XAttribute path = parent.Element(node).Attribute("path");
            XAttribute parPath = parent.Attribute("path");

            if (path != null && !Path.IsPathRooted(path.Value)) path.Value = precursor + path.Value;
            if (parPath != null && !Path.IsPathRooted(parPath.Value)) parPath.Value = precursor + parPath.Value;

            if (path != null)
                return path.Value + el.Value;
            else if (parPath != null)
                return parPath.Value + el.Value;
            else
                return el.Value;
        }
        protected static XDocument GetState()
        {
            return xdoc;
        }
        protected static void SetState(XDocument doc)
        {
            xdoc = doc;
        }
        protected static void Commit()
        {
            xdoc.Save(filename);
        }
    }
}
