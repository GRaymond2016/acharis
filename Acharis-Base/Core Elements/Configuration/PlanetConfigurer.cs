﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Xml.Linq;
using Acharis.Game_Elements.Combat;

namespace Acharis.Core_Elements.Configuration
{
    class PlanetConfigurer : DefaultConfig
    {
        private static AcharisConfig config = new AcharisConfig();
        public static void Parse(Planet p, string location)
        {
            try
            {
                string path = config["Paths\\PlanetDescriptions"];
                if (!location.Contains("\\"))
                    location = path + location;

                XDocument xdoc = XDocument.Load(location);
                p.Name = xdoc.Root.Attribute("name").Value;
                p.Description = xdoc.Root.Element("Description").Value;
                p.Population = int.Parse(xdoc.Root.Element("Population").Value);
                p.Owner = int.Parse(xdoc.Root.Element("Owner").Value);
                p.Income = int.Parse(xdoc.Root.Element("Income").Value);
                p.Type = int.Parse(xdoc.Root.Element("Type").Value);
                XElement el = xdoc.Root.Element("RacialHome");
                p.RacialHome = Race.None;
                if(el != null)
                {
                    switch (xdoc.Root.Element("RacialHome").Value.ToLower())
                    {
                        case "tchuan": p.RacialHome = Race.Tchuan; break;
                        case "endlorian": p.RacialHome = Race.Endlorian; break;
                        case "human": p.RacialHome = Race.Human; break;
                    }
                }
            }
            catch(Exception e)
            {
                Logger.WriteLine("Exception when parsing planet file " + location + "." + Environment.NewLine + e.ToString(), Logger.LogType.CRITICAL);
            }
        }
    }
}
