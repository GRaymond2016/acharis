﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace Acharis.Core_Elements.Configuration
{
    public enum ShipAnimationTypes
    {
        None,
        ShieldIdle,
        ShieldDamage,
        WarpIdle
    }
    class ShipTemplate
    {
        public string image;
        public string name;
        public string race;
        public string @class;
        public float scale;
        public float damage_reduction;
        public int sight_radius;
        public int cost;
        public int shield_points;
        public int health_points;
        public float speed;
        public float rotation_speed;
        public Dictionary<ShipAnimationTypes, string> animations = new Dictionary<ShipAnimationTypes, string>();
    }

    class ShipConfigurer : DefaultConfig
    {
        private static AcharisConfig config = new AcharisConfig();
        private List<ShipTemplate> ships = new List<ShipTemplate>();

        public ShipConfigurer()
        {
            string path = config["Paths\\ShipStats"];
            string[] files = Directory.GetFiles(path, "*.stat");

            foreach (string file in files)
            {
                try
                {
                    XDocument doc = XDocument.Load(file);
                    foreach (XElement sh in doc.Root.Elements("ship"))
                    {
                        DecodeShip(sh);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Unable to process file " + file + Environment.NewLine + e.ToString(), Logger.LogType.ERROR);
                }
            }
        }

        public List<ShipTemplate> GetShipTemplates()
        {
            return ships;
        }

        private void DecodeShip(XElement ship)
        {
            try
            {
                ShipTemplate tship = new ShipTemplate();
                tship.image = DeterminePrec(ship, "image");
                tship.name = ship.Element("name").Value;
                tship.race = ship.Element("race").Value;
                tship.@class = ship.Element("class").Value;
                tship.damage_reduction = float.Parse(ship.Element("damage_reduction").Value);
                tship.sight_radius = int.Parse(ship.Element("sight_radius").Value);
                tship.shield_points = int.Parse(ship.Element("shield_points").Value);
                tship.health_points = int.Parse(ship.Element("health_points").Value);
                tship.speed = float.Parse(ship.Element("speed").Value);
                tship.rotation_speed = float.Parse(ship.Element("rotation_speed").Value);
                tship.scale = float.Parse(ship.Element("scale").Value);
                tship.cost = int.Parse(ship.Element("cost").Value);
                tship.animations = DefaultConfig.GetMatchedEnum<ShipAnimationTypes>(DefaultConfig.GetState().Root, "ShipAnimations").
                                    Concat(DefaultConfig.GetMatchedEnum<ShipAnimationTypes>(ship, "animations")).ToDictionary(gr => gr.Key, gr => gr.Value);

                ships.Add(tship);
            }
            catch
            {
                Logger.WriteLine("Invalid syntax in XML node: " + ship.ToString() + Environment.NewLine, Logger.LogType.ERROR);
            }
        }
    }
}
