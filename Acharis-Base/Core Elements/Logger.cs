﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Acharis.Core_Elements
{
    public class Logger : TextWriterTraceListener
    {
        private static Dictionary<string, FileStream> logs = new Dictionary<string, FileStream>();
        private static string precursor = Path.GetDirectoryName(System.Environment.GetEnvironmentVariable("ACH_ERROR_PATH", EnvironmentVariableTarget.User) ?? Environment.CurrentDirectory);
        //Logging
        public enum LogType
        {
            WARN, ERROR, CRITICAL, INFO, DEBUG
        }
        private static object m_lock = new object();
        private static Logger instance = new Logger();
        public static void WriteLine(string message, LogType lt)
        {
            instance.WriteLine(message, Enum.GetName(typeof(LogType),lt));
        }
        public static void Write(Exception e, LogType lt)
        {
            WriteLine(e.Message, lt);
            WriteLine(e.StackTrace.ToString(), lt);
        }
        private static string DefaultWrite = Enum.GetName(typeof(LogType), LogType.ERROR);
        public override void WriteLine(string message)
        {
            WriteLine(message, DefaultWrite);
        }
        public override void WriteLine(string message, string category)
        {
            message = "[" + System.AppDomain.CurrentDomain.FriendlyName + "][Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.f") + "]\t" + message;
            lock (m_lock)
            {
                //Info will only show in debug mode
#if RELEASE
                if (category == "DEBUG")
                    return;
#endif
                try
                {
                    string dir = precursor + @"\Logs\";
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    string filename = dir + category + ".log";

                    FileStream fs;
                    if (logs.ContainsKey(filename))
                        fs = logs[filename];
                    else
                    {
                        fs = new FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                        logs[filename] = fs;
                    }

                    fs.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                    fs.Write(Encoding.ASCII.GetBytes(Environment.NewLine), 0, Environment.NewLine.Length);

                    fs.Flush();
                }
                catch
                {
                }
            }
#if DEBUG
            System.Diagnostics.Debug.WriteLine(message);
#endif
        }
    }
}
