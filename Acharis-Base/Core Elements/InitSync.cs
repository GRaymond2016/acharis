﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Xml.Linq;
using Acharis.Core_Elements.Configuration;

namespace Acharis.Core_Elements
{
    public class InitSync
    {
        private static InitSync ins = null;

        private Dictionary<string, bool> filesAquired = new Dictionary<string, bool>();
        private Stack<string> threadSafeAquired = new Stack<string>();
        public static Dictionary<string, byte[]> Files { get; set; }

        public int TotalFiles 
        { 
            get 
            {
                fileAquired();
                if (Files == null)
                    return 0; 
                return Files.Count;
            } 
        }
        public int FilesDownloaded 
        { 
            get 
            {
                fileAquired();
                if (filesAquired == null)
                    return int.MaxValue; 
                return filesAquired.Count(x => x.Value); 
            } 
        }
        public static InitSync Instance 
        {
            get
            {
                if (ins == null)
                    ins = new InitSync();
                return ins;
            }
        }
        public static void Dispose()
        {
            ins = null;
        }
        //Enumeration would not be thread safe.
        private void fileAquired()
        {
            if (threadSafeAquired.Count == 0)
                return;

            string s = "";
            try
            {
                s = threadSafeAquired.Pop();
            }
            catch (InvalidOperationException) { return; }

            lock (filesAquired)
            {
                filesAquired[s] = true;
            }
        }
        public void fileAquired(string s)
        {
            threadSafeAquired.Push(s);
        }

        //This needs work, its been hacked apart with configuration changes.
        public void CheckForMap(string XmlSegment, Dictionary<string, byte[]> hashes)
        {
            filesAquired.Clear();
            Files = hashes;

            GalaxyConfigurer.GalaxyTemplate galaxyMap = null;
            try
            {
                galaxyMap = GalaxyConfigurer.DecodeMap(XElement.Parse(XmlSegment));
            }
            catch(Exception e)
            {
                Logger.WriteLine("Exception thrown when processing xmlSegment " + XmlSegment + "." + Environment.NewLine + e.ToString(), Logger.LogType.CRITICAL);
                throw;
            }
            GalaxyController.GalaxyMap = galaxyMap.Name;

            if (!GalaxyConfigurer.CompareMap(galaxyMap))
            {
                MD5 hash = MD5.Create();
                foreach (var mp in hashes)
                {
                    if (File.Exists(mp.Key))
                    {
                        byte[] has = hash.ComputeHash(File.ReadAllBytes(mp.Key));
                        if (!has.SequenceEqual(mp.Value))
                        {
                            GlobalNetwork.SendToAll(new FileDownload(mp.Key));
                            string top = mp.Key.Substring(0, mp.Key.LastIndexOf('.'));
                            int it = 1;
                            if (File.Exists(mp.Key.Replace(top, top + ".previous")))
                            {
                                while (File.Exists(mp.Key.Replace(top, top + ".previous" + it))) it++;
                                File.Move(mp.Key, mp.Key.Replace(top, top + ".previous" + it));
                            }
                            else
                                File.Move(mp.Key, mp.Key.Replace(top, top + ".previous"));
                        }
                        else
                        {
                            fileAquired(mp.Key);
                            continue;
                        }
                    }
                    else
                    {
                        GlobalNetwork.SendToAll(new FileDownload(mp.Key));
                    }
                }
            }
            else
                foreach (var st in hashes.Keys)
                    fileAquired(st);
        }
        public void LoadMap(string scenarioName, bool server = true)
        {
            AcharisConfig config = new AcharisConfig();

            var gmap = GalaxyConfigurer.GetMap(scenarioName);
            if (server)
            {
                Dictionary<string, byte[]> fileList = new Dictionary<string, byte[]>();
                MD5 hasher = MD5.Create();
                foreach(string file in GalaxyConfigurer.GetFiles(scenarioName))
                {
                    try
                    {
                        if (fileList.ContainsKey(file))
                        {
                            fileList[file] = hasher.ComputeHash(File.ReadAllBytes(file));
                        }
                        else
                        {
                            fileList.Add(file, hasher.ComputeHash(File.ReadAllBytes(file)));
                        }
                    }
                    catch(Exception e)
                    {
                        Logger.WriteLine("Cannot hash file " + file + "." + Environment.NewLine + e.ToString(), Logger.LogType.CRITICAL);
                        throw;
                    }
                }

                Files = fileList;
                GlobalNetwork.SendToAll(new GalaxyTransfer(gmap.XMLNode, fileList));
            }

            using (var planl = GalaxyController.Planets)
            {
                planl.Get().Clear();
            }
            using (var mapl = GalaxyController.Maps)
            {
                mapl.Get().Clear();
            }

            foreach (var map in gmap.maps)
            {
                GalaxyPlanetBinding gpb = new GalaxyPlanetBinding();
                gpb.Anchor = map.Anchor;
                gpb.Bounds = new Acharis.Game_Elements.Point(map.Bounds.X,map.Bounds.Y);
                gpb.ThumbPath = map.ThumbPath;
                gpb.AssociatedPlanet = new Planet(map.PlanetDescFileName);
                gpb.AssociatedFleetStats = new Game_Elements.Galaxy.FleetStats();
                gpb.LinkDistance = map.LinkDistance;

                //Parse MapData
                Map m = new Map(map.MapFileName);
                m.ControlIncome = gpb.AssociatedPlanet.Income;
                GalaxyController.AddMap(m);
                gpb.AssociatedMap = m.MapKey;
                using (var planl = GalaxyController.Planets)
                {
                    planl.Get().Add(gpb);
                }
            }

            GalaxyController.GalaxyMap = scenarioName;
            Dispose();
        }
    }
}
