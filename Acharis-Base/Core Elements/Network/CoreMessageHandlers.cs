﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements;
using Acharis.Game_Elements.Combat;
using System.Security.Cryptography;
using System.IO;

namespace Acharis.Core_Elements.Network
{
    public class CoreMessageHandlers : ANetworkHandleBase
    {
        private Dictionary<string, DateTime> recs = new Dictionary<string, DateTime>();
        private object recLock = new object();

        protected virtual bool Handler(PlayerStats stat, Client sender)
        {
            if (!CredSupplied && GlobalNetwork.Server)
            {
                sender.Send(new ClosingConnection("Credentials not supplied."));
                sender.Cleanup();
                return false;
            }

            if (!sender.Server)
                return false;

            if (stat.add)
            {
                using (var playl = GalaxyController.Players)
                {
                    Player player = playl.Get().Where(x => x.PlayerName == stat.playerName).FirstOrDefault();
                    if (player == null)
                    {
                        Player p = new Player();

                        p.PlayerName = stat.playerName;
                        p.PlayerKey = stat.key;
                        p.PlayerColor = PlayerColours.GetColourByIndex(p.PlayerKey);

                        playl.Get().Add(p);
                        sender.AddPlayer(p.PlayerName);
                    }
                    else
                    {
                        player.PlayerRace = stat.playerrace;
                        player.PlayerColor = stat.playerColour;
                        if (player.PlayerKey != stat.key)
                        {
                            player.PlayerKey = stat.key;
                            lock (player.ship_lock)
                            {
                                List<Ship> ships = player.Ships;
                                player.Ships = new List<Ship>();
                                foreach (Ship s in ships)
                                {
                                    player.ClaimShip(s);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                using (var playl = GalaxyController.Players)
                {
                    var coll = playl.Get().Where(x => x.PlayerKey == stat.key);
                    foreach (var pl in coll)
                        pl.Cleanup();
                }
            }

            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(ShipStat stat, Client sender)
        {
            Ship s = new Ship(stat);
            ShipFactory.UpdateNonNetworkStats(ref s);
            using (var players = GalaxyController.Players)
            {
                Player player = players.Get().Where(x => x.PlayerName == stat.ownerName).FirstOrDefault();
                if (player != null)
                {
                    s.Owner = player;
                    lock (player.ship_lock)
                    {
                        foreach (Ship tship in player.Ships)
                        {
                            if (tship.UniqueID == s.UniqueID)
                            {
                                Logger.WriteLine("A ship was received that was already contained in the collection. Ship #" + stat.shipid, Logger.LogType.DEBUG);
                                return false;
                            }
                        }
                    }

                    player.ClaimShip(s);
                    lock (s.Owner.fleet_lock)
                    {
                        foreach (Fleet fleet in player.Fleets)
                        {
                            if (fleet.Name == stat.fleetName)
                            {
                                s.Fleet = fleet;
                                fleet.Ships.Add(s);
                            }
                        }
                    }      
                }
                else 
                {
                    Logger.WriteLine("A ship was not registered (#" + stat.shipid + ") because the owning player was not found in the collection " + stat.ownerName, Logger.LogType.CRITICAL);
                }
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(MoveShipBetweenMaps ship, Client sender)
        {
            Map source = null, destination = null;
            using (var play = GalaxyController.Maps)
            {
                var dict = play.Get();
                destination = dict[ship.newMapId];
                source = dict[ship.previousMapId];
            }
            using (var player = GalaxyController.Players)
            {
                foreach (Player play in player.Get())
                {
                    lock (play.ship_lock)
                    {
                        Ship s = play.Ships.Where(x => x.UniqueID == ship.shipid).FirstOrDefault();
                        if (s != null && source != null)
                        {
                            Ship sp = s;
                            source.RemoveShip(sp);
                            destination.AddShip(ref sp);
                        }
                    }
                }
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(ClosingConnection stat, Client sender)
        {
            if (!GlobalNetwork.Server && sender.Server)
                sender.DisconnectClient(stat.message);
            return false;
        }
        protected virtual bool Handler(UpdatePlanet stat, Client sender)
        {
            using (var Maps = GalaxyController.Maps)
            {
                var dict = Maps.Get();
                if (dict.ContainsKey(stat.MapId))
                    Maps.Get()[stat.MapId].ControllingRace = stat.ControllingRace;
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(MoveFleet fleet, Client sender)
        {
            return false;
        }
        protected virtual bool Handler(AddFleet fleet, Client sender)
        {
            Map mp;
            using (var maps = GalaxyController.Maps)
            {
                var dict = maps.Get();
                if (!dict.ContainsKey(fleet.MapId))
                {
                    Logger.WriteLine("Warning, the map specified when attempting to add a fleet was invalid. The fleet may not be viewable.", Logger.LogType.WARN);
                    return GlobalNetwork.Server;
                }
                mp = maps.Get()[fleet.MapId];             
            }
            using (var plays = GalaxyController.Players)
            {
                Player p = plays.Get().Where(x => x.PlayerName == fleet.PlayerName).FirstOrDefault();
                if (p != null)
                {
                    Fleet f = new Fleet()
                    {
                        Name = fleet.FleetName,
                        Owner = p,
                        Ships = new List<Ship>(),
                        Location = mp
                    };
                    p.AddFleet(f);
                }
                else
                {
                    Logger.WriteLine("Error adding fleet for player, player \"" + fleet.PlayerName + "\" does not exist.", Logger.LogType.ERROR);
                }
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(LocationRequest stat, Client sender)
        {
            if (stat.request)
            {
                Player current = GalaxyController.CurrentPlayer;
                using (var Maps = GalaxyController.Maps)
                {
                    foreach (Map mb in Maps.Get().Values)
                    {
                        List<LocationRequest> lcUp = mb.GetShipLocations();
                        for (int i = 0; i < lcUp.Count; i++)
                        {
                            if (GlobalNetwork.Server || lcUp[i].playerid == current.PlayerKey)
                                sender.Send(lcUp[i]);
                        }
                    }
                }
            }
            else
            {
                using (var playl = GalaxyController.Players)
                {
                    var players = playl.Get().Where(x => x.PlayerKey == stat.playerid);
                    if (players.Count() > 0)
                    {
                        Player pl = players.First();
                        lock (pl.ship_lock)
                        {
                            Ship ship = pl.Ships.Where(y => y.UniqueID == stat.shipid).FirstOrDefault();
                            if (ship != null)
                            {
                                lock (ship.sourceAnchorLock)
                                {
                                    ship.SourceAnchor = new PointF(stat.location);
                                }
                                ship.Rotation = stat.rotation;
                                ship.LastRotate = stat.lastRotation;
                                ship.LastMove = stat.lastMove;
                                ship.SetVelocity(stat.currentVelocity);
                                return false;
                            }
                        }
                        Logger.WriteLine("No ships were found with the uniqueID (" + stat.shipid + ") requested by remote client.", Logger.LogType.WARN);
                        return false;
                    }
                }
                Logger.WriteLine("Player #" + stat.playerid + " was not found as requested by remote client.", Logger.LogType.WARN);
            }
            return false;
        }
        protected virtual bool Handler(PlayShipAnimation anim, Client sender)
        {
            using (var playl = GalaxyController.Players)
            {
                Player player = playl.Get().Where(x => x.PlayerKey == anim.playerkey).FirstOrDefault();
                if (player == null)
                {
                    Logger.WriteLine("Could not find player associated with requested ship animation. Player #" + anim.playerkey + " Ship #" + anim.shipkey, Logger.LogType.WARN);
                    return true;
                }
                lock (player.ship_lock)
                {
                    Ship ship = player.Ships.Where(x => x.UniqueID == anim.shipkey).FirstOrDefault();
                    if (ship == null)
                    {
                        Logger.WriteLine("Could not find ship associated with requested ship animation. Player #" + anim.playerkey + " Ship #" + anim.shipkey, Logger.LogType.WARN);
                        return true;
                    }

                    ship.AddAnimation(anim.animType, DateTime.UtcNow - anim.endTime.Subtract(DateTime.UtcNow.Subtract(anim.UTCTimeSent)), anim.scale, anim.zorder);
                }
            }
            return true;
        }
        protected virtual bool Handler(PlayerForceUpdate plu, Client sender)
        {
            if (!sender.Server)
                return false;

            using (var playl = GalaxyController.Players)
            {
                IEnumerable<Player> players = playl.Get().Where(x => x.PlayerName == plu.playerName);
                if (players.Count() > 1)
                    players = players.Where(x => x.PlayerKey == plu.prevkey);
                if (players.Count() != 1)
                {
                    Logger.WriteLine("Could not determine difference between players when given a player update.", Logger.LogType.CRITICAL);
                    return false;
                }
                Player pl = players.First();
                pl.PlayerKey = plu.key;
                pl.PlayerRace = plu.assignedRace;
                lock (pl.ship_lock)
                {
                    pl.Ships.ForEach(x => x.UniqueID = (x.UniqueID - plu.prevkey * Player.MaxShipCount) + plu.key * Player.MaxShipCount);
                }
            }
            return false;
        }
        protected virtual bool Handler(MoveShip move, Client sender)
        {
            Map mp;
            using (var mapl = GalaxyController.Maps)
            {
                var dict = mapl.Get();
                if (!dict.ContainsKey(move.currentmapid))
                {
                    Logger.WriteLine("Could not find map #" + move.currentmapid + " to order move on", Logger.LogType.WARN);
                    return GlobalNetwork.Server;
                }
                mp = dict[move.currentmapid];
            }
            mp.MoveShipToPointLocal(move.shipid, move.movePoint, move.add, move.finalrot);
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(PlayerIncome income, Client sender)
        {
            using (var playlock = GalaxyController.Players)
            {
                foreach (Player p in playlock.Get())
                {
                    if (p.PlayerKey == income.playerid)
                        p.SetMoney(income.money);
                }
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(AttackShip attack, Client sender)
        {
            Map mp;
            using (var mapl = GalaxyController.Maps)
            {
                var dict = mapl.Get();
                if (!dict.ContainsKey(attack.currentmapid))
                {
                    Logger.WriteLine("Could not find map #" + attack.currentmapid + " to prioritize attack order on", Logger.LogType.WARN);
                    return GlobalNetwork.Server;
                }
                mp = dict[attack.currentmapid];
            }
            mp.PrioritizeShipTarget(attack.attackingshipid, attack.targetshipid, attack.add);
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(DoDamage dmg, Client sender)
        {
            Player pl;
            using (var playl = GalaxyController.Players)
            {
                pl = playl.Get().Where(x => x.PlayerKey == dmg.playerid).FirstOrDefault();
            }
            if (pl != null)
            {
                lock (pl.ship_lock)
                {
                    Ship ship = pl.Ships.Where(y => y.UniqueID == dmg.shipid).FirstOrDefault();
                    if (ship != null)
                    {
                        ship.ReceiveDamage(dmg.damage);
                    }
                    else
                        Logger.WriteLine("Could not find ship #" + dmg.shipid + " to add damage to", Logger.LogType.WARN);
                }
            }
            else
                Logger.WriteLine("Could not find player #" + dmg.playerid + " to add damage to", Logger.LogType.WARN);

            return false;
        }
        protected virtual bool Handler(UpdateObjective ob, Client sender)
        {
            Map mp;
            using (var mapc = GalaxyController.Maps)
            {
                var dict = mapc.Get();
                if (!dict.ContainsKey(ob.MapKey))
                {
                    Logger.WriteLine("Could not find map #" + ob.MapKey + " to add objective update to", Logger.LogType.WARN);
                    return GlobalNetwork.Server;
                }
                mp = dict[ob.MapKey];
            }
            mp.ChangeObjectiveState(ob.ObjectiveIndex, ob.ControllingRace, ob.StartAmount);
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(DoAttack dmg, Client sender)
        {
            Map mp;
            using (var mapc = GalaxyController.Maps)
            {
                var dict = mapc.Get();
                if (!dict.ContainsKey(dmg.mapid))
                {
                    Logger.WriteLine("Could not find map #" + dmg.mapid + " to add damage to", Logger.LogType.WARN);
                    return GlobalNetwork.Server;
                }
                mp = dict[dmg.mapid];
            }
            mp.AddAttackIndicator(dmg.sourceshipid, dmg.targetshipid, dmg.damage, dmg.direction);
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(YourDead dead, Client sender)
        {
            if (!sender.Server)
                return false;

            Map mp;
            using (var mapl = GalaxyController.Maps)
            {
	    	var dict = mapl.Get();
		if (!dict.ContainsKey(dead.mapid))
		    return false;
		mp = dict[dead.mapid];
            }
            mp.AddDeadShip(dead.shipid);
            return false;
        }
        protected virtual bool Handler(ChatMessage stat, Client sender)
        {
            ChatInterface.ReceiveChatMessage(stat);
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(RegisterShipToMap shipReg, Client sender)
        {
            if (!sender.Server && !GlobalNetwork.Server)
                return false;

            Map ent;
            using (var mapl = GalaxyController.Maps)
            {
                var dict = mapl.Get();
                if (!dict.ContainsKey(shipReg.mapId))
                {
                    Logger.WriteLine("Ship not added: Could not find map " + shipReg.mapId + " to add ship #" + shipReg.shipid + " owned by player #" + shipReg.playerid, Logger.LogType.ERROR);
                    return GlobalNetwork.Server;
                }
                ent = dict[shipReg.mapId];
            }
            using (var players = GalaxyController.Players)
            {
                Player pl = players.Get().Where(x => x.PlayerKey == shipReg.playerid).FirstOrDefault();
                Ship ship = null;
                if (pl != null)
                {
                    lock (pl.ship_lock)
                    {
                        ship = pl.Ships.Where(y => y.UniqueID == shipReg.shipid).FirstOrDefault();
                        if (ship != null)
                        {
                            if (shipReg.register)
                                ent.AddShip(ref ship);
                            else
                                ent.RemoveShip(ship);
                        }
                        else
                        {
                            Logger.WriteLine("Ship not added: Could not find ship #" + shipReg.shipid, Logger.LogType.ERROR);
                        }
                    }
                }
                else
                {
                    Logger.WriteLine("Ship not added: Could not find player #" + shipReg.playerid, Logger.LogType.ERROR);
                }
            }
            return GlobalNetwork.Server;
        }
        protected virtual bool Handler(GalaxyTransfer stat, Client sender)
        {
            if (!GlobalNetwork.Server)
                InitSync.Instance.CheckForMap(stat.MapXMLSegment, stat.files);
            return false;
        }
        protected virtual bool Handler(FileDownload fileDown, Client sender)
        {
            MD5 hash = MD5.Create();
            if (fileDown.request)
            {
                FileDownload fd = new FileDownload(fileDown.fileName);
                fd.file = File.ReadAllBytes(fileDown.fileName);
                fd.MD5Hash = hash.ComputeHash(fd.file);
                fd.request = false;

                sender.Send(fd);
            }
            else
            {
                if (!GlobalNetwork.Server)
                {
                    try
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(fileDown.fileName));
                        FileStream fs = new FileStream(fileDown.fileName, FileMode.Create);
                        fs.Write(fileDown.file, 0, fileDown.file.Length);
                        fs.Flush();
                        fs.Close();

                        using (fs = new FileStream(fileDown.fileName, FileMode.Open))
                        {
                            if (!hash.ComputeHash(fs).SequenceEqual(fileDown.MD5Hash))
                                sender.Send(new FileDownload(fileDown.fileName));
                        }

                        InitSync.Instance.fileAquired(fileDown.fileName);
                    }
                    catch
                    {
                        sender.Send(new FileDownload(fileDown.fileName));
                    }
                }
            }
            return false;
        }
    }
}
