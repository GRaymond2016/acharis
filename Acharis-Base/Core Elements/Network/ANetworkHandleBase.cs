﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Core_Elements.Network;
using System.Reflection;

namespace Acharis.Core_Elements.Network
{
    public abstract class ANetworkHandleBase
    {
        private readonly object m_dictionarylock = new object();
        private Dictionary<Type, MethodInfo> registeredHandlers = null;
        public bool CredSupplied { get; set; }

        public bool InvokeByType(ASerial param, Client sender)
        {
            if (sender.Disposing) return true;
            lock (m_dictionarylock)
            {
                if (registeredHandlers == null)
                {
                    registeredHandlers = new Dictionary<Type, MethodInfo>();
                    List<MethodInfo> methods = this.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
                    foreach (MethodInfo meth in methods)
                    {
                        ParameterInfo[] @params = meth.GetParameters();
                        if (@params.Length > 0 && meth.Name == "Handler" && @params[0].ParameterType.BaseType == typeof(ASerial))
                            registeredHandlers.Add(@params[0].ParameterType, meth);
                    }
                }
                try
                {
                    lock (sender.DisposingLock)
                    {
                        if (sender.Disposing) return true;
                        if (!registeredHandlers.ContainsKey(param.GetType()) && GlobalNetwork.Server)
                        {
                            sender.Send(new ClosingConnection("Invalid message sent from your machine. Server could not handle given message."));
                            sender.DisconnectClient("Invalid message given from client.");
                            return false;
                        }
                        return (bool)registeredHandlers[param.GetType()].Invoke(this, new object[] { param, sender });
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteLine(e.Message + Environment.NewLine + " Stack:" + e.StackTrace, Logger.LogType.CRITICAL);
                    return true;
                }
            }
        }
    }
}
