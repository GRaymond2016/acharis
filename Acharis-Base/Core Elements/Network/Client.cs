﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.Text;
using Acharis.Core_Elements;

namespace Acharis.Core_Elements.Network
{
    public class Client
    {
        public delegate void HandleDC(string message);
        public event HandleDC ClientDisconnect;

        private readonly object m_playerLock = new object();
        private readonly object m_socketLock = new object();
        private List<Socket> Sockets { get; set; }
        private List<string> Players { get; set; }

        public bool Server { get; set; }
        public string ServerName { get; set; }
        public static ANetworkHandleBase Handler { get; set; }
        public object DisposingLock = new object();
        public bool Disposing { get; set; }
        public bool ReadyForDisposal { get; set; }
        public String PlayerString
        {
            get
            {
                String player = "";
                lock (m_playerLock)
                {
                    if (Players == null)
                        return "Unknown Player";

                    bool first = false;
                   
                    foreach (string s in Players)
                    {
                        if (!first)
                            first = true;
                        else
                            player += ", ";
                        player += s;
                    }
                }
                return player;
            }
        }

        public int SocketCount
        {
            get
            {
                lock (m_socketLock)
                {
                    return Sockets.Count;
                }
            }
        }

        public Client(List<Socket> sockets)
        {
            lock (m_socketLock)
            {
                Sockets = new List<Socket>(sockets);
            }
            lock (m_playerLock)
            {
                Players = new List<string>();
            }
        }
        public void Cleanup()
        {
            Disposing = true;
            lock (DisposingLock)
            {
                if (GlobalNetwork.Server)
                    Send(new ClosingConnection("Server has shut down."));
                CleanupInternal();
            }
        }
        private void CleanupInternal()
        {
            if (ReadyForDisposal)
                return;

            ReadyForDisposal = true;
            for (int i = 0; i < Sockets.Count; i++)
                Sockets[i].Close();

            lock (m_playerLock)
            {
                while (Players.Count > 0)
                {
                    IEnumerable<Game_Elements.Player> players = null;
                    using (var plays = GalaxyController.Players)
                    {
                        players = plays.Get().Where(x => x.PlayerName == Players[0]);
                        plays.Get().RemoveAll(x => x.PlayerName == Players[0]);
                    }
                    foreach (var play in players)
                        play.Cleanup();
                    Players.Remove(Players[0]);
                }
            }
        }
        public void ProcessMessage(object s, Socket sender)
        {
            if (s == null || Handler == null || Disposing) return;

            if (s.GetType() == typeof(string))
            {
                string tempS = (string)s;
                if (tempS == "name_resolve")
                {
                    Send("name:" + GlobalNetwork.LocalName);
                }
                return;
            }

            if (s.GetType().BaseType == typeof(ASerial) &&
                 Handler.InvokeByType(s as ASerial, this))
                    GlobalNetwork.SendToAll(s, sender);
        }
        public void DisconnectClient(string s)
        {
            if(ClientDisconnect != null)
                ClientDisconnect(s);

            CleanupInternal();
        }
        public void Send(object s, Socket noSend = null)
        {
            if (Disposing) return;
            List<Socket> errors = new List<Socket>();
            int sockCount = 1;
            lock (m_socketLock)
            {
                foreach (Socket sock in Sockets)
                {
                    try
                    {
                        lock (sock)
                        {
                            if (!sock.Connected) throw new Exception();
                            if (sock != noSend)
                                SendObject(s, sock);
                        }
                    }
                    catch 
                    {
#if DEBUG
                        StringBuilder sb = new StringBuilder();
                        lock (m_playerLock)
                        {
                            Players.ForEach(x => sb.Append(x + " "));
                        }
                        Logger.WriteLine("Client: A socket failed to send, removing socket from player collection: " + sb.ToString(), Logger.LogType.DEBUG);
#endif
                        errors.Add(sock); 
                    } //Remove all faulty sockets
                }

                foreach (Socket sock in errors)
                {
                    Sockets.Remove(sock);
                }
                sockCount = Sockets.Count;
            }
            if (sockCount < 1)
                CleanupInternal();
        }
        public static void SendObject(object obj, Socket con)
        {
            if (obj == null) return;
            
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            BinaryWriter bs = new BinaryWriter(ms);

            bs.Write((long)0);
            bs.Flush();
            bf1.Serialize(ms, obj);
            ms.Position = 0;
            bs.Write(ms.Length);
            bs.Flush();

            try
            {
                con.Send(ms.ToArray()); 
            }
            catch(Exception e)
            {
                Logger.WriteLine("Client: Cannot send type of: " + obj.GetType() + ". " + e.Message + ".", Logger.LogType.ERROR);
            }
        }
        public void AddPlayer(string playerName)
        {
            Logger.WriteLine("Adding player " + playerName, Logger.LogType.INFO);
            lock (m_playerLock)
            {
                Players.Add(playerName);
            }
        }
        public bool ContainsPlayer(string playerName)
        {
            lock (m_playerLock)
            {
                return Players.Contains(playerName);
            }
        }
        public bool Equals(Socket obj)
        {
            foreach (Socket s in Sockets)
                if (obj == s)
                    return true;
            return false;
        }
    }
}
