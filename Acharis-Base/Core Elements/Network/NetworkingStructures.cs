﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Acharis.Game_Elements;
using Acharis.Game_Elements.Combat;
using Acharis.Core_Elements.Configuration;

namespace Acharis.Core_Elements.Network
{
    [AttributeUsage(AttributeTargets.All)] //TODO: Implement UDP networking
    public class TcpPriority : System.Attribute {}

    [Serializable]
    public class ASerial
    {
        public DateTime UTCTimeSent = DateTime.UtcNow;
        public byte[] ToByteArray()
        {
            byte[] userDataBytes;
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            bf1.Serialize(ms, this);
            ms.WriteByte((byte)'\t');
            userDataBytes = ms.ToArray();
            return userDataBytes;
        }
    }

    [Serializable]
    public class KeepAlive
    {
        public string Keep = "KEEPALIVE";
    }

    [Serializable]
    public class UdpEnabled : ASerial
    {
        public string server;
        public bool enabled = true;
        public uint listening = 4955;
    }

    [Serializable]
    public class PlayerStats : ASerial
    {
        public bool add = true;
        public bool npc = false;
        public string playerName;
        public uint playerColour;
        public int key;
        public Race playerrace;
    }

    [Serializable]
    public class UpdatePlanet : ASerial
    {
        public Race ControllingRace;
        public int MapId;
    }

    [Serializable]
    public class UpdateObjective : ASerial
    {
        public Race ControllingRace;
        public int StartAmount;
        public int ObjectiveIndex;
        public int MapKey;
    }

    [Serializable]
    public class MoveFleet : ASerial
    {
        public string FleetName;
        public string PlayerName;
        public int PreviousMapId;
        public int MapId;
    }

    [Serializable]
    public class PlayShipAnimation : ASerial
    {
        public ShipAnimationTypes animType;
        public int shipkey;
        public int playerkey;
        public float scale;
        public ZOrder zorder;
        public DateTime endTime;
    }

    [Serializable]
    public class AddFleet : ASerial
    {
        public string FleetName;
        public string PlayerName;
        public int MapId;
    }

    [Serializable]
    public class PlayerForceUpdate : ASerial
    {
        public string playerName;
        public int prevkey;
        public int key;
        public Race assignedRace;
    }

    [Serializable]
    public class BuyMeAShip : ASerial
    {
        public int playerid;
        public ShipClass ship_class;
        public int planet_id = -1; //-1 for home_planet
    }

    [Serializable]
    public class LocationRequest : ASerial
    {
        public bool request = true;
        public int playerid;
        public int shipid;
        public Point location;
        public float rotation;
        public PointF currentVelocity;
        public DateTime lastRotation;
        public DateTime lastMove;
    }

    [Serializable]
    public class PlayerIncome : ASerial
    {
        public int playerid;
        public int money;
    }

    [Serializable]
    public class RequestInitData : ASerial
    {
    }

    [Serializable]
    public class FileDownload : ASerial
    {
        public FileDownload(string fn) { fileName = fn; }
        public string fileName;
        public byte[] MD5Hash = null;
        public byte[] file = null;
        public bool request = true;
    }

    [Serializable, TcpPriority]
    public class GalaxyTransfer : ASerial
    {
        public GalaxyTransfer(string xmlSeg, Dictionary<string, byte[]> s) { MapXMLSegment = xmlSeg; files = s; }
        public string MapXMLSegment;
        public Dictionary<string, byte[]> files;
    }

    [Serializable, TcpPriority]
    public class CheckUser : ASerial
    {
        public string username;
        public byte[] password;
        public bool accept = false;
    }

    [Serializable, TcpPriority]
    public class ClosingConnection : ASerial
    {
        public ClosingConnection(string s) { message = s; }
        public byte[] hash;
        public string message;
    }

    [Serializable, TcpPriority]
    public class ShipStat : ASerial
    {
        //Necissary
        public string imageLoc;
        public string name;
        public int shipid;
        public string ownerName;
        public string fleetName;
        public Race race;
        public ShipClass sclass;
        public Point location = new Point();
        public float rotation;
        public Point size = new Point();

        public int hue;

        //Other
        public int hp;
        public int shield;
    }

    [Serializable]
    public class MoveShip : ASerial
    {
        public bool add;
        public int currentmapid;
        public int shipid;
        public Point movePoint;
        public float finalrot;
    }

    [Serializable]
    public class AttackShip : ASerial
    {
        public bool add;
        public int currentmapid;
        public int attackingshipid;
        public int targetshipid;
    }

    [Serializable]
    public class ChatMessage : ASerial
    {
        public string exitPoint;
        public string playerSource;
        public string message;
    }

    [Serializable]
    public class DoAttack : ASerial
    {
        public AAttackType damage;
        public int mapid;
        public int targetshipid;
        public int sourceshipid;
        public float direction;
    }

    [Serializable]
    public class DoDamage : ASerial
    {
        public AAttackType damage;
        public int shipid;
        public int playerid;
    }

    [Serializable]
    public class YourDead : ASerial
    {
        public int mapid;
        public int shipid;
    }

    [Serializable, TcpPriority]
    public class RegisterShipToMap : ASerial
    {
        public int mapId;
        public int playerid;
        public int shipid;
        public bool register; // false for removal
    }

    [Serializable]
    public class MoveShipBetweenMaps : ASerial
    {
        public int previousMapId;
        public int newMapId;
        public int playerid;
        public int shipid;
        public DateTime complete;
    }
}
