﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Forms;

namespace Acharis.Core_Elements.Network
{
    public class NetworkBase
    {
        protected const int cm_port = 4950;

        protected readonly static object c_client_lock = new object();
        protected static List<Client> Clients = new List<Client>();

        public static bool Server { get; protected set; }
        public static bool Shutdown { get; set; }

        public static string LocalName { get { return g_computername; } set { g_computername = value; } }
        protected static string g_computername = SystemInformation.ComputerName;

        public delegate void NetworkEvent(ConnectionStates connected, string address, Exception error);

        public static event NetworkEvent m_disconnection_event; //Events are not thread safe
        public static event NetworkEvent m_client_connection_event; //Events are not thread safe

        private static void DoDisconnectionEvent(ConnectionStates connected, string address, Exception error)
        {
            if (m_disconnection_event == null) return;

            Delegate[] arr = m_disconnection_event.GetInvocationList();
            for (int i = 0; i < arr.Length; i++)
            {
                NetworkEvent et = (arr[i] as NetworkEvent);
                try
                {
                    et.Invoke(connected, address, error);
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Networking: Handler failed to run for disconnection event " + e.Message, Logger.LogType.ERROR);
                }
            }
        }

        private static void DoClientConnectedEvent(string address)
        {
            if (m_client_connection_event == null) return;
            Delegate[] arr = m_client_connection_event.GetInvocationList();
            for (int i = 0; i < arr.Length; i++)
            {
                NetworkEvent et = (arr[i] as NetworkEvent);
                try
                {
                    et.Invoke(ConnectionStates.Connected, address, null);
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Networking: Handler failed to run for connection event " + e.Message, Logger.LogType.ERROR);
                }
            }
        }

        protected static string NameResolve(Socket s)
        {
            try
            {
                Client.SendObject("name_resolve", s);
                for (int i = 0; i < 3; )
                {
                    object received = Receive(s);
                    if (received.GetType() == typeof(string))
                    {
                        if (received as string == "name_resolve")
                            Client.SendObject("name:" + LocalName, s);
                        else
                            if ((received as string).StartsWith("name:"))
                                return (received as string).Substring(5);
                            else
                                i++;
                    }
                }
                throw new Exception("Unable to resolve name.");
            }
            catch { }
            return "";
        }
        public static void SendToAll(object payload, Client noSend)
        {
            lock (c_client_lock)
            {
                foreach (Client s in Clients.SkipWhile(x => x == noSend))
                {
                    s.Send(payload);
                }
            }
        }
        public static void SendToAll(object payload, Socket noSend = null)
        {
            lock (c_client_lock)
            {
                foreach (Client s in Clients)
                    if(noSend == null || !s.Equals(noSend))
                        s.Send(payload, noSend);
            }
        }
        protected static object Receive(Socket con)
        {
            try
            {
                byte[] size = new byte[8];
                con.Receive(size);
                long cSize = BitConverter.ToInt64(size, 0) - 8;

                MemoryStream ms = new MemoryStream();
                byte[] buffer = new byte[1];

                DateTime end = DateTime.Now.AddSeconds(15);
                while (ms.Length < cSize)
                {
                    con.Receive(buffer);
                    ms.Write(buffer, 0, 1);

                    if (end < DateTime.Now) return null;
                }

                ms.Position = 0;
                BinaryFormatter bf1 = new BinaryFormatter();
                return (object)bf1.Deserialize(ms);
            }
            catch { return null; }
        }
        /// <summary>
        /// We add a client to the physcial client lists, the player data should be downloaded from the server
        /// through the internal event receiver, on top of this any other connections will be handled in this fashion based on player data.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newConnection"></param>
        /// <returns></returns>
        protected static bool AddClient(string name, Socket newConnection)
        {
            Client c = new Client(new List<Socket>() { newConnection });
            Receiver.NewReceiver(c, newConnection);
            lock (c_client_lock)
            {
                if (!Server && Clients.Count == 0)
                {
                    c.Server = true;
                    c.ServerName = name;
                }
                else
                    c.Server = false;

                Clients.Add(c);
            }

            DoClientConnectedEvent(name);
            return true;
        }
        public static void RemoveClient(Client cl)
        {
            lock (c_client_lock)
            {
                if (cl.Server)
                {
                    if (Shutdown == true)
                    {
                        DoDisconnectionEvent(ConnectionStates.Shutdown, cl.PlayerString, new Exception("Client side shutdown."));
                    }
                    else
                    {
                        Shutdown = true;
                        DoDisconnectionEvent(ConnectionStates.Ended, cl.PlayerString, new Exception("Connection to " + cl.ServerName + " has timed out."));
                    }
                }
                else
                {
                    DoDisconnectionEvent(ConnectionStates.Ended, cl.PlayerString, new Exception(cl.PlayerString + " has disconnected."));
                }

                cl.Cleanup();
                Clients.Remove(cl);
            }
        }

        private class Receiver
        {
            public Client pc { get; set; }
            public Socket s { get; set; }
            public static void NewReceiver(Client pc, Socket s)
            {
                Receiver r = new Receiver();
                r.pc = pc;
                r.s = s;
                new Thread(new ThreadStart(r.ReceiveLoop)).Start();
            }
            public void ReceiveLoop()
            {
                DateTime endTime = DateTime.Now.AddSeconds(15);

                String name = "Unknown";
                if (s.Connected)
                    name = s.RemoteEndPoint.ToString();

                while (!Shutdown && s.Connected)
                {
                    try
                    {
                        if (s.Available > 0)
                            pc.ProcessMessage(Receive(s), s);

                        if (endTime <= DateTime.Now)
                        {
                            pc.Send(new KeepAlive());
                            endTime = DateTime.Now.AddSeconds(15);
                        }
                        if (pc.ReadyForDisposal)
                        {
                            GlobalNetwork.RemoveClient(pc);
                        }
                    }
                    catch
                    {
                        break;
                    }

                    Thread.Sleep(50);
                }

                if (s.Connected)
                    s.Close();

                pc.Cleanup();
                GlobalNetwork.RemoveClient(pc);
            }
        }
    }
}
