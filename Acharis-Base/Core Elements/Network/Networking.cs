﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Threading.Tasks;

namespace Acharis.Core_Elements.Network
{
    public enum ConnectionStates
    {
        NotStarted,
        Failed,
        FailedT1,
        Connected,
        Ended,
        Shutdown
    }
    public class GlobalNetwork : NetworkBase
    {
        private static TcpListener gm_listener = null;
        private static object gm_udp_lock = new object();
        private static IPEndPoint gm_serverEndPoint = new IPEndPoint(IPAddress.Broadcast, 16011);
        private static IPEndPoint gm_clientEndPoint = new IPEndPoint(IPAddress.Broadcast, 16012);
        private static UdpClient gm_serverBroadcast = new UdpClient();
        private const string c_udp_ping_message = "PING_ACHARIS_SERVERS";


        static GlobalNetwork()
        {
            Shutdown = true;
            Server = false;
            gm_serverBroadcast.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            gm_serverBroadcast.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 1);
            gm_serverBroadcast.MulticastLoopback = true;
        }
        public static void GetServers(Action<string> output)
        {
            //Code to determine all servers. Xml / Text file + Broadcast over LAN
            PingForActiveServers(delegate(string s) { output(ResolveHostname(s)); }, 1);
        }
        public static void GetServersASync(Action<string> provided)
        {
            Task.Factory.StartNew(() => GetServers(provided));
        }
        public static void MakeDedicatedServer()
        {
            string local_end_point = GetLocalIPAddress();
            gm_serverBroadcast.Client.Bind(new IPEndPoint(IPAddress.Parse(local_end_point), gm_serverEndPoint.Port));

            Server = true;
            RespondToPendingServerPings();
        }
        public static void RespondToPendingServerPings()
        {
            if (Server)
                gm_serverBroadcast.BeginReceive(RespondToPingingClients, null);
        }
        private static void RespondToPingingClients(IAsyncResult res)
        {
            if (res.IsCompleted)
            {
                byte[] received;
                lock (gm_udp_lock)
                {
                    received = gm_serverBroadcast.EndReceive(res, ref gm_serverEndPoint);
                }
                string data = Encoding.ASCII.GetString(received);
                if (data.CompareTo(c_udp_ping_message) == 0)
                {
                    string ip = GetLocalIPAddress();
                    if (ip != null)
                    {
                        byte[] ip_as_data = Encoding.ASCII.GetBytes(ip);
                        gm_serverBroadcast.Send(ip_as_data, ip_as_data.Length, gm_clientEndPoint);
                    }
                }
            }

            if (!Shutdown)
                RespondToPendingServerPings();
        }
        public static string GetLocalIPAddress()
        {
            IPHostEntry entry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in entry.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return null;
        }
        public static void PingForActiveServers(Action<string> provided, int second_timeout = 5)
        {
            try
            {
                string local_end_point = GetLocalIPAddress();
                if (!gm_serverBroadcast.Client.IsBound)
                    gm_serverBroadcast.Client.Bind(new IPEndPoint(IPAddress.Parse(local_end_point), gm_clientEndPoint.Port));

                byte[] to_send = Encoding.ASCII.GetBytes(c_udp_ping_message);
                gm_serverBroadcast.Send(to_send, to_send.Length, gm_serverEndPoint);

                DateTime end = DateTime.Now.AddSeconds(second_timeout);
                while (DateTime.Now < end)
                {
                    gm_serverBroadcast.Client.ReceiveTimeout = (int) second_timeout;

                    byte[] data = gm_serverBroadcast.Receive(ref gm_clientEndPoint);
                    String data_readable = Encoding.ASCII.GetString(data);
                    IPAddress address;
                    if (IPAddress.TryParse(data_readable, out address))
                        provided(data_readable);
                }
            }
            catch
            {
            }
        }
        public static string ResolveHostname(string ip)
        {
            string hostname = Dns.GetHostEntry(ip).HostName;
            int ind = hostname.IndexOf('.');
            if (ind != -1)
                return hostname.Substring(0, ind);
            else
                return hostname;
        }
        public static string ResolveIPAddress(string hostname)
        {
            IPAddress[] addresses = Dns.GetHostEntry(hostname).AddressList;
            foreach (IPAddress addr in addresses)
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                    return addr.ToString();
            return "";
        }
        public static void StartServer()
        {
            if (Shutdown)
            {
                g_computername = g_computername + "-SERVER";
                gm_listener = new TcpListener(IPAddress.Any, cm_port);
                gm_listener.AllowNatTraversal(true);
                gm_listener.Start();
                new Thread(new ThreadStart(Accept)).Start();
                Server = true;
                Shutdown = false;
            }
        }
        public static void Stop()
        {
            if (!Shutdown)
            {
                if (Server)
                {
                    if (gm_listener != null)
                        gm_listener.Stop();
                    int dind = g_computername.IndexOf("-SERVER");
                    if (dind > 0)
                        g_computername = g_computername.Remove(dind);
                }

                Shutdown = true;
                Server = false;
            }
        }
        public static void Accept()
        {
            while(!Shutdown)
            {
                try
                {
                    Socket newConnection = gm_listener.AcceptSocket();
                    if (!Shutdown)
                    {
                        string name = NameResolve(newConnection);
                        if (name != "")
                        {
                            Server = true;
                            AddClient(name, newConnection);
                            continue;
                        }
                    }
                    newConnection.Close();
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Accept: " + e.Message, Logger.LogType.ERROR);
                }
            }
        }
    }
}//End Namespace
