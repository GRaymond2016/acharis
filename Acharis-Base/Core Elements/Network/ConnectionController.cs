﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Acharis.Core_Elements.Network
{
    public class ConnectionController : NetworkBase
    {
        public bool Connected { get; private set; }
        public event Acharis.Core_Elements.Network.GlobalNetwork.NetworkEvent m_connection_event; //Events are not thread safe
        private Socket m_connectedSocket;
        private string m_connectedName;

        public void ReleaseToGlobal()
        {
            if (Connected)
                AddClient(m_connectedName, m_connectedSocket);
        }
        public void Close()
        {
            if (m_connectedSocket != null && m_connectedSocket.Connected)
                m_connectedSocket.Close();
            Connected = false;
        }
        public bool Connect(string address)
        {
            if (Connected)
            {
                Logger.WriteLine("Connection: Connection already accomplished.", Logger.LogType.CRITICAL);
                return false;
            }

            Connected = false;
            Shutdown = false;
            Socket s = null;

            try
            {
                s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (Exception e) // Unable to access sockets usually firewall caused
            {
                Logger.WriteLine("Connection: " + e.Message, Logger.LogType.ERROR);
                DoConnectionEvent(ConnectionStates.FailedT1, "", e);
                return false;
            }

            IPAddress[] ip = new IPAddress[0];
            try
            {
                ip = Dns.GetHostAddresses(address);
            }
            catch (Exception e)
            {
                Logger.WriteLine("Connection: " + e.Message, Logger.LogType.ERROR);
                DoConnectionEvent(ConnectionStates.FailedT1, "", e);
                return false;
            }

            foreach (IPAddress i in ip)
            {
                try
                {
                    s.BeginConnect(new IPEndPoint(i, cm_port), new AsyncCallback(AConnected), s);
                    break;
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Connection: " + e.Message, Logger.LogType.ERROR);
                    DoConnectionEvent(ConnectionStates.FailedT1, i.ToString(), e);
                    return false;
                }
            }
            return true;
        }
        private void AConnected(IAsyncResult iar)
        {
            Socket client = (Socket)iar.AsyncState;
            string address = "";

            try
            {
                client.EndConnect(iar);

                if (Connected) return;
                address = ((IPEndPoint)client.RemoteEndPoint).Address.ToString();

                m_connectedName = NameResolve(client);
                if (m_connectedName != "")
                {
                    m_connectedSocket = client;

                    Logger.WriteLine("Accept: " + m_connectedName + " Accepts.", Logger.LogType.DEBUG);
                    DoConnectionEvent(ConnectionStates.Connected, client.RemoteEndPoint.ToString(), null);
                    Connected = true;
                }
                else
                {
                    Logger.WriteLine("Accept: Failed due to invalid NameResolve call. Server did not correctly respond.", Logger.LogType.ERROR);
                    throw new Exception("Invalid server response");
                }
            }
            catch (Exception e)
            {
                Logger.WriteLine("Accept: Connection Failed. " + e.Message, Logger.LogType.ERROR);
                DoConnectionEvent(ConnectionStates.Failed, address, e);
            }
        }
        private void DoConnectionEvent(ConnectionStates connected, string address, Exception error)
        {
            if (m_connection_event == null) return;

            Delegate[] arr = m_connection_event.GetInvocationList();
            for (int i = 0; i < arr.Length; i++)
            {
                try
                {
                    (arr[i] as Acharis.Core_Elements.Network.GlobalNetwork.NetworkEvent).Invoke(connected, address, error);
                }
                catch (Exception e) { Logger.WriteLine("Connection: Handler failed to run for connection event. " + e.ToString(), Logger.LogType.ERROR); }
            }
        }
    }
}
