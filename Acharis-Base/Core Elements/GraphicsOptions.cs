﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Acharis.Game_Elements;

namespace Acharis.Core_Elements
{
    public class GraphicsOptions
    {
        private static AcharisConfig config = new AcharisConfig();
        private const string AAIndex = "Graphics\\AntiAlias", MultiIndex = "Graphics\\MultiSample", ModeInd = "Graphics\\Mode", MonitorInd = "Graphics\\MonitorIndex",
                    WI = "Graphics\\Bounds\\x", HI = "Graphics\\Bounds\\y", XI = "Graphics\\Position\\x", YI = "Graphics\\Position\\y";

        private static bool aa = false;
        private static bool ms = false;
        private static int mode = 0;
        private static Screen monitor = null;
        private static object resLock = new object();
        private static Point res;
        private static Point location;

        public static Screen Monitor { get { return monitor; } set { monitor = value; } }
        public static int MonitorID { get { return getMonitorId(); } }
        public static int Mode { get { return mode; } set { mode = value; } }
        public static bool MultiSampling { get { return ms; } set { ms = value; } }
        public static bool AntiAliasing { get { return aa; } set { aa = value; } }
        public static Point Resolution { get { lock (resLock) { return res; } } set { lock (resLock) { res = value; } } }
        public static Point Location { get { return location; } set { location = value; } }

        public static void updateFromFile()
        {
            try
            {
                if (config[AAIndex].CompareTo("0") == 0 || config[AAIndex].ToLower().CompareTo("false") == 0)
                    aa = false;
                if (config[AAIndex].CompareTo("1") == 0 || config[AAIndex].ToLower().CompareTo("true") == 0)
                    aa = true;
            }
            catch { aa = false; }
            try
            {
                if (config[MultiIndex].CompareTo("0") == 0 || config[MultiIndex].ToLower().CompareTo("false") == 0)
                    ms = false;
                if (config[MultiIndex].CompareTo("1") == 0 || config[MultiIndex].ToLower().CompareTo("true") == 0)
                    ms = true;
            }
            catch { ms = false; }

            int rem = -1;
            int.TryParse(config[ModeInd], out rem);

            if (rem >= 0 && rem <= 2)
                mode = rem;
            else
                mode = 0;

            //This is probably not applicable anymore.
            rem = -1;
            string monI = config[MonitorInd];

            if (int.TryParse(monI, out rem) && rem < Screen.AllScreens.Length)
                monitor = Screen.AllScreens[rem];
            else
                monitor = Screen.PrimaryScreen;

            int w = 0, h = 0, x = 0, y = 0;
            string tw = config[WI], th = config[HI], tx = config[XI], ty = config[YI];

            if(int.TryParse(tw, out w) && int.TryParse(th, out h) && 
                            monitor.Bounds.Width >= w && monitor.Bounds.Height >= h)
                res = new Point(w, h);
            else
                res = new Point(monitor.Bounds.Width, monitor.Bounds.Height);

            if (int.TryParse(tx, out x) && int.TryParse(ty, out y) &&
                        monitor.Bounds.Contains(new System.Drawing.Point(x,y)))
                location = new Point(x, y);
            else
                location = new Point(monitor.Bounds.X, (monitor.Bounds.Y < 0)?0:monitor.Bounds.Y);
        }
        public static void saveToFile()
        {
            config[AAIndex] = AntiAliasing.ToString();
            config[MultiIndex] = MultiSampling.ToString();
            config[ModeInd] = Mode.ToString();
            config[MonitorInd] = MonitorID.ToString();
            config[WI] = Resolution.X.ToString();
            config[HI] = Resolution.Y.ToString();
            config[XI] = Location.X.ToString();
            config[YI] = Location.Y.ToString();
            config.CommitChanges();
        }
        private static int getMonitorId()
        {
            int iden = 0;
            foreach (Screen s in Screen.AllScreens)
            {
                if (s.Equals(monitor))
                    break;
                else
                    iden++;
            }
            return iden;
        }
        public static void setAll(bool a, bool s, int mod, Screen mon)
        {
            aa = a;
            ms = s;
            mode = mod;
            monitor = mon;
            saveToFile();
        }
    }
}
