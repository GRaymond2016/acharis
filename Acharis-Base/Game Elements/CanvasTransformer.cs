﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements
{
    public class CanvasTransformer
    {
        public Rectangle TotalMapBounds {get;set;}
        public Rectangle ViewableMapBounds {get;set;}
        public Rectangle DrawBounds {get;set;}

        private void ErrorCheck()
        {
            if (TotalMapBounds == null || ViewableMapBounds == null || DrawBounds == null)
                throw new NotSupportedException("A function was called in CanvasTransformer before all necissary variables were set.");
        }
        public Rectangle TransformRectToDraw(Rectangle inputRect)
        {
            ErrorCheck();

            Point topleft = TransformPointToDraw(new Point(inputRect.X, inputRect.Y));
            Point bottomright = TransformPointToDraw(new Point(inputRect.Right, inputRect.Bottom));

            if (topleft == null || bottomright == null) return null;

            return new Rectangle(topleft.X, topleft.Y, bottomright.X - topleft.X, bottomright.Y - topleft.Y);
        }
        public Point TransformPointToDraw(Point p)
        {
            ErrorCheck();

            return TransformPointGeneric(DrawBounds, ViewableMapBounds, new Point(p.X, p.Y));
        }
        public Rectangle TransformRectToMap(Rectangle inputRect)
        {
            ErrorCheck();

            Point topleft = TransformPointToMap(new Point(inputRect.X, inputRect.Y));
            Point bottomright = TransformPointToMap(new Point(inputRect.Right, inputRect.Bottom));

            if(topleft == null || bottomright == null) return null;

            return new Rectangle(topleft.X, topleft.Y, bottomright.X - topleft.X, bottomright.Y - topleft.Y);
        }
        public Point TransformPointToMap(Point p)
        {
            ErrorCheck();
            float xfac = (float)ViewableMapBounds.Width / (float)DrawBounds.Width, yfac = (float)ViewableMapBounds.Height / (float)DrawBounds.Height;
            return new Point((int)Math.Round((float)p.X * xfac) + ViewableMapBounds.X, (int)Math.Round((float)p.Y * yfac) + ViewableMapBounds.Y);
        }
        private Point TransformPointGeneric(Rectangle rect1, Rectangle rect2, Point p)
        {
            //Define modifiers
            float xfac = (float)rect1.Width / (float)rect2.Width, yfac = (float)rect1.Height / (float)rect2.Height;
            bool xneg = p.X < rect2.X, yneg = p.Y < rect2.Y;

            //Get an absolute difference between points
            if(p.X < 0 && rect2.X < 0)
                p.X = (int)(Math.Abs(p.X) - Math.Abs(rect2.X));
            else
                p.X = (int)Math.Abs(rect2.X - p.X);

            if(p.Y < 0 && rect2.Y < 0)
                p.Y = (int)(Math.Abs(p.Y) - Math.Abs(rect2.Y));
            else
                p.Y = (int)Math.Abs(rect2.Y - p.Y);

            //If the point is less than the rect we record the difference negatively
            p.X *= xneg ? -1 : 1;
            p.Y *= yneg ? -1 : 1;

            //Scale the resulting point based on factor and then add the rects x,y which adjust for differing screens
            return new Point((int)((float)p.X * xfac + rect1.X), (int)((float)p.Y * yfac + rect1.Y));
        }
    }
}