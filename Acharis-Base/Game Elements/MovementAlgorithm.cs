﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Core_Elements;

namespace Acharis.Game_Elements
{
    public class MovementAlgorithm
    {
        //pass -1 as sections parameter to autochoose based on rotation
        public static int GetMovementSector(float rot)
        {
            int sector = -1;
            if (rot > 337 || rot <= 22) sector = 0;
            if (rot > 22 && rot <= 67) sector = 1;
            if (rot > 67 && rot <= 112) sector = 2;
            if (rot > 112 && rot <= 157) sector = 3;
            if (rot > 157 && rot <= 202) sector = 4;
            if (rot > 202 && rot <= 247) sector = 5;
            if (rot > 247 && rot <= 292) sector = 6;
            if (rot > 292 && rot <= 337) sector = 7;

            return sector;
        }
        public static double CalculateRotationBetweenPoints(Point start, Point end)
        {
            return CalculateRotationBetweenPoints(new PointF(start.X, start.Y), new PointF(end.X, end.Y));
        }
        public static double CalculateRotationBetweenPoints(PointF start, PointF end)
        {
            if (start == end)
            {
                Logger.WriteLine("Asked for rotation between identical points.", Logger.LogType.WARN);
                return 0;
            }

            double A = Math.Abs(start.X - end.X);
            double B = Math.Abs(start.Y - end.Y);
            //Radian Conversion
            double angle = Math.Atan(B / A) * 180 / Math.PI;
            if ((start.X >= end.X && start.Y <= end.Y) || (start.X <= end.X && start.Y >= end.Y))
            {
                angle = (90 - angle);
            }

            if (start.X < end.X)
                angle += (start.Y <= end.Y) ? 90 : 0;
            else if (start.X == end.X)
                angle += (start.Y < end.Y) ? 180 : 0;
            else
                angle += (start.Y >= end.Y) ? 270 : 180;
            return angle;
        }
        public static float GetDifferenceOfRotation(float current, float target)
        {
            float diff = current - target;
            if (Math.Abs(diff) > 180)
                diff = Math.Sign(-diff) * ((360 - Math.Max(current, target)) + Math.Min(current, target));
            return diff;
        }
    }
}
