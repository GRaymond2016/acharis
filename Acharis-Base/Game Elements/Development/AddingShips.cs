﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.Combat;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;

namespace Acharis.Game_Elements.Development
{
    public class debug_AddingShips
    {
        private static void GenerateRandomShipCombo(ref Race race, ref ShipClass classes)
        {
            Random r = new Random();

            string[] race_types = Enum.GetNames(typeof(Race));
            string[] ship_classes = Enum.GetNames(typeof(ShipClass));

            Ship ai_temp = null;
            while (ai_temp == null)
            {
                classes = (ShipClass)Enum.Parse(classes.GetType(), ship_classes[r.Next(ship_classes.Length)]);
                race = (Race)Enum.Parse(race.GetType(), race_types[r.Next(race_types.Length)]);

                ai_temp = ShipFactory.GetShip(race, classes);
            }
        }
        public static void AddShip(Map map, Player player)
        {
            Race r = Race.Endlorian;
            ShipClass c = ShipClass.Cruiser;
            GenerateRandomShipCombo(ref r, ref c);
            AddShip(map, r, c, player);
        }
        public static void AddAIShip(Map map, Player player)
        {
            Race r = Race.Endlorian;
            ShipClass c = ShipClass.Cruiser;
            GenerateRandomShipCombo(ref r, ref c);
            AddShip(map, r, c, player);
        }
        public static void AddShip(Map map, Race race, ShipClass fac, Player player)
        {
            Ship template = SetupShip(map.Bounds, race, fac, player);
            if (template != null)
            {
                Fleet fleet = new Fleet()
                {
                    Name = "Neutral Fleet",
                    Location = map,
                    Owner = player,
                    Ships = new List<Ship>()
                };
                player.AddFleet(fleet);
                AddFleet afl = new AddFleet()
                {
                    FleetName = fleet.Name,
                    MapId = fleet.Location.MapKey,
                    PlayerName = fleet.Owner.PlayerName
                };
                GlobalNetwork.SendToAll(afl);

                ApplyShipToGame(template, player, map);
            }
        }
        public static void AddAIShip(Map map, Race race, ShipClass fac, Player player)
        {
            Random r = new Random();

            Ship template = SetupShip(map.Bounds, race, fac, player);
            if (template != null)
            { 
                ApplyShipToGame(template, player, map);
                for (int i = 0; i < 125; ++i)
                {
                    Point movePoint = new Acharis.Game_Elements.Point(r.Next(map.Bounds.Width), r.Next(map.Bounds.Height));
                    template.EnqueuePath(movePoint);
                    MoveShip movement = new MoveShip()
                    {
                        add = true,
                        currentmapid = map.MapKey,
                        shipid = template.UniqueID,
                        movePoint = movePoint
                    };
                    GlobalNetwork.SendToAll(movement);
                }
            }
        }
        private static Ship SetupShip(Rectangle bounds, Race race, ShipClass fac, Player player)
        {
            Random r = new Random();

            Ship template = ShipFactory.GetShip(race, fac);
            if (template != null)
            {
                template.CurrentPosition = new Acharis.Game_Elements.PointF(r.Next(bounds.Width), r.Next(bounds.Height));
                template.Owner = player;
                template.SetShipID(player.NextShipID());
                template.Rotation = r.Next(0, 361);
            }
            return template;
        }
        private static void ApplyShipToGame(Ship s, Player pl, Map mp)
        {
            s.AssociatedMap = mp;
            pl.AddShip(s, true);
        }
    }
}
