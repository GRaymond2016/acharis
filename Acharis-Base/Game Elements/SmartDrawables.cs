﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements
{
    class ProgressPie : PieShape
    {
        public int PercentageComplete 
        { 
            set 
            {
                int perc = Math.Min(100, Math.Max(0, value));
                AnglesToRemove.Clear();
                int amount = (int) Math.Round(360.0 * (perc * 0.01));
                if (amount <= 90)
                {
                    AnglesToRemove.Add(new Point(270, amount + 270));
                    return;
                }
                   
                AnglesToRemove.Add(new Point(270, 360));
                AnglesToRemove.Add(new Point(0, amount - 90));
            } 
        }

    }
}
