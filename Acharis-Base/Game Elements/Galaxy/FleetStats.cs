﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements.Galaxy
{
    public class FleetStats
    {
        public int EnemyFleets { get; set; }
        public int AlliedFleets { get; set; }
        public int YourFleets { get; set; }
    }
}
