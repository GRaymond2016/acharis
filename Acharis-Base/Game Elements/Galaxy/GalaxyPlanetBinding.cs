﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements.Galaxy;

namespace Acharis.Game_Elements.GalaxyMapNamespace
{
    public class GalaxyPlanetBinding
    {
        public string ThumbPath;
        public string Anchor;
        public Point Bounds;
        public int AssociatedMap;
        public Planet AssociatedPlanet;
        public FleetStats AssociatedFleetStats;
        public int LinkDistance;
    }
}
