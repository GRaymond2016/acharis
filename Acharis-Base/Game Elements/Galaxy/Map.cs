﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Acharis.Core_Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Acharis.Core_Elements.Network;
using Acharis.Core_Elements.Configuration;
using System.Threading;
using Acharis.Game_Elements.Combat;

namespace Acharis.Game_Elements.GalaxyMapNamespace
{
    public class Map : ISupplyDrawables
    {
        private static MapConfigurer mconfig = new MapConfigurer();
        private ReaderWriterLock m_shipLock = new ReaderWriterLock();

        public class StaticObject
        {
            public Point Position { get; set; }
            public string Image { get; set; }
        }
        private const long damagesMoveDelay = 10;

        private object dead_lock = new object();
        private List<Ship> dead = new List<Ship>();

        private List<string> mapChat = new List<string>();
        private List<MapConfigurer.StaticObject> Objects = new List<MapConfigurer.StaticObject>();
        public string Background { get; private set; }
        
        public delegate void UpdateShip(Map context, Ship ship, bool added);

        private List<UpdateShip> subscribers = new List<UpdateShip>();
        private List<Objective> objectives = new List<Objective>();
        private string ObjectiveOwned;
        private string ObjectiveEnemy;
        private string ObjectiveNeutral;
        private Dictionary<int, Ship> Ships { get; set; }
        public Rectangle Bounds { get; private set; }

        public Rectangle SelectionBounds { get; set; }
        public Point SelectionPoint { get; set; }
        public ShipClass SpawnType { get; set; }
        public bool SelectInverseMode { get; set; }
        public int CountSelectedShips { get; private set; }
        public Race ControllingRace { get; set; }
        public int ControllingAmount { get; private set; }
        public int ControlIncome { get; set; }

        private const int ObjectiveMaxCount = 800, ObjectiveIncrement = 40;

        public string MapName { get; set; }
        public int MapKey { get; set; }

        private TargetAcquirer acquirer = new TargetAcquirer();
        private object damages_lock = new object();
        private List<KeyValuePair<Ship, IAttack>> damages = new List<KeyValuePair<Ship, IAttack>>();
        private object m_objectiveLock = new object();
        public class RacialConquest
        {
            public Race current_owner;
            public Race current_capper;
            public bool conflicting;
            public int amount;
        }
        private Dictionary<Objective, RacialConquest> m_objectiveShipCount = new Dictionary<Objective, RacialConquest>();
        private DateTime lastObjectiveUpdate = DateTime.UtcNow;

        public delegate void PlanetControlChange(Race controlling, int mapName);
        public event PlanetControlChange planet_change;

        public delegate void ObjectiveControlChange(Race controlling, int startAmount, int mapKey, int obKey);
        public event ObjectiveControlChange ob_change;

        public Formation SelectedFormation { get; set; }

        public Map(string mapName)
        {
            ControllingRace = Race.None;
            ControllingAmount = 0;
            SelectedFormation = new BoxFormation();

            GalaxyController.remove_ship += new GalaxyController.RemoveShipFromMaps(GalaxyController_remove_ship);

            CountSelectedShips = 0;
            MapName = Path.GetFileNameWithoutExtension(mapName);
            using(WriteLockWrapper wrap = new WriteLockWrapper(m_shipLock))
            {
                Ships = new Dictionary<int, Ship>();
            }
            var template = MapConfigurer.GetMap(MapName);
            if (template == null)
            {
                Logger.WriteLine("Unable to load " + mapName + ". Quitting.", Logger.LogType.CRITICAL);
                throw new ArgumentException("Invalid file " + mapName + ". Could not load.");
            }
            ObjectiveOwned = template.ObjectiveOwned;
            ObjectiveEnemy = template.ObjectiveEnemy;
            ObjectiveNeutral = template.ObjectiveNeutral;

            Background = template.BackgroundPath;
            Bounds = new Rectangle(0, 0, template.Bounds.X, template.Bounds.Y);
            MapKey = template.Key;

            Objects = template.Objects;
            objectives = template.Objectives;

            SelectInverseMode = false;

            foreach (Objective ob in objectives)
                m_objectiveShipCount.Add(ob, new RacialConquest()
                {
                     current_owner = Race.None,
                     current_capper = Race.None,
                     conflicting = false,
                     amount = 0
                });
        }
        public void SendStateToClient(Client sender)
        {
            UpdatePlanet planet = new UpdatePlanet()
            {
                ControllingRace = ControllingRace,
                MapId = MapKey
            };
            sender.Send(planet);

            foreach(var obj in m_objectiveShipCount)
            {
                UpdateObjective objective = new UpdateObjective()
                {
                    ControllingRace = obj.Value.current_owner,
                    StartAmount = obj.Value.amount,
                    MapKey = MapKey,
                    ObjectiveIndex = obj.Key.ObjectiveKey
                };
                sender.Send(objective);
            }
        }
        public void GalaxyController_remove_ship(int s)
        {
            RemoveShip(s);
        }
        public void AddAttackIndicator(int source, int destin, AAttackType type, float rotation)
        {
            Ship src = null, dest = null;
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                if (Ships.ContainsKey(source))
                    src = Ships[source];
                if (Ships.ContainsKey(destin))
                    dest = Ships[destin];
            }
            if (src == null)
            {
                Logger.WriteLine("Unable to add damage to the map source ship ID # " + source + " was not registered to map", Logger.LogType.WARN);
                return;
            }
            if (dest == null)
            {
                Logger.WriteLine("Unable to add damage to the map destination ship ID # " + source + " was not registered to map", Logger.LogType.WARN);
                return;
            }
            lock (damages_lock)
            {
                IAttack attack = src.GetAttackByName(type.AttackFormName, dest.CurrentPosition.ToPoint());
                attack.Target = rotation;
                damages.Add(new KeyValuePair<Ship, IAttack>(src, attack));
            }
        }
        public void AddDeadShip(int s)
        {
            Ship sh = null;;
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                if(Ships.ContainsKey(s))
                    sh = Ships[s];
            }
            if (sh != null)
            {
                lock (dead_lock)
                {
                    dead.Add(sh);
                }
            }
        }
        public List<ShipStat> GetFullShipUpdates()
        {
            using (ReadLockWrapper ll = new ReadLockWrapper(m_shipLock))
            {
                List<ShipStat> ret = new List<ShipStat>();
                foreach(KeyValuePair<int, Ship> s in Ships)
                    ret.Add(s.Value.ShipStat);

                return ret;
            }
        }
        public List<int> GetRegisteredShipIDs()
        {
            using (ReadLockWrapper ll = new ReadLockWrapper(m_shipLock))
            {
                return Ships.Keys.ToList();
            }
        }
        public List<LocationRequest> GetShipLocations()
        {
            using (ReadLockWrapper ll = new ReadLockWrapper(m_shipLock))
            {
                List<LocationRequest> ret = new List<LocationRequest>();
                foreach (KeyValuePair<int, Ship> s in Ships)
                    ret.Add(s.Value.LocationUpdate);
                return ret;
            }
        }
        public List<RegisterShipToMap> GetShipRegistrations()
        {
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                List<RegisterShipToMap> ret = new List<RegisterShipToMap>();
                foreach (KeyValuePair<int, Ship> s in Ships)
                    ret.Add(s.Value.ShipRegistration);
                return ret;
            }
        }
        public void ChangeObjectiveState(int index, Race owner, int startingCap)
        {
            lock (m_objectiveLock)
            {
                var obj = objectives.Where(x => x.ObjectiveKey == index).FirstOrDefault();
                if (obj == null)
                {
                    Logger.WriteLine("Could not change objective state, the objective key " + index + " did not match any in our collection.", Logger.LogType.WARN);
                    return;
                }

                var objective = m_objectiveShipCount[obj];
                objective.current_owner = owner;
                objective.current_capper = Race.None;
                objective.amount = startingCap;
            }
        }
        public void MoveToPoint(Point p, bool addition, float finalrotation)
        {
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                string cp = GalaxyController.CurrentPlayer.PlayerName;
                int sel = -1;
                foreach(KeyValuePair<int, Ship> s in Ships)
                    if(s.Value.GetBoundingBox().Contains(p) && s.Value.Owner.PlayerName != cp)
                    {
                        sel = s.Key;
                        break;
                    }

                Formation formation = SelectedFormation.BeginFormation(p);
                double rotation = default(double);
                foreach (KeyValuePair<int, Ship> s in Ships)
                {
                    if (s.Value.Selected)
                    {
                        if (sel == -1 && sel != s.Value.UniqueID)
                        {
                            
                            if (finalrotation != -1)
                            {
                                s.Value.FinalRotation = finalrotation;
                                if(rotation == default(double))
                                    rotation = finalrotation;
                            }
                            else
                            {
                                if(rotation == default(double))
                                    rotation = MovementAlgorithm.CalculateRotationBetweenPoints(s.Value.CurrentPosition.ToPoint(), p);
                            }

                            Point myPoint = formation.GetNextPoint(s.Value.GetBoundingBox(), rotation);

                            if (!addition)
                                s.Value.Target = myPoint;
                            else
                                s.Value.EnqueuePath(myPoint);
                            
                            MoveShip ms = new MoveShip()
                            {
                                movePoint = myPoint,
                                currentmapid = MapKey,
                                shipid = s.Key,
                                add = addition,
                                finalrot = finalrotation
                            };
                            GlobalNetwork.SendToAll(ms);
                        }
                        else
                        {
                            acquirer.Prioritize(s.Key, sel, addition);
                            AttackShip ash = new AttackShip()
                            {
                                targetshipid = sel,
                                currentmapid = MapKey,
                                attackingshipid = s.Key,
                                add = addition
                            };
                            GlobalNetwork.SendToAll(ash);
                        }
                    }
                }
            }
        }
        public void MoveShipToPointLocal(int id, Point p, bool addition, float finalrot)
        {
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                if (Ships.ContainsKey(id))
                {
                    Ship sh = Ships[id];
                    if (!addition)
                        sh.Target = p;
                    else
                        sh.EnqueuePath(p);

                    if (finalrot != -1)
                        sh.FinalRotation = finalrot;
                }
            }
        }
        public void PrioritizeShipTarget(int id, int target, bool addition)
        {
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                if (Ships.ContainsKey(id))
                {
                    acquirer.Prioritize(id, target, addition);
                }
            }
        }
        public void Subscribe(UpdateShip s)
        {
            subscribers.Add(s);
        }
        public void Unsubscribe(UpdateShip s)
        {
            subscribers.Remove(s);
        }
        private void SupplySubs(Ship s, bool accept)
        {
            foreach (var t in subscribers) t(this, s, accept);
        }
        public void AddShip(ref Ship s)
        {
            using (WriteLockWrapper rl = new WriteLockWrapper(m_shipLock))
            {
                s.Selected = false;
                if (!Ships.ContainsKey(s.UniqueID))
                {
                    Ships.Add(s.UniqueID, s);
                    s.AssociatedMap = this;
                }
                else
                {
                    Logger.WriteLine("Ship #" + s.UniqueID + " attempted to be added to map " + MapName + " more than once.", Logger.LogType.WARN);
                    return;
                }
            }
            acquirer.AddShip(s);
            SupplySubs(s, true);
        }
        public void RemoveShip(Ship s)
        {
            RemoveShip(s.UniqueID);
        }
        public void RemoveShip(int index)
        {
            Ship s = null;
            using (WriteLockWrapper rl = new WriteLockWrapper(m_shipLock))
            {
                if (Ships.ContainsKey(index))
                {
                    s = Ships[index];
                    Ships.Remove(index);
                    s.AssociatedMap = null;
                }
                else
                {
                    Logger.WriteLine("Ship #" + index + " attempted to be removed from map " + MapName + " but was not present in the collection.", Logger.LogType.WARN);
                    return;
                }
            }
            if (s != null)
            {
                s.Selected = false;
                SupplySubs(s, false);
                acquirer.RemoveShip(s);
            }
        }
        private int GetColour(int r, int g, int b, int a)
        {
            return r + (g << 8) + (b << 16) + (a << 24);
        }
        public void NotifyMapOwnerChange()
        {
            if (planet_change != null)
                foreach (var i in planet_change.GetInvocationList())
                    i.DynamicInvoke(new object[] { ControllingRace, MapKey });
        }
        public void NotifyMapObjectiveChange(int index, RacialConquest conq)
        {
            if (!GlobalNetwork.Server)
                return;

            if (ob_change != null)
                foreach (var func in ob_change.GetInvocationList())
                    func.DynamicInvoke(new object[] { conq.current_owner, conq.amount, MapKey, index });
        }
        public void CalculateObjectiveModifications(Dictionary<Objective, RacialConquest> objectiveShipCount, int obMax, int obInc)
        {
            int humanCount = 0, endCount = 0, tchuanCount = 0;
            foreach (var keyp in objectiveShipCount)
            {
                var value = objectiveShipCount[keyp.Key];
                if (value.current_capper == value.current_owner)
                    value.amount = Math.Min(obMax, value.amount + obInc);
                else if (value.current_capper != Race.None)
                {
                    value.amount = Math.Max(0, value.amount - obInc);
                    if (value.amount == 0)
                    {
                        value.current_owner = value.current_capper;
                        NotifyMapObjectiveChange(keyp.Key.ObjectiveKey, value);
                    }
                }
                switch (value.current_owner)
                {
                    case Race.Human: humanCount++; break;
                    case Race.Tchuan: tchuanCount++; break;
                    case Race.Endlorian: endCount++; break;
                    case Race.None: break;
                }
            }

            int result = Math.Max(tchuanCount, Math.Max(humanCount, endCount));
            bool humRes = humanCount == result, endRes = endCount == result, tchRes = tchuanCount == result;
            //If two races share the lead it is contested, so we end here.
            if ((humanCount == endCount || humanCount == tchuanCount) && humRes ||
                (endCount == tchuanCount || endCount == humanCount) && endRes ||
                (tchuanCount == endCount || tchuanCount == humanCount) && tchRes)
            {
                return;
            }

            Race current_capper = Race.None;
            if (humRes)
                current_capper = Race.Human;
            else if (endRes)
                current_capper = Race.Endlorian;
            else if (tchRes)
                current_capper = Race.Tchuan;

            if (current_capper == Race.None)
            {
                Logger.WriteLine("Error in objective calculation, capper was invalid. This is a programmer error.", Logger.LogType.WARN);
                return;
            }

            if (ControllingRace == current_capper)
                ControllingAmount = Math.Min(20, ControllingAmount + 1);
            else
            {
                ControllingAmount = Math.Max(0, ControllingAmount - 1);
                if (ControllingAmount == 0)
                {
                    ControllingRace = current_capper;
                    NotifyMapOwnerChange();
                }
            }
        }
        public void CalculateDamages()
        {
            Player currentPlayer = GalaxyMapNamespace.GalaxyController.CurrentPlayer;
            for (int i = damages.Count - 1; i > -1; --i)
            {
                var damage = damages[i];
                if (damage.Value.Dead)
                {
                    damages.Remove(damages[i]);
                }
                else
                {
                    Point location = damage.Value.Move();
                    if (location.Valid && !Bounds.Contains(location))
                    {
                        damages.Remove(damages[i]);
                    }
                }
                using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
                {
                    foreach (Ship s in Ships.Values)
                    {
                        if (damage.Key.UniqueID != s.UniqueID && damage.Value.Receive(s) && GlobalNetwork.Server)
                        {
                            switch (s.ReceiveDamage(damage.Value.GetAttackType()))
                            {
                                case AttackState.DeathAndHit:
                                    YourDead youdead = new YourDead()
                                    {
                                        mapid = MapKey,
                                        shipid = s.UniqueID
                                    };
                                    GlobalNetwork.SendToAll(youdead);
                                    s.Die();
                                    lock (dead_lock)
                                    {
                                        dead.Add(s); 
                                    }
                                    break;
                                case AttackState.Hit:
                                    DoDamage dmg = new DoDamage()
                                    {
                                        shipid = s.UniqueID,
                                        playerid = s.Owner.PlayerKey,
                                        damage = damage.Value.GetAttackType()
                                    };
                                    GlobalNetwork.SendToAll(dmg);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        public void CalculateShipMovements(ref Dictionary<Objective, RacialConquest> objectiveShipCount)
        {
            foreach (var conq in objectiveShipCount.Keys)
            {
                var obj = objectiveShipCount[conq];
                obj.conflicting = false;
                obj.current_capper = Race.None;
            }

            List<DoAttack> attacks = new List<DoAttack>();
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                foreach (Ship s in Ships.Values)
                {
                    if (s.Owner.PlayerKey == GalaxyController.CurrentPlayer.PlayerKey)
                    {
                        KeyValuePair<int, IAttack> ss = acquirer.TryAttackingClosestTarget(s);
                        if (ss.Key != -1 && ss.Value.GetType() != typeof(NullAttack))
                        {
                            DoAttack damage = new DoAttack()
                            {
                                damage = new AAttackType()
                                {
                                    AttackFormName = ss.Value.GetType().Name,
                                    Damage = ss.Value.Damage,
                                    Type = AttackType.Projectile
                                },
                                targetshipid = ss.Key,
                                sourceshipid = s.UniqueID,
                                mapid = s.AssociatedMap.MapKey
                            };
                            attacks.Add(damage);
                            damages.Add(new KeyValuePair<Ship, IAttack>(s, ss.Value));
                        }
                    }

                    s.Move();
                    s.RotateToTarget();

                    Rectangle selection_box = s.GetSelectionBox();
                    foreach (Objective objective in objectives)
                    {
                        Point closest = selection_box.ClosestPoint(objective.Location);
                        int distance = closest.DistanceTo(objective.Location);
                        if (distance <= objective.Radius)
                        {
                            var obj = objectiveShipCount[objective];
                            if (obj.current_capper == Race.None && !obj.conflicting)
                                obj.current_capper = s.Faction;
                            else if (obj.current_capper != s.Faction)
                            {
                                obj.current_capper = Race.None;
                                obj.conflicting = true;
                            }
                        }
                    }
                }
            }
            foreach (DoAttack attack in attacks)
                GlobalNetwork.SendToAll(attack);
        }
        public void RemoveDeadShips()
        {
            using (WriteLockWrapper rl = new WriteLockWrapper(m_shipLock))
            {
                lock (dead_lock)
                {
                    foreach (Ship s in dead)
                    {
                        RemoveShip(s);
                        acquirer.RemoveShip(s);
                    }
                    dead.Clear();
                }
            }
        }
        public void UpdateLocalObjectives()
        {
            //Hackity Hack Hack. Not sure what I want to do with this yet.
            if (GlobalNetwork.Server || lastObjectiveUpdate > DateTime.UtcNow.Subtract(TimeSpan.FromMilliseconds(50)))
                return;

            CalculateObjectiveModifications(m_objectiveShipCount, ObjectiveMaxCount, ObjectiveIncrement / 20);
            lastObjectiveUpdate = DateTime.UtcNow;
        }
        public void UpdateObjectives()
        {
            lock (m_objectiveLock)
            {
                CalculateObjectiveModifications(m_objectiveShipCount, ObjectiveMaxCount, ObjectiveIncrement);
            }
        }
        public void Update()
        {
            lock (m_objectiveLock)
            {
                CalculateShipMovements(ref m_objectiveShipCount);
                UpdateLocalObjectives();
            }
            CalculateDamages();
            RemoveDeadShips();
        }
        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            Player currentPlayer = GalaxyMapNamespace.GalaxyController.CurrentPlayer;
            foreach (Objective o in objectives)
            {
                var race = m_objectiveShipCount[o];

                MetaTexture text = new MetaTexture()
                {
                    Bounds = new Rectangle(o.Location.X - o.Radius, o.Location.Y - o.Radius, o.Radius * 2, o.Radius * 2),
                    SizeScale = 1.0f,
                };

                if (currentPlayer.PlayerRace == race.current_owner)
                    text.Texture = ObjectiveOwned;
                else if (race.current_owner == Race.None)
                    text.Texture = ObjectiveNeutral;
                else if (race.current_owner != currentPlayer.PlayerRace)
                    text.Texture = ObjectiveEnemy;
                else
                    return;

                draws.Enqueue(text);

                ProgressPie pie = new ProgressPie();
                pie.SizeScale = 0.8f;
                pie.Bounds = text.Bounds;
                pie.PercentageComplete = (int)(race.amount / (double)ObjectiveMaxCount * 100);
                if (currentPlayer.PlayerRace == race.current_capper &&
                    currentPlayer.PlayerRace != race.current_owner)
                {
                    pie.Color = unchecked((uint)((100 << 24) + (50 << 16) + (200 << 8) + 50));
                    draws.Enqueue(pie);
                }
                else if (currentPlayer.PlayerRace == race.current_owner &&
                          currentPlayer.PlayerRace != race.current_capper)
                {
                    pie.Color = unchecked((uint)((100 << 24) + (50 << 16) + (50 << 8) + 200));
                    draws.Enqueue(pie);
                }
            }
            lock (damages_lock)
            {
                foreach (KeyValuePair<Ship, IAttack> ls in damages)
                {
                    IAttack attack = ls.Value;
                    if (!attack.Dead)
                        draws.Enqueue(attack.Drawable);
                }
            }
            using (ReadLockWrapper rl = new ReadLockWrapper(m_shipLock))
            {
                CountSelectedShips = 0;
                foreach (Ship s in Ships.Values)
                {
                    s.AddMyDrawables(ref draws);
                    if (currentPlayer.OwnsShip(s))
                    {
                        //Find selected ships
                        Rectangle shipBox = s.GetSelectionBox();
                        if (SelectionPoint != null)
                        {
                            if(shipBox.Contains(SelectionPoint))
                            {
                                if (!SelectInverseMode)
                                    s.Selected = !s.Selected;
                                else
                                    s.Selected = true;
                            }
                            else
                                s.Selected = false;
                        }
                        else if (SelectionBounds != null)
                        {
                            s.Selected = SelectionBounds.Contains(shipBox) == 2;
                        }

                        //Now draw the current target for that ship
                        if (s.Selected)
                        {
                            CountSelectedShips++;
                            KeyValuePair<int, Point> target = acquirer.GetCurrentTarget(s as IAttackingObject);
                            if (target.Key != -1 && Ships.ContainsKey(target.Key))
                            {
                                Ships[target.Key].AddTargetDrawable(ref draws);
                            }
                        }
                    }
                }
                SelectionBounds = null;
                SelectionPoint = null;
                SelectInverseMode = false;
            }
        }
    }
}
