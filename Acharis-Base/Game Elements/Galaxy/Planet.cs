﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Configuration;
using Acharis.Game_Elements.Combat;

namespace Acharis.Game_Elements.GalaxyMapNamespace
{
    public class Planet
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Population { get; set; }
        public int Owner { get; set; }
        public int Income { get; set; }
        public int Type { get; set; }
        public Race RacialHome { get; set; }

        public Planet(string planetLocationName)
        {
            PlanetConfigurer.Parse(this, planetLocationName);
        }

    }
}
