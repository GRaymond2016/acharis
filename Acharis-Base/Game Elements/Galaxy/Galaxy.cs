﻿using System.Collections.Generic;
using System.Linq;
using Acharis.Core_Elements.Network;
using System.Threading;
using System;
using System.Diagnostics;

namespace Acharis.Game_Elements.GalaxyMapNamespace
{
    public class ReadLockWrapper : IDisposable
    {
        private bool dummylock = false;
        private ReaderWriterLock m_wrap;
        public ReadLockWrapper(ReaderWriterLock wrap)
        {
            m_wrap = wrap;
            if (m_wrap.IsWriterLockHeld)
            {
                dummylock = true;
                return;
            }

            m_wrap.AcquireReaderLock(500);
            if (!m_wrap.IsReaderLockHeld)
                throw new Exception("Could not acquire read lock within 30 seconds.");
        }
        ~ReadLockWrapper()
        {
            if (m_wrap.IsReaderLockHeld)
                throw new Exception("Read lock was incorrectly disposed, lock still held but destructor called.");
        }
        public void Dispose()
        {
            if (!dummylock)
                m_wrap.ReleaseReaderLock();
        }
    }
    public class WriteLockWrapper : IDisposable
    {
        private bool dummylock = false;
        private ReaderWriterLock m_wrap;
        public WriteLockWrapper(ReaderWriterLock wrap)
        {
            m_wrap = wrap;
            if (m_wrap.IsWriterLockHeld)
            {
                dummylock = true;
                return;
            }

            m_wrap.AcquireWriterLock(500);
            if (!m_wrap.IsWriterLockHeld)
                throw new UnauthorizedAccessException("Could not acquire write lock within 30 seconds.");
        }
        ~WriteLockWrapper()
        {
            if (m_wrap.IsWriterLockHeld)
                throw new Exception("Write lock was incorrectly disposed, lock still held but destructor called.");
        }
        public void Dispose()
        {
            if (!dummylock)
                m_wrap.ReleaseWriterLock();
        }
    }
    public class LockingWrapper<T> : IDisposable
    {
        private T m_protectedObject;
        private bool m_locked = false;
        private object m_lockObject;
#if DEBUG
        private StackTrace m_begin;
#endif

        public LockingWrapper(ref object toLock, ref T protectedObject, bool initialLock = true) 
        {
            #if DEBUG
            m_begin = new StackTrace();
            #endif
            m_lockObject = toLock; 
            m_protectedObject = protectedObject;
            if(initialLock == true)
                Lock();
        }
        ~LockingWrapper()
        {
            if (m_locked)
                throw new Exception("LockingWrapper was incorrectly disposed, lock still held but destructor called.");
        }
        public void Dispose()
        {
            Unlock();
        }
        public T Get()
        {
            return m_protectedObject;
        }
        public bool Lock(int timeout = -1)
        {
            if (m_locked == false)
            {
                m_locked = Monitor.TryEnter(m_lockObject, (timeout == -1 ? 30000 : timeout));
            }
            return m_locked;
        }
        public void Unlock()
        {
            if (m_locked == true)
            {
                Monitor.Exit(m_lockObject);
                m_locked = false;
            }
        }
    }

    //Not a good way to do this. Thread safety.
    public static class GalaxyController
    {
        public static List<GalaxyPlanetBinding> m_internalPlanets = new List<GalaxyPlanetBinding>();
        public static Dictionary<int, Map> m_internalMaps = new Dictionary<int, Map>();
        public static List<Player> m_internalPlayers = new List<Player>();

        private static object m_internalPlayerLock = new object();
        private static object m_internalMapLock = new object();
        private static object m_internalPlanetLock = new object();

        public static LockingWrapper<List<Player>> Players
        {
            get { return new LockingWrapper<List<Player>>(ref m_internalPlayerLock, ref m_internalPlayers); }
        }
        public static LockingWrapper<Dictionary<int, Map>> Maps
        {
            get { return new LockingWrapper<Dictionary<int, Map>>(ref m_internalMapLock, ref m_internalMaps); }
        }
        public static LockingWrapper<List<GalaxyPlanetBinding>> Planets
        {
            get { return new LockingWrapper<List<GalaxyPlanetBinding>>(ref m_internalPlanetLock, ref m_internalPlanets); }
        }
        public static void AddMap(Map map)
        {
            using (var mp = Maps)
            {
                mp.Get()[map.MapKey] = map;
                if (map_added != null)
                    map_added(map);
            }
        }
        public static void RemoveShip(int uniqueIndex)
        {
            if (remove_ship != null)
                remove_ship(uniqueIndex);
        }

        public static Player CurrentPlayer 
        { 
            get 
            {
                using (var playl = Players)
                {
                    return playl.Get().Where(x => x.PlayerName == CurrentPlayerName).FirstOrDefault();
                }
            } 
        }
        public static string GalaxyMap { get; set; }
        public static Microsoft.Xna.Framework.Game RunningGame { get; set; }
        public static string CurrentPlayerName { get { return GlobalNetwork.LocalName; } }

        public delegate void MapAdded(Map mp);
        public static event MapAdded map_added;

        public delegate void RemoveShipFromMaps(int shipID);
        public static event RemoveShipFromMaps remove_ship;
    }
}
