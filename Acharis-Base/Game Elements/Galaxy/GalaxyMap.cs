﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.GalaxyMapNamespace;

namespace Acharis.Game_Elements.Galaxy
{        
    public struct DrawableShip
    {
        public int ID { get; set; }
        public string Texture { get; set; }
        public Combat.ShipClass Class { get; set; }
    };
    public class GalaxyMap : ISupplyDrawables
    {
        private struct InternalStructure
        {
            public Rectangle Bounds { get; set; }
            public int LinkDistance { get; set; }
            public List<InternalStructure> LinkedMaps { get; set; }
        };

        private Dictionary<GalaxyMapNamespace.Map, InternalStructure> internal_map = new Dictionary<GalaxyMapNamespace.Map, InternalStructure>();
        public void AddMap(GalaxyMapNamespace.Map map, Rectangle location, int LinkDistance)
        {
            InternalStructure str = new InternalStructure()
            {
                Bounds = location,
                LinkDistance = LinkDistance,
                LinkedMaps = new List<InternalStructure>()
            };
            ProcessMap(ref internal_map, ref str);
            internal_map.Add(map, str);
        }
        private void ProcessMap(ref Dictionary<GalaxyMapNamespace.Map, InternalStructure> map, ref InternalStructure str)
        {
            foreach (var m in map)
            {
                int distance = str.Bounds.Center.DistanceTo(m.Value.Bounds.Center);
                if(distance <= str.LinkDistance || distance <= m.Value.LinkDistance)
                {
                    str.LinkedMaps.Add(m.Value);
                    m.Value.LinkedMaps.Add(str);
                }
            }
        }
        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            foreach (var m in internal_map)
            {
                Circle c = new Circle()
                {
                    Bounds = m.Value.Bounds,
                    SizeScale = 1.0f,
                    Width = 4,
                    Points = 128
                };
                switch (m.Key.ControllingRace)
                {
                    case Combat.Race.None: c.Color = 0x88888888; break;
                    case Combat.Race.Endlorian: c.Color = 0x883333ff; break;
                    case Combat.Race.Human: c.Color = 0x88ff3333; break;
                    case Combat.Race.Tchuan: c.Color = 0x8866ff66; break;
                }
                draws.Enqueue(c);

                foreach (var s in m.Value.LinkedMaps)
                {
                    Line l = new Line()
                    {
                        StartPoint = s.Bounds.Center,
                        EndPoint = m.Value.Bounds.Center,
                        Color = 0x88888888
                    };
                    draws.Enqueue(l);
                }
            }
        }
    }
}
