﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace Acharis.Core_Elements
{
    class SpriteEditing
    {
        private static Stack<String> exceptionStack = new Stack<String>();

        public static Image ResizeImage(Image FullsizeImage,Size siz)
        {
            Bitmap newG = new Bitmap(siz.Width, siz.Height);
            Graphics g = Graphics.FromImage(newG);
            g.DrawImage(FullsizeImage, new Rectangle(0, 0, siz.Width, siz.Height));
            return newG;
        }
        public static unsafe Bitmap[] splitImage(Bitmap e)
        {
            int splitPoint = e.Height / 3;
            Bitmap[] bp = new Bitmap[3];
            for (int i = 0; i < 3; i++)
            {
                Bitmap b1 = new Bitmap(e.Width, splitPoint);
                BitmapData b = e.LockBits(new Rectangle(0, 0, e.Width, e.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                BitmapData be = b1.LockBits(new Rectangle(0, 0, b1.Width, b1.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                for (int x = 0; x < e.Width; x++)
                {
                    for (int y = 0; y < splitPoint; y++)
                    {
                        byte* row = (byte*)b.Scan0 + ((splitPoint * i + y) * b.Stride) + (x * 4);
                        byte* row2 = (byte*)be.Scan0 + (y * be.Stride) + (x * 4);
                        for (int i2 = 0; i2 < 4; i2++)
                        {
                            row2[i2] = row[i2];
                        }
                    }
                }
                bp[i] = b1;
                b1.UnlockBits(be);
                e.UnlockBits(b);
            }
            return bp;
        }
        public static Image ResizeImage(string File, Size dt)
        {
            return ResizeImage(Image.FromFile(File), dt);
        }
        public static Image ResizeImage(string File, int NewWidth, int NewHeight, bool OnlyResizeIfWider)
        {
            return ResizeShip(File, NewWidth, NewHeight, OnlyResizeIfWider);
        }
        public static Image ResizeShip(string File, int NewWidth, int NewHeight, bool OnlyResizeIfWider)
        {
            try
            {
                Image FullsizeImage = System.Drawing.Image.FromFile(File);

                // Prevent using images internal thumbnail
                FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

                if (OnlyResizeIfWider)
                {
                    if (FullsizeImage.Width <= NewWidth)
                    {
                        NewWidth = FullsizeImage.Width;
                    }
                }

                int TemHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
                if (TemHeight > NewHeight)
                {
                    // Resize with height instead
                    NewWidth = FullsizeImage.Width * NewHeight / FullsizeImage.Height;
                    TemHeight = NewHeight;
                }

                Image NewImage = ResizeImage(FullsizeImage, new Size(NewWidth, NewHeight));

                Bitmap padding = null;
                if (TemHeight > NewWidth)
                    padding = new Bitmap(TemHeight, TemHeight);
                if (NewWidth > TemHeight)
                    padding = new Bitmap(NewWidth, NewWidth);
                if (padding != null)
                {
                    padding.SetResolution(FullsizeImage.VerticalResolution, FullsizeImage.HorizontalResolution);
                    Graphics g = Graphics.FromImage(padding);
                    g.DrawImage(NewImage,new Point(0,0));
                }

                //FullsizeImage.Dispose();

                return NewImage;
            }
            catch (Exception ex)
            {
                if (!exceptionStack.Contains("Unable to load file:\"" + ex.Message + "\". Please ensure the file exists."))
                    MessageBox.Show("Unable to load file:\"" + ex.Message + "\". Please ensure the file exists.", "Exception Thrown", MessageBoxButtons.OK, MessageBoxIcon.Error);
                exceptionStack.Push("Unable to load file:\"" + ex.Message + "\". Please ensure the file exists.");
                return new Bitmap(NewWidth, NewHeight);
            }
        }
        public static Image rotateImage(Image b, float angle)
        {
            var newBitmap = new Bitmap(b.Width, b.Height);
            newBitmap.SetResolution(b.HorizontalResolution, b.VerticalResolution);
            var graphics = Graphics.FromImage(newBitmap);
            graphics.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
            graphics.RotateTransform(angle);
            graphics.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
            graphics.DrawImage(b, new Point(0, 0));
            return newBitmap;
        }
        public static double[] RGB_to_HSL(int R, int G, int B)
        {
            double[] i = new double[3];
            i[0] = R / 255.0;
            i[1] = G / 255.0;
            i[2] = B / 255.0;

            double maxcolor = 0, mincolor = 1;
            for (int iterator = 0; iterator != 3; iterator++) 
            {
                if (i[iterator] > maxcolor) maxcolor = i[iterator];
                if (i[iterator] < mincolor) mincolor = i[iterator];
            }
            double H = 0,S = 0,L = 0;
            L = (maxcolor + mincolor) / 2; 

            if(L < 0.5)S = (maxcolor-mincolor)/(maxcolor+mincolor);
            if(L >= 0.5)S = (maxcolor - mincolor) / (2.0 - maxcolor - mincolor);

            if(R == maxcolor * 255)H = (G-B)/(maxcolor-mincolor);
            if(G == maxcolor * 255)H = 2.0 + (B-R)/(maxcolor-mincolor);
            if(B == maxcolor * 255)H = 4.0 + (R - G) / (maxcolor - mincolor);

            return new double[3]{H,S,L};
        }
        public static Color HSB_to_RGB(double H, double S, double L)
        {
            double r = L, g = L, b = L;
            double temp1, temp2;
            double[] ret = new double[3];

            if (L == 0)
            {
                r = g = b = 0;
            }
            else
            {
                if (S == 0)
                {
                    r = g = b = L;
                }
                else
                {
                    temp2 = ((L <= 0.5) ? L * (1.0 + S) : L + S - (L * S));
                    temp1 = 2.0 * L - temp2;

                    H /= 360;
                    double[] temp3 = new double[3];
                    temp3[0] = H+1.0/3.0;
                    temp3[1] = H;
                    temp3[2] = H - 1.0 / 3.0;
                    
                    for (int i = 0; i != 3; i++)
                    {
                        if (temp3[i] < 0) temp3[i] += 1.0;
                        if (temp3[i] > 1) temp3[i] -= 1.0;

                        if (6.0 * temp3[i] < 1) ret[i] = temp1 + (temp2 - temp1) * 6.0 * temp3[i];
                        else if (2.0 * temp3[i] < 1) ret[i] = temp2;
                        else if (3.0 * temp3[i] < 2) ret[i] = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - temp3[i]) * 6.0;
                        else ret[i] = temp1;
                    }
                }
            }
            for (int i = 0; i != 3; i++)
            {
                if (ret[i] < 0) ret[i] = 0;
                if (ret[i] > 255) ret[i] = 255;
                if (double.IsNaN(ret[i])) ret[i] = 0;
            }
            return Color.FromArgb((int)(255 * ret[0]), (int)(255 *  ret[1]), (int)(255 * ret[2]));
        }
    }
}
