﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements.Combat;
using Acharis;

namespace Acharis.Game_Elements
{
    public interface IAttack
    {
        DrawableComponent Drawable { get; set; }
        float Target { get; set; }
        int Damage { get; set; }
        bool Dead { get; set; }
        DateTime LastDirectionMove { get; set; }

        bool Receive(Ship target);
        Point Move();
        AAttackType GetAttackType();
    }
    public class NullAttack : IAttack
    {
        public DrawableComponent Drawable { get; set; }
        public float Target { get; set; }
        public int Damage { get; set; }
        public bool Dead { get; set; }
        public DateTime LastDirectionMove { get; set; }

        public NullAttack()
        {
            LastDirectionMove = DateTime.UtcNow;
        }
        public Point Move()
        {
            return new Point();
        }
        public bool Receive(Ship target)
        {
            return false;
        }
        public AAttackType GetAttackType()
        { return new NormalAttackType() { Damage = 0 }; }
    }
    public class Projectile : DirectionalObject, IAttack
    {
        public DrawableComponent Drawable { get; set; }
        public float Target { get; set; }
        public int Damage { get; set; }
        public DateTime LastDirectionMove { get { return LastMove; } set { LastMove = value; } }

        private bool hashit = false;
        public bool Dead { get { return hashit; } set { hashit = value; } }

        public Projectile()
        {
            Speed = 5;
        }
        public new Point Move() 
        {
            MoveDirection = Target;
            base.Move();
            (Drawable as BoundedComponent).Bounds.Position = CurrentPosition.ToPoint();
            return CurrentPosition.ToPoint();
        }
        public bool Receive(Ship target)
        {
            if (target.HitsShield(CurrentPosition.ToPoint()))
            {
                hashit = true;
                return true;
            }
            return false;
        }
        public AAttackType GetAttackType()
        { return new NormalAttackType() { Damage = Damage }; }
    }
    public interface IAttackingObject
    {
        int UniqueID { get; set; }
        long LastAttack { get; set; }

        IAttack Attack(Point target, double distance);
        double GetAttackDistance(Point attacker);
        Point GetAttackPoint();
        Race GetFaction();
    }
    public interface IAttackableObject
    {
        AttackState ReceiveDamage(AAttackType type);
    }
}
 