﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;

namespace Acharis
{
    public class RotatingObject : MovingObject
    {
        public DateTime LastRotate { get; set; }
        public int RotationDelay { get; private set; }

        public float RotationScale { get; set; }
        public float TargetRotation { get; set; }
        public float Rotation { get; set; }

        public RotatingObject()
        {
            LastRotate = DateTime.UtcNow;
            RotationDelay = 15;
        }
        public bool Rotate()
        {
            float delta = (DateTime.UtcNow - LastRotate).Milliseconds / (float)RotationDelay;
            if (delta >= 1.0f)
            {
                if (Target.Valid)
                {
                    ReevaluateTargetRotation();
                }

                LastRotate = DateTime.UtcNow;
                if (Rotation != TargetRotation)
                {
                    float diff = MovementAlgorithm.GetDifferenceOfRotation(Rotation, TargetRotation);
                    float change = 0;
                    if (Math.Abs(diff) > RotationScale * delta)
                    {
                        if (diff > 0)
                            change -= RotationScale * delta;
                        else
                            change += RotationScale * delta;

                        Rotation += change;
                        if (Rotation > 360.0f)
                            Rotation = 0.0f;
                        if (Rotation < 0.0f)
                            Rotation = 360.0f;
                    }
                }
                else
                    return false;
            }
            return true;
        }
        public new void Move()
        {
            Rotate();
            base.Move();
        }
        protected void ReevaluateTargetRotation()
        {
            TargetRotation = EvaluateTargetRotations(CurrentPosition, new PointF(Target));
        }
        protected float EvaluateTargetRotations(PointF current, PointF target)
        {
            if (current.X == target.X || current.Y == target.Y)
                return (current.X == target.X) ? (current.Y < target.Y) ? 180 : 0 : (current.X < target.X) ? 90 : 270;
            else
                return (float)MovementAlgorithm.CalculateRotationBetweenPoints(current, target);
        }
    }
}
