﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;

namespace Acharis
{
    public class MovingObject : DirectionalObject
    {
        public virtual Point Target { get; set; }

        public new void Move()
        {
            MoveDirection = (float)MovementAlgorithm.CalculateRotationBetweenPoints(CurrentPosition, new PointF(Target));
            base.Move();
        }
    }
}
