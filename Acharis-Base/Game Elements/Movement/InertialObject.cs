﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;
using Acharis.Core_Elements;

namespace Acharis
{
    public class InertialObject : RotatingObject
    {
        enum Ruleset
        {
            None = 0,
            CloseRule,
            RotationRule
        };

        protected const float IntertialIncrease = 1.0f;
        protected const float IntertialDecrease = 0.50f;
        protected const double MaxReduction = 3.5;
        protected double CloseReduction = 1.0;
        protected double RotationReduction = 1.0;

        protected const double TurningModeMin = 0.9;
        protected const double TurningModeMax = 2.0;
        protected const double CloseMode = 0.10;
        protected const float Decay = 0.6f;
        protected double DiagMode = 0.5;

        protected PointF CurrentSpeed = new PointF(0,0);
        protected DateTime lastrulecheck = DateTime.UtcNow;
        protected readonly TimeSpan RuleCheckDelay = TimeSpan.FromMilliseconds(300);

        protected double[] CurrentIntertia = new double[8];
        public Point Size { get; protected set; } //Doesn't effect intertia yet
        public PointF CurrentVelocity { get { return new PointF(CurrentSpeed.X, CurrentSpeed.Y); } }

        private static int inertial_objects = 0;
        private int intertial_object_id;

        public InertialObject()
        {
            intertial_object_id = inertial_objects;
            inertial_objects++;
        }

        public new void Move()
        {
            float delta = (DateTime.UtcNow - LastMove).Milliseconds / (float)MoveDelay;
            for (int d = 0; d < (int)delta; d++)
            {
                double acspeed = 0;
                if (Target.Valid)
                {
                    ReevaluateTargetRotation();

                    CurrentSpeed.TranslateByMagnitude(Rotation, IntertialIncrease);
                    acspeed = Math.Sqrt((CurrentSpeed.X * CurrentSpeed.X) + (CurrentSpeed.Y * CurrentSpeed.Y));

                    if (lastrulecheck + RuleCheckDelay < DateTime.UtcNow)
                    {
                        Ruleset rules = Ruleset.None;
                        RecheckCloseRule(ref rules);
                        RecheckRotationRule(ref rules);
                        lastrulecheck = DateTime.UtcNow;
                    }

                    acspeed *= CloseReduction;
                    acspeed *= RotationReduction;
                }

                if (acspeed > Speed)
                {
                    CurrentSpeed.X *= Speed / (float)acspeed;
                    CurrentSpeed.Y *= Speed / (float)acspeed;
                }
                else
                {
                    CurrentSpeed.X *= Decay;
                    CurrentSpeed.Y *= Decay;
                }

                if (CurrentSpeed.X > 0.01 || CurrentSpeed.X < -0.01 || CurrentSpeed.Y > 0.01 || CurrentSpeed.Y < -0.01)
                {
                    CurrentPosition.X += CurrentSpeed.X;
                    CurrentPosition.Y += CurrentSpeed.Y;

                    Moving = true;
                }
                else
                    Moving = false;
            }
            LastMove = DateTime.UtcNow.Subtract(new TimeSpan((long)Math.Round(MoveDelay * (delta - (int)delta) * TimeSpan.TicksPerMillisecond)));
        }
        public void SetVelocity(PointF vel)
        {
            CurrentSpeed.X = vel.X;
            CurrentSpeed.Y = vel.Y;
        }
        private void RecheckCloseRule(ref Ruleset rule)
        {
            double closebounds = 200;
            double dist = Math.Abs(Math.Sqrt(Math.Pow(Target.X, 2) + Math.Pow(Target.Y, 2)) - Math.Sqrt(Math.Pow(CurrentPosition.X, 2) + Math.Pow(CurrentPosition.Y, 2)));
            //We are close to target & turning, reduce speed
            double distToTarget = (dist / closebounds);
            if (dist < closebounds && distToTarget > closebounds * 0.1)
            {
                CloseReduction += 1.0 - distToTarget;
                if (CloseReduction > MaxReduction)
                    CloseReduction = MaxReduction;
                rule |= Ruleset.CloseRule;
            }
            else if (CloseReduction > 1.01)
            {
                CloseReduction -= 1.0 - distToTarget;
            }
            else
            {
                CloseReduction = 1.0;
            }
        }
        private void RecheckRotationRule(ref Ruleset rule)
        {
            double initoffset = 90.0 - Math.Abs(MovementAlgorithm.GetDifferenceOfRotation(Rotation, TargetRotation));
            double offset = Math.Abs(initoffset);
            if (initoffset < 0)
            {
                RotationReduction -= (90 - offset) * 0.00001;
                if (RotationReduction < TurningModeMin)
                    RotationReduction = TurningModeMin;
                rule |= Ruleset.RotationRule;
            }
            else if (!rule.HasFlag(Ruleset.CloseRule))
            {
                RotationReduction += (90 - offset) * 0.001;
                if (RotationReduction > TurningModeMax)
                    RotationReduction = TurningModeMax;
                rule |= Ruleset.RotationRule;
            }
        }
    }
}
