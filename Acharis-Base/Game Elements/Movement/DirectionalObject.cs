﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;

namespace Acharis
{
    public class DirectionalObject
    {
        public DateTime LastMove { get; set; }
        public int MoveDelay { get; private set; }

        public float Speed { get; set; }
        public bool Moving { get; set; }

        protected PointF curPos = new PointF(0, 0);
        public PointF CurrentPosition
        {
            get { return curPos; }
            set { curPos = value; }
        }
        // This describes a mistake between a location source, we apply this position adjust gradually if greater than a certain threshold
        public const int PositionAdjustThresh = 25;
        public object sourceAnchorLock = new object();
        public PointF SourceAnchor
        {
            get; set;
        }

        protected float MoveDirection = 0.0f;

        public DirectionalObject()
        {
            LastMove = DateTime.UtcNow;
            MoveDelay = 25;
        }

        public void Move()
        {
            float delta = (DateTime.UtcNow - LastMove).Milliseconds / (float)MoveDelay;
            if (delta >= 1.0f)
            {
                CurrentPosition.TranslateByMagnitude(MoveDirection, delta * Speed);
                double dist = 0;
                lock (sourceAnchorLock)
                {
                    if (SourceAnchor != null)
                        SourceAnchor.TranslateByMagnitude(MoveDirection, delta * Speed);
                        dist = SourceAnchor.DistanceTo(CurrentPosition);
                        if (dist > PositionAdjustThresh)
                            CurrentPosition.TranslateByMagnitude((float) MovementAlgorithm.CalculateRotationBetweenPoints(CurrentPosition, SourceAnchor), delta * Speed * 0.2f);
                        else
                            SourceAnchor = null;
                }
                LastMove = DateTime.UtcNow;
                Moving = true;
            }
        }
    }
}
