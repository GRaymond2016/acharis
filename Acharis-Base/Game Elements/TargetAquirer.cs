﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Acharis.Core_Elements;

namespace Acharis.Game_Elements
{
    public class TargetAcquirer
    {
        internal class Target
        {
            public int Ship;
            public double Distance;
            public Point Position;
        };
        private Dictionary<int, List<Ship>> UserSelectedTargets = new Dictionary<int, List<Ship>>();
        private Dictionary<int, Ship> TargetList = new Dictionary<int, Ship>();
        private object TargetListLock = new object(), UserListLock = new object();

        public void Prioritize(int selectedShip, int shipToTarget, bool chain = false)
        {
            Ship target = TargetList[shipToTarget];
            lock (UserListLock)
            {
                if (!chain)
                    UserSelectedTargets[selectedShip].Clear();
                //No sense of priority yet.
                UserSelectedTargets[selectedShip].Add(target);
            }
        }
        public void AddShip(Ship s)
        {
            lock (TargetListLock)
            {
                UserSelectedTargets.Add(s.UniqueID, new List<Ship>());
                TargetList.Add(s.UniqueID, s);
            }
        }
        public void RemoveShip(Ship s)
        {
            lock (TargetListLock)
            {
                TargetList.Remove(s.UniqueID);
                UserSelectedTargets.Remove(s.UniqueID);
                foreach (List<Ship> ts in UserSelectedTargets.Values)
                    ts.RemoveAll(x => x.UniqueID == s.UniqueID);
            }
        }
        private Target GetClosestTarget(IEnumerable<Target> targets, IAttackingObject attacker)
        {
            double distance = double.MaxValue;
            Target minTarget = null;
            foreach (Target target in targets)
            {
                if (target == null)
                    continue;

                double dist = attacker.GetAttackDistance(target.Position);
                if (dist < distance)
                {
                    distance = dist;
                    minTarget = target;
                }
            }
            return minTarget;
        }
        private Target TargetFromShip(IAttackingObject source, Ship target)
        {
            return new Target()
            {
                Distance = source.GetAttackDistance(target.CurrentPosition.ToPoint()),
                Position = target.CurrentPosition.ToPoint(),
                Ship = target.UniqueID
            };
        }
        Target GetClosestTarget(IAttackingObject key)
        {
            lock (UserListLock)
            {
                List<Ship> userSelectedTargets = UserSelectedTargets[key.UniqueID];
                if (userSelectedTargets.Count > 0)
                    return GetClosestTarget(userSelectedTargets.Select(x => TargetFromShip(key, x)), key);
            }
            lock (TargetListLock)
            {
                return GetClosestTarget(TargetList.Values.Select(delegate(Ship x)
                {
                    if (x.UniqueID != key.UniqueID && x.Faction != key.GetFaction())
                        return TargetFromShip(key, x);
                    else
                        return null;
                }), key);
            }
        }
        public KeyValuePair<int,Point> GetCurrentTarget(IAttackingObject key)
        {
            Target target = GetClosestTarget(key);
            if (target == null)
                return new KeyValuePair<int, Point>(-1, new Point());
            return new KeyValuePair<int, Point>(target.Ship, target.Position);
        }
        public KeyValuePair<int, IAttack> TryAttackingClosestTarget(IAttackingObject key)
        {
            Target attack = GetClosestTarget(key);
            if (attack == null)
                return new KeyValuePair<int, IAttack>(-1, new NullAttack());

            IAttack pr = key.Attack(attack.Position, attack.Distance);
            if (pr == null)
                return new KeyValuePair<int, IAttack>(-1, new NullAttack());

            return new KeyValuePair<int, IAttack>(attack.Ship, pr);
        }
    }
}
