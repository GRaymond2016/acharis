﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements
{
    [Serializable]
    public class Rectangle
    {
        public Rectangle(int x, int y, int width, int height)
        {
            Size = new Point(width, height);
            Position = new Point(x, y);
        }
        public Point Size { get; set; }
        public Point Position { get; set; }
        public int X { get { return Position.X; } set { Position.X = value; } }
        public int Y { get { return Position.Y; } set { Position.Y = value; } }
        public int Height { get { return Size.Y; } set { Size.Y = value; } }
        public int Width { get { return Size.X; } set { Size.X = value; } }
        public int Right { get { return X + Width; } }
        public int Bottom { get { return Y + Height; } }
        public bool Valid { get { return X >= 0 && Y >= 0 && Width > 0 && Height > 0; } }

        public bool Contains(Point p)
        {
            return !(p.X <= Position.X || p.Y <= Position.Y || p.X >= Size.X + Position.X || p.Y >= Size.Y + Position.Y); 
        }
        public Point ClosestPoint(Point cp)
        {
            int x, y;

            if (cp.X < X)
                x = X;
            else if (cp.X > Right)
                x = Right;
            else
                x = cp.X;

            if (cp.Y < Y)
                y = Y;
            else if (cp.Y > Bottom)
                y = Bottom;
            else
                y = cp.Y;

            return new Point(x, y);
        }
        public Rectangle ScaleByFactor(float factor)
        {
            float wdx = Width * factor / 2,
                  hey = Height * factor / 2;
            return new Rectangle((int)(Center.X - wdx), (int)(Center.Y - hey), (int)(wdx * 2), (int)(hey * 2));
        }
        public Point ClosestEdge(Point cp)
        {
            Point [] points = new Point [] {new Point(X, cp.Y), new Point(cp.X, Y), new Point(Right, cp.Y), new Point(cp.X, Bottom)};
            Point ap = null;
            int apdist = int.MaxValue;
            foreach(Point p in points)
            {
                int distance = p.DistanceTo(cp);
                if (distance < apdist)
                {
                    apdist = distance; 
                    ap = p;
                }
            }
            return ap;
        }
        public Point Center
        {
            get
            {
                return new Point(Position.X + Size.X / 2, Position.Y + Size.Y / 2);
            }
        }
        public Point TopLeft
        {
            get
            {
                return new Point(X, Y);
            }
        }
        public Point TopRight
        {
            get
            {
                return new Point(X + Size.X, Y);
            }
        }
        public Point BottomLeft
        {
            get
            {
                return new Point(X, Y + Size.Y);
            }
        }
        public Point BottomRight
        {
            get
            {
                return new Point(X + Size.X, Y + Size.Y);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="r"></param>
        /// <returns>2 = r is a full subset of this object, 1 = rectangles intersect, 0 = no match</returns>
        public int Contains(Rectangle r)
        {
            bool leftTop = this.Contains(r.Position);
            bool botRight = this.Contains(new Point(r.X+r.Height,r.Y+r.Height));
            if(leftTop && botRight)
                return 2;
            else if(leftTop || botRight || this.Contains(new Point(r.X,r.Y+r.Height)) || this.Contains(new Point(r.X+r.Width,r.Y)))
                return 1;
            else 
                return 0;
        }
    }
    public static class PointExtensions
    {
        public static double RadToDeg(double rad)
        {
            return rad * (180.0 / Math.PI);
        }
        public static double DegToRad(double deg)
        {
            return deg * (Math.PI / 180.0);
        }
        private static PointF TranslateByMagnitude(float Direction, float Magnitude)
        {
            double scale_x = Math.Sin(DegToRad(Direction));
            double scale_y = -Math.Cos(DegToRad(Direction));
            double vel_x = Magnitude * scale_x;
            double vel_y = Magnitude * scale_y;
            return new PointF((float)vel_x, (float)vel_y);
        }
        public static void AddThisPoint(this PointF start, PointF end)
        {
            start.X += end.X;
            start.Y += end.Y;
        }
        public static void AddThisPoint(this Point start, PointF end)
        {
            start.X += (int)Math.Round(end.X);
            start.Y += (int)Math.Round(end.Y);
        }
        public static void TranslateByMagnitude(this Point p, float Dir, float Mag)
        {
            p.AddThisPoint(TranslateByMagnitude(Dir, Mag));
        }
        public static void TranslateByMagnitude(this PointF p, float Dir, float Mag)
        {
            p.AddThisPoint(TranslateByMagnitude(Dir, Mag));
        }
    }
    [Serializable]
    public class PointF
    {
        public PointF(float x, float y) { X = x; Y = y; }
        public PointF(Point p) { X = p.X; Y = p.Y; }
        public float X { get; set; }
        public float Y { get; set; }


        public Point ToPoint()
        {
            return new Point((int)Math.Round(X), (int)Math.Round(Y));
        }

        public double DistanceTo(PointF p)
        {
            float a = Math.Abs(p.X - X),
                   b = Math.Abs(p.Y - Y);
            return Math.Sqrt(a * a + b * b);
        }

    }
    [Serializable]
    public class Point
    {
        private int m_x = 0;
        private int m_y = 0;
        private bool valid = false;

        public Point() { }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
            valid = true;
        }
        public int X { get { return m_x; } set { m_x = value; valid = true; } }
        public int Y { get { return m_y; } set { m_y = value; valid = true; } }
        public bool Valid
        { get { return valid; } }

        public static Point operator +(Point me, Point them)
        {
            if (!them.Valid)
                throw new ArgumentException("RHS argument to Point operator+ was invalid.");
            else
                return new Point(me.X + them.X, me.Y + them.Y);
        }
        public static Point operator -(Point me, Point them)
        {
            if (!them.Valid)
                throw new ArgumentException("RHS argument to Point operator- was invalid.");
            else
                return new Point(me.X - them.X, me.Y - them.Y);
        }
        public int DistanceTo(Point p)
        {
            double a = Math.Abs(p.m_x - m_x),
                   b = Math.Abs(p.m_y - m_y);
            return (int)Math.Round(Math.Sqrt(a * a + b * b));
        }
    }
    public enum ZOrder
    {
        Background = 0,
        Foreground = 1,
        TopMost = 2
    }
    public interface IPaintCanvas
    {
        void Update(ISupplyDrawables draw, CanvasTransformer trans);
        void Draw(CanvasTransformer trans, ZOrder zorder);
    }
    public interface IPaintShipCanvas : IPaintCanvas
    {
        uint TextColour { get; set; }
        uint SelectionColour { get; set; }
    }
    public interface ISupplyDrawables
    {
        void AddMyDrawables(ref Queue<DrawableComponent> draws);
    }
}
