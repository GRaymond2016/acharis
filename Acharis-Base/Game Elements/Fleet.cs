﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Core_Elements.Network;

namespace Acharis.Game_Elements
{
    public class FleetTransfer
    {
        public Map start;
        public Map end;
        public Action result;
    }
    public class Fleet
    {
        public string Name { get; set; }
        public FleetTransfer Transfer { get; set; }
        public object shipLock = new object();
        public List<Ship> Ships { get; set; }
        public Map Location { get; set; }
        public Player Owner { get; set; }

        public void ChangeMap(Map nextMap, Action notifyChange)
        {
            Map tMap = Location;
            Location = nextMap;
            Transfer = new FleetTransfer()
            {
                start = tMap,
                end = Location,
                result = delegate
                {
                    lock (shipLock)
                    {
                        foreach (Ship s in Ships)
                        {
                            RegisterShipToMap maps = new RegisterShipToMap()
                            {
                                register = true,
                                mapId = Location.MapKey,
                                shipid = s.UniqueID,
                                playerid = Owner.PlayerKey
                            };
                            GlobalNetwork.SendToAll(maps);
                            s.EnterMap(Location);
                        }
                    }
                }
            };

            if (Owner == GalaxyController.CurrentPlayer)
            {
                MoveFleet movement = new MoveFleet()
                {
                    FleetName = Name,
                    MapId = Location.MapKey,
                    PreviousMapId = tMap.MapKey,
                    PlayerName = Owner.PlayerName
                };
                GlobalNetwork.SendToAll(movement);

                lock (shipLock)
                {
                    foreach (Ship s in Ships)
                    {
                        s.LeaveMap(tMap, delegate
                        {
                            RegisterShipToMap maps = new RegisterShipToMap()
                            {
                                register = false,
                                mapId = tMap.MapKey,
                                shipid = s.UniqueID,
                                playerid = Owner.PlayerKey
                            };
                            GlobalNetwork.SendToAll(maps);
                            tMap.RemoveShip(s);

                            if (notifyChange != null)
                                notifyChange();
                        });
                    }
                }
            }
            Owner.Update(this, tMap);
        }
    }
}
