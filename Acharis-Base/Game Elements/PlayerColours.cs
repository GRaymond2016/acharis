﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements.GalaxyMapNamespace;

namespace Acharis.Game_Elements
{
    static public class Players
    {
        static public int GetFreePlayerNumber()
        {
            List<int> free = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            using (var playl = GalaxyController.Players)
            {
                foreach (Player p in playl.Get())
                {
                    if (free.Contains(p.PlayerKey))
                        free.Remove(p.PlayerKey);
                }
            }
            if (free.Count == 0)
                return -1;
            return free.First();
        }
    }
    static public class PlayerColours
    {
        static public uint Player1 = 0xff008000; //Green
        static public uint Player2 = 0xffA52A2A; //Red
        static public uint Player3 = 0xffCD9575; //Brown
        static public uint Player4 = 0xff5D8AA8; //Blue
        static public uint Player5 = 0xffE52B50; //Pink
        static public uint Player6 = 0xff3B444B; //Black
        static public uint Player7 = 0xff87A96B; //DarkGreen
        static public uint Player8 = 0xff915C83; //Purple
        static public uint GetColourByIndex(int index)
        {
            switch (index)
            {
                case 1: return Player1;
                case 2: return Player2;
                case 3: return Player3;
                case 4: return Player4;
                case 5: return Player5;
                case 6: return Player6;
                case 7: return Player7;
                case 8: return Player8;
                default: return 0xff00ffff;
            }
        }
    }
}
