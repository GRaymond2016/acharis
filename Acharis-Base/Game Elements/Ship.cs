﻿using System;
using System.Collections.Generic;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.Combat;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Collections.Concurrent;
using System.Threading;
using Acharis.Core_Elements.Configuration;

namespace Acharis.Game_Elements
{
    public class TravelQueuePoint
    {
        public Point MovePoint;
        public Action Action;
    }
    public class Ship : InertialObject, ISupplyDrawables, IAttackingObject, IAttackableObject
    {
        private FloatingText text = new FloatingText();
        private MetaTexture baseSprite = new MetaTexture();
        private AnimatedTexture baseShield = new AnimatedTexture(DateTime.Now.Ticks);
        private MetaTexture backingDisk = new MetaTexture();

        private const string backingDiskImage = "Resources\\GUI\\BackingDisc.png";
        private const string selectionDiskImage = "Resources\\GUI\\FanningTaurus.png";

        private AnimationLoader loader = new AnimationLoader();
        private Queue<DrawableComponent> m_queuedAnimations = new Queue<DrawableComponent>();

        //Fields / Variables
        private string filename;
        private float hue = -1.0f;
        private bool locked_movement = false;
        public string Filename { get { return filename; } }
        
        //Ship Metadata
        public Player Owner { get; set; }
        public uint PlayerColor { get { return Owner.PlayerColor; } }
        public int UniqueID { get; set; }

        //Ship pathing information
        public ConcurrentQueue<TravelQueuePoint> TravelPath { get; private set; }
        public bool Dead { get; set; }
        public float FinalRotation { get; set; }
        public override Point Target
        {
            get
            {
                lock (TravelPath) 
                {
                    if (TravelPath.Count > 0)
                    {
                        TravelQueuePoint point;
                        TravelPath.TryPeek(out point);
                        return point.MovePoint;
                    }
                    else
                        return new Point();
                }
            }
            set
            {
                if (locked_movement)
                    return;

                lock (TravelPath)
                {
                    TravelQueuePoint point;
                    while (!TravelPath.IsEmpty)
                        TravelPath.TryDequeue(out point);
                    FinalRotation = -1;
                    TravelPath.Enqueue(new TravelQueuePoint(){
                        MovePoint = value
                    });
                }
            }
        }

        //Game stats
        public int SightRadius { get; set; }
        public string ShipType { get; set; }
        public string ShipName { get; set; }
        public Race Faction { get; set; }
        public ShipClass Class { get; set; }
        public Map AssociatedMap { get; set; }
        public Fleet Fleet { get; set; }
        public string MovementMarker { get; set; }

        //TODO: Remove this variable it should be integrated with weapons
        public int Damage { get; set; }

        //public List<Turret> Weapons { get; set; } TODO: Add turret functionality
        public int ShieldCount { get; set; }
        public double DamageReduction { get; set; }
        public int HealthPoints { get; set; }

        public long LastAttack { get; set; }

        private object anim_lock = new object();
        private List<AnimatedTexture> current_anims = new List<AnimatedTexture>();

        private Timer warp_timer = null;

        //Rendering data
        public bool Selected { get; set; }
        public float Scale { get; set; }
        public ShipStat ShipStat
        {
            get
            {
                ShipStat ss = new ShipStat();
                ss.hp = this.HealthPoints;
                ss.hue = (int)this.hue;
                ss.imageLoc = filename.Substring(filename.IndexOf("Resources\\"));
                ss.location = this.CurrentPosition.ToPoint();
                ss.name = this.ShipName;
                ss.ownerName = this.Owner.PlayerName;
                ss.rotation = this.Rotation; 
                ss.size = this.Size;
                ss.shield = this.ShieldCount;
                ss.shipid = this.UniqueID;
                ss.race = Faction;
                ss.sclass = Class;
                ss.fleetName = Fleet.Name;
                return ss;
            }
        }
        public LocationRequest LocationUpdate
        {
            get
            {
                LocationRequest lc = new LocationRequest();
                lc.location = curPos.ToPoint();
                lc.playerid = Owner.PlayerKey;
                lc.request = false;
                lc.rotation = Rotation;
                lc.shipid = UniqueID;
                lc.currentVelocity = CurrentVelocity;
                lc.lastMove = LastMove;
                lc.lastRotation = LastRotate;
                return lc;
            }
        }
        public RegisterShipToMap ShipRegistration
        {
            get
            {
                RegisterShipToMap shipr = new RegisterShipToMap();
                shipr.playerid = Owner.PlayerKey;
                shipr.shipid = UniqueID;
                return shipr;
            }
        }

        //Constructors and Destructors
        public Ship(ShipStat e) : this()
        {
            LastAttack = DateTime.Now.Ticks;
            HealthPoints = e.hp;
            hue = e.hue;
            filename = e.imageLoc.Substring(e.imageLoc.IndexOf("Resources\\"));
            if (e.size.Valid)
                Size = new Point(e.size.X, e.size.Y);
            ShipName = e.name;
            Rotation = e.rotation;
            ShieldCount = e.shield;
            UniqueID = e.shipid;  
            if(e.location.Valid)
                CurrentPosition = new PointF(e.location.X, e.location.Y);
            Faction = e.race;
            Class = e.sclass;

            loader.RetrieveAnimations(ShipName, filename);
        }
        public Ship()
        {
            LastAttack = DateTime.Now.Ticks;
            Dead = false;

            DamageReduction = 0.6;
            SightRadius = 1000;
            ShieldCount = 30;
            HealthPoints = 40;

            Speed = 40;

            Scale = 1.6f;
            RotationScale = 0.3f;   
            FinalRotation = -1;
            TravelPath = new ConcurrentQueue<TravelQueuePoint>();
            TargetRotation = Rotation;
            Size = new Point((int)(512 * Scale), (int)(512 * Scale)); //This should equate to texture size.
            Rotation = 0;
        }

        //Functions
        public void EnqueuePath(Point p) 
        {
            if (locked_movement)
                return;

            lock (TravelPath)
            {
                FinalRotation = -1;
                TravelPath.Enqueue(new TravelQueuePoint()
                {
                    MovePoint = p
                });
            }
        }
        public Race GetFaction()
        {
            return Faction;
        }
        public void SetShipID(int id)
        {
            UniqueID = id;
        }
        public Rectangle GetSelectionBox()
        {
            Rectangle pp = GetBoundingBox();
            int xdiff = (int)(pp.Width * 0.2), ydiff = (int)(pp.Height * 0.2);
            pp.X += xdiff;
            pp.Y += ydiff;
            pp.Width -= xdiff*2;
            pp.Height -= ydiff*2;
            return pp;
        }
        public Rectangle GetScaledBoundingBox(float scale)
        {
            float radx = Size.X * scale / 2, rady = Size.Y * scale / 2;
            return new Rectangle((int)Math.Round(curPos.X - radx),
                                (int)Math.Round(curPos.Y - rady),
                                (int)Math.Round(radx * 2),
                                (int)Math.Round(rady * 2));
        }
        public Rectangle GetBoundingBox()
        {
            return GetScaledBoundingBox(1.0f);
        }
        public bool HitsShield(Point p)
        {
            return GetScaledBoundingBox(0.6f).Contains(p);
        }
        public void LeaveMap(Map source, Action removal)
        {
            const int duration_in_secs = 20;

            PlayShipAnimation play = new PlayShipAnimation()
            {
                shipkey = UniqueID,
                playerkey = Owner.PlayerKey,
                animType = ShipAnimationTypes.WarpIdle,
                endTime = DateTime.UtcNow.AddSeconds(duration_in_secs),
                zorder = ZOrder.Foreground,
                scale = Scale
            };
            GlobalNetwork.SendToAll(play);

            AddAnimation(play.animType, TimeSpan.FromSeconds(duration_in_secs), Scale, play.zorder, removal);
        }
        public void AddAnimation(ShipAnimationTypes anim, TimeSpan endTime, float scale, ZOrder zorder, Action extra = null)
        {
            AnimatedTexture animation = new AnimatedTexture(DateTime.Now.Ticks)
            {
                Texture = loader.RetrieveFilename(anim),
                Transparency = 255,
                SizeScale = scale,
                zorder = zorder
            };
            lock (anim_lock)
            {
                current_anims.Add(animation);
            }

            if (warp_timer != null)
                warp_timer.Dispose();
            warp_timer = new Timer(delegate
            {
                lock (anim_lock)
                {
                    current_anims.Remove(animation);
                }
                if (extra != null)
                    extra();

                warp_timer.Change(Timeout.Infinite, Timeout.Infinite);
                warp_timer.Dispose();
            }, null, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(40));
        }
        public void EnterMap(Map destination)
        {
            locked_movement = false;
            Ship me = this;
            destination.AddShip(ref me);
        }
        public void RemoveAllMovementPoints()
        {
            lock (TravelPath)
            {
                while (TravelPath.Count > 0)
                {
                    TravelQueuePoint point;                      
                    if(TravelPath.TryDequeue(out point))
                    {
                        MoveShip movement = new MoveShip()
                        {
                            add = false,
                            currentmapid = AssociatedMap.MapKey,
                            shipid = UniqueID,
                            movePoint = point.MovePoint
                        };
                        GlobalNetwork.SendToAll(movement);
                    }
                }
            }
        }
        public bool MovementHit(Point p)
        {
            return GetScaledBoundingBox(0.4f * Scale).Contains(p);
        }
        private Point GetCenterPoint()
        {
            return curPos.ToPoint();
        }
        public void AddTargetDrawable(ref Queue<DrawableComponent> draws)
        {
            Rectangle origBounds = GetScaledBoundingBox(Scale);
            Rectangle bounds = GetScaledBoundingBox(0.3f).ScaleByFactor(Scale);
            bounds.X = origBounds.X - bounds.Width;
            bounds.Y = origBounds.Y - bounds.Height;
            MetaTexture tex = new MetaTexture()
            {
                Texture = "Resources\\GUI\\AttackMarker.png",
                Bounds = bounds,
                SizeScale = 1.0f
            };
            draws.Enqueue(tex);
        }
        public void AddMyDrawables(ref Queue<DrawableComponent> draws)
        {
            backingDisk.SizeScale = baseSprite.SizeScale;
            backingDisk.Bounds = GetScaledBoundingBox(1.5f);
            backingDisk.Rotation = 0;
            backingDisk.Texture = backingDiskImage;
            backingDisk.Transparency = 200;
            draws.Enqueue(backingDisk);

            if (Selected)
            {
                MetaTexture selDisk = new MetaTexture();
                selDisk.Texture = selectionDiskImage;
                selDisk.SizeScale = baseSprite.SizeScale;
                selDisk.Bounds = GetScaledBoundingBox(1.5f);
                selDisk.Rotation = 0;
                selDisk.Texture = selectionDiskImage;
                selDisk.Transparency = 200;
                draws.Enqueue(selDisk);
            }

            baseSprite.SizeScale = Scale;
            baseSprite.Texture = filename;
            baseSprite.Bounds = GetBoundingBox();
            baseSprite.Rotation = Rotation;
            draws.Enqueue(baseSprite);

            string shieldfile = loader.RetrieveFilename(ShipAnimationTypes.ShieldIdle);
            if (shieldfile != null)
            {
                baseShield.SizeScale = Scale;
                baseShield.Texture = shieldfile;
                baseShield.Bounds = GetScaledBoundingBox(1.2f);
                baseShield.Rotation = Rotation;
                baseShield.Transparency = 150;
                draws.Enqueue(baseShield);
            }

            if (Selected)
            {
                Point last = GetCenterPoint();
                uint renderColour = locked_movement ? unchecked((uint)(50 + (50 << 8) + (50 << 16) + (50 << 24))) : PlayerColor;
                foreach (TravelQueuePoint path in TravelPath)
                {
                    Point p = path.MovePoint;

                    int half = Math.Max(baseSprite.Bounds.Width / 4, baseSprite.Bounds.Height / 4);
                    MetaTexture movement = new MetaTexture();
                    movement.Texture = MovementMarker;
                    movement.Bounds = new Rectangle(p.X - half, p.Y - half, half * 2, half * 2);
                    movement.SizeScale = baseSprite.SizeScale;
                    draws.Enqueue(movement);
                    if (last != null)
                    {
                        Line ln = new Line();
                        ln.StartPoint = new Point(last.X, last.Y);
                        ln.EndPoint = new Point(p.X, p.Y);
                        ln.Color = renderColour;
                        draws.Enqueue(ln);
                    }
                    last = new Point(p.X, p.Y);
                }
            }

            text.Attachment = baseSprite;
            text.Point = AttachmentPoint.Bottom;
            text.Text = Fleet.Name;
            text.Color = Owner.PlayerColor;
            draws.Enqueue(text);

            lock (anim_lock)
            {
                foreach(AnimatedTexture tex in current_anims)
                {
                    tex.Attachment = baseSprite;
                    tex.SizeScale = Scale;
                    tex.Rotation = Rotation;
                    tex.Bounds = null;
                    draws.Enqueue(tex);
                }
            }

            //TODO: Turret rendering with obId.
        }
        public new void Move()
        {
            Moving = false;
            if (MovementHit(Target))
            {
                lock (TravelPath)
                {
                    if (TravelPath.Count > 0)
                    {
                        TravelQueuePoint point;
                        if (TravelPath.TryPeek(out point) && point.Action != null)
                        {
                            point.Action.BeginInvoke(null, null);
                        }
                        TravelPath.TryDequeue(out point);
                        if (TravelPath.Count <= 0 && FinalRotation != -1)
                            TargetRotation = FinalRotation;
                    }
                }
            }
            base.Move();
        }
        public bool RotateToTarget()
        {
            if (!Rotate())
            {
                FinalRotation = -1;
                return false;
            }
            else
                return true;
        }
        public IAttack Attack(Point target, double distance)
        {
            Random rand = new Random();
            if (target == null || distance > SightRadius ||
                DateTime.Now.Ticks - LastAttack < TimeSpan.FromSeconds(0.5).Ticks)
                return null;

            LastAttack = DateTime.Now.Ticks;
            IAttack attack = GetAttackByName(typeof(Projectile).Name, target);
            double perc = 0.05;
            attack.Target += rand.Next((int)-((perc / 2.0) * 360.0), (int)((perc / 2.0) * 360.0));
            return attack;
        }
        public IAttack GetAttackByName(string name, Point target)
        {
            Rectangle bds = GetScaledBoundingBox(0.5f);
            PointF cpoint = new PointF(GetCenterPoint());
            float rotation = (float)MovementAlgorithm.CalculateRotationBetweenPoints(cpoint, new PointF(target.X, target.Y)); ;
            Projectile pr = new Projectile()
            {
                CurrentPosition = new PointF(cpoint.X, cpoint.Y),
                Drawable = new MetaTexture()
                {
                    Bounds = bds,
                    Texture = "Resources\\Projectiles\\plasmayellowlarge.png",
                    SizeScale = Scale * 0.5f,
                    Rotation = rotation,
                    Transparency = 200
                },
                Damage = this.Damage
            };
            pr.CurrentPosition.X -= bds.Width / 2;
            pr.CurrentPosition.Y -= bds.Height / 2;
            pr.Target = rotation;
            return pr;
        }
        public double GetAttackDistance(Point attacker)
        {
            return CurrentPosition.ToPoint().DistanceTo(attacker);
        }
        public Point GetCurrentPosition()
        {
            return CurrentPosition.ToPoint();
        }
        public Point GetAttackPoint()
        {
            return GetCenterPoint();
        }
        public AttackState ReceiveDamage(Combat.AAttackType type)
        {
            if (!Dead)
            {
                type.Damage = (int)Math.Round(type.Damage * DamageReduction);

                ShieldCount -= type.Damage;
                if (ShieldCount < 0)
                {
                    HealthPoints += ShieldCount;
                    ShieldCount = 0;
                }
                else if (ShieldCount == 0)
                    HealthPoints -= type.Damage;

                if (HealthPoints <= 0)
                {
                    return AttackState.DeathAndHit;
                }
                return AttackState.Hit;
            }
            return AttackState.DeathAndPassThrough;
        }
        public void Die()
        {
        }
    }
}
