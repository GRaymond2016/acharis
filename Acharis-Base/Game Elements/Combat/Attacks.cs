﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements.Combat
{
    public enum AttackState
    {
        Hit,
        PassThrough,
        DeathAndHit,
        DeathAndPassThrough
    }
    public enum AttackType
    {
        Projectile,
        Instant
    }
    [Serializable]
    public class AAttackType
    {
        public AttackType Type;
        public string AttackFormName;
        public int Damage;
    }
    [Serializable]
    public class NormalAttackType : AAttackType
    {
    }
}
