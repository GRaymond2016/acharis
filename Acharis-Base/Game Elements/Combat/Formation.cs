﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements.Combat
{
    enum Direction
    {
        Up,
        Down,
        Right,
        Left
    }
    public interface Formation
    {
        Formation BeginFormation(Point start);
        Point GetNextPoint(Rectangle bounds, double direction);
    }
    public class BoxFormation : Formation
    {
        public BoxFormation()
        {
        }
        private BoxFormation(Point str)
        {
            Center = str;
        }
        private int ships = 0;
        private Point Center;
        private double Rotation;
        private Direction NextDirection;
        private Rectangle CurrentBounds;

        public Formation BeginFormation(Point Start)
        {
            return new BoxFormation(Start);
        }

        public Point GetNextPoint(Rectangle bounds, double direction)
        {
            Point pr = new Point(Center.X, Center.Y);
            if (ships == 0)
            {
                Rotation = direction;
                CurrentBounds = bounds;
                NextDirection = Direction.Left;
            }
            else
            {
                float mag = 0;
                float dir = (float)Rotation;
                switch (NextDirection)
                {
                    case Direction.Left: 
                        mag = bounds.Width / 2 + CurrentBounds.Width / 2; 
                        dir -= 90; 
                        NextDirection = Direction.Right;
                        break;
                    case Direction.Right: 
                        mag = bounds.Width / 2 + CurrentBounds.Width / 2; 
                        dir += 90;
                        NextDirection = Direction.Down;
                        break;
                    case Direction.Down: 
                        mag = bounds.Height / 2 + CurrentBounds.Height / 2;
                        dir += 180;
                        Center.TranslateByMagnitude(dir, mag);
                        CurrentBounds = bounds;
                        NextDirection = Direction.Left;
                        break;
                }
                if(dir > 360)
                    dir -= 360;
                if(dir < 0)
                    dir += 360;

                pr.TranslateByMagnitude(dir, mag);
            }
            ships++;
            return pr;
        }
    }
}
