﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Core_Elements.Network;
using Acharis.Core_Elements.Configuration;
using Acharis.Core_Elements;

namespace Acharis.Game_Elements.Combat
{
    [Serializable]
    public enum Race
    {
        Tchuan,
        Endlorian,
        Human,
        None
    }
    [Serializable]
    public enum ShipClass
    {
        Frigate,
        Destroyer,
        Cruiser
    }
    public class ShipFactory
    {
        private static ShipConfigurer m_config = new ShipConfigurer();
        private static AcharisConfig m_achConfig = new AcharisConfig();
        private static string movement_marker = null;
        static ShipFactory()
        {
            movement_marker = m_achConfig["PlanetMap\\MovementMarker"];
        }
        public static int GetShipCost(Race race, ShipClass sclass)
        {
            foreach (ShipTemplate ts in m_config.GetShipTemplates())
                if (race == (Race)Enum.Parse(typeof(Race), ts.race) && sclass == (ShipClass)Enum.Parse(typeof(ShipClass), ts.@class))
                    return ts.cost;
            return -1;
        }
        public static Ship GetShip(Race race, ShipClass sclass)
        {
            List<ShipTemplate> temps = m_config.GetShipTemplates();

            ShipTemplate found = null;
            ShipStat st = new ShipStat();
            foreach (ShipTemplate ts in temps)
            {
                Race r = (Race)Enum.Parse(typeof(Race), ts.race);
                ShipClass c = (ShipClass)Enum.Parse(typeof(ShipClass), ts.@class);
                if(r == race && c == sclass)
                {
                    found = ts;
                    st.imageLoc = ts.image;
                    st.name = ts.name;
                    st.race = r;
                    st.sclass = c;
                    st.hp = ts.health_points;
                    st.shield = ts.shield_points;
                    break;
                }
            }
            if(found != null)
            {
                Ship part_ship = new Ship(st);
                part_ship.MovementMarker = movement_marker;
                part_ship.Speed = found.speed;
                part_ship.SightRadius = found.sight_radius;
                part_ship.DamageReduction = found.damage_reduction;
                part_ship.RotationScale = found.rotation_speed;
                part_ship.Scale = found.scale;
                return part_ship;
            }

            return null;
        }
        public static void UpdateNonNetworkStats(ref Ship ship)
        {
            Ship sl = GetShip(ship.Faction, ship.Class);
            if (sl != null)
            {
                ship.Speed = sl.Speed;
                ship.SightRadius = sl.SightRadius;
                ship.DamageReduction = sl.DamageReduction;
                ship.RotationScale = sl.RotationScale;
                ship.Damage = 5;
                ship.Scale = sl.Scale;
                ship.MovementMarker = movement_marker;
            }
        }
    }
}
