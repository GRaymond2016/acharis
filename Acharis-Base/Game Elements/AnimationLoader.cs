﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Acharis.Core_Elements.Configuration;
using Acharis.Core_Elements;

namespace Acharis.Game_Elements
{
    internal class AnimationLoader
    {
        private ShipConfigurer config = new ShipConfigurer();
        private Dictionary<ShipAnimationTypes, string> m_animations = new Dictionary<ShipAnimationTypes, string>();
        public void RetrieveAnimations(string shipname, string filename)
        {
            string precursor = System.Environment.GetEnvironmentVariable("ACH_RES_PATH", EnvironmentVariableTarget.User) ?? "";
            var s = config.GetShipTemplates().Where(x => x.name == shipname).FirstOrDefault();

            if (s == null)
            {
                Logger.WriteLine("Could not find a valid animation template for ship: " + shipname, Logger.LogType.ERROR);
                return;
            }
            
            m_animations = s.animations;
        }
        public string RetrieveFilename(ShipAnimationTypes anim)
        {
            if (m_animations.ContainsKey(anim))
                return m_animations[anim];
            else
                return null;
        }
    }
}
