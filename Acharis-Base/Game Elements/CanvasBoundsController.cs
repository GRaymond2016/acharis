﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements
{
    public class CanvasBoundsController
    {
        public const float MaxZoom = 0.60f, MinZoom = 0.30f, ZoomRate = 0.020f;

        protected Point m_CurrentPosition = new Point(0, 0);
        protected Point m_size = new Point(0, 0);
        protected float m_zoomLevel;
        protected int m_xoffset = 0;
        protected int m_yoffset = 0;
        protected double m_yScaleFactor = 0;

        public Point Size
        {
            get { return m_size; }
            set
            {
                m_size = value;
                Recheck();
            }
        }
        public Point CurrentPosition { get { return m_CurrentPosition; } }
        public float ZoomLevel { get { return m_zoomLevel; } }

        public CanvasBoundsController(int width, int height, double yscalefactor)
        {
            m_size = new Point(width, height);
            m_yScaleFactor = yscalefactor;

            m_CurrentPosition = CenterPoint;
            m_zoomLevel = MinZoom + (MaxZoom - MinZoom) / 2;

            Recheck();
            CenterCamera();
        }

        public void CenterCamera()
        {
            m_CurrentPosition = new Point(0, (int)Math.Round((Size.Y - (Size.Y * m_yScaleFactor)) / 2.0));
        }
        public void CenterCameraOnPoint(Point target)
        {
            int x = m_xoffset / 2, y = m_yoffset / 2;
            m_CurrentPosition = new Point(target.X - x > 0 ? Math.Min(target.X - x, (Size.X - m_xoffset) - 1) : Math.Max(target.X - x, 0),
                                            target.Y - y > 0 ? Math.Min(target.Y - y, (Size.Y - m_yoffset) - 1) : Math.Max(target.Y - y, 0)); 
        }
        public void PanLeft(int count = 100)
        {
            m_CurrentPosition.X = Math.Max(m_CurrentPosition.X - (int)(count * m_zoomLevel), 0);
        }
        public void PanRight(int count = 100)
        {
            m_CurrentPosition.X = Math.Min(m_CurrentPosition.X + (int)(count * m_zoomLevel), (Size.X - m_xoffset) - 1);
        }
        public void PanUp(int count = 100)
        {
            m_CurrentPosition.Y = Math.Max(m_CurrentPosition.Y - (int)(count * m_zoomLevel), 0);
        }
        public void PanDown(int count = 100)
        {
            m_CurrentPosition.Y = Math.Min(m_CurrentPosition.Y + (int)(count * m_zoomLevel), (Size.Y - m_yoffset) - 1);
        }
        public void Translate(Point trans)
        {
            CurrentPosition.X += trans.X;
            CurrentPosition.Y += trans.Y;
        }
        private void ModifyPointByZoom(Point pnt, Point size, float oldZoom, float newZoom)
        {
            pnt.X += ((int)Math.Floor(size.X * oldZoom) - (int)Math.Floor(size.X * newZoom)) / 2;
            pnt.Y += ((int)Math.Floor(size.Y * oldZoom * m_yScaleFactor) - (int)Math.Floor(size.Y * oldZoom * m_yScaleFactor)) / 2;
        }
        public float ZoomIn(Point mousePos)
        {
            float zoomAmount = m_zoomLevel * (1.0f - ZoomRate);
            float transformation = 1.0f - (m_zoomLevel - zoomAmount);
            Point centerPoint = CenterPoint;
            Point diff = new Point(mousePos.X - centerPoint.X, mousePos.Y - centerPoint.Y);
            Point change = new Point((int)Math.Round(diff.X * transformation), (int)Math.Round(diff.Y * transformation));
            change = new Point(change.X - diff.X, change.Y - diff.Y);
            
            int nx = (int)Math.Round(m_size.X * zoomAmount);
            if (nx < Size.X * MinZoom)
            {
                if (m_zoomLevel > MinZoom)
                {
                    m_zoomLevel = MinZoom;
                    Recheck();
                }
            }
            else
            {
                float newzoom = Math.Max(zoomAmount, MinZoom);
                if (newzoom < m_zoomLevel)
                { 
                    ModifyPointByZoom(CurrentPosition, m_size, m_zoomLevel, newzoom);
                    m_zoomLevel = newzoom;
                    Translate(change);
                    Recheck();
                }
            }
            return zoomAmount;
        }
        public float ZoomOut(Point mousePos)
        {
            float zoomAmount = m_zoomLevel * (1.0f + ZoomRate);
            int nx = (int)Math.Round(m_size.X * zoomAmount);
            if (nx > Size.X)
            {
                if (m_zoomLevel < MaxZoom)
                {
                    m_zoomLevel = MaxZoom;
                    Recheck();
                }
            }
            else
            {
                float newzoom = Math.Max(zoomAmount, MinZoom);
                if (newzoom > m_zoomLevel)
                {
                    ModifyPointByZoom(CurrentPosition, m_size, m_zoomLevel, newzoom);
                    m_zoomLevel = newzoom;
                    Recheck();
                }
            }
            return zoomAmount;
        }
        private void Recheck()
        {
            if (CurrentPosition.X < 0)
                CurrentPosition.X = 0;
            if (CurrentPosition.Y < 0)
                CurrentPosition.Y = 0;

            m_xoffset = (int)Math.Floor(m_size.X * m_zoomLevel);
            m_yoffset = (int)Math.Floor(m_size.Y * m_zoomLevel * m_yScaleFactor);

            if (CurrentPosition.X + m_xoffset > Size.X)
                CurrentPosition.X = Size.X - m_xoffset;
            if (CurrentPosition.Y + m_yoffset > Size.Y)
                CurrentPosition.Y = Size.Y - m_yoffset;
        }
        public Point CenterPoint
        {
            get
            {
                return new Point(CurrentPosition.X + (int)Math.Floor(Size.X * ZoomLevel) / 2,
                                   CurrentPosition.Y + (int)Math.Floor(Size.Y * ZoomLevel * m_yScaleFactor) / 2);
            }
        }
        public Rectangle VisibleBounds
        {
            get
            {
                return new Rectangle(CurrentPosition.X,
                                        CurrentPosition.Y,
                                        (int)Math.Floor(Size.X * ZoomLevel),
                                        (int)Math.Floor(Size.Y * ZoomLevel * m_yScaleFactor));
            }
        }
    }
}
