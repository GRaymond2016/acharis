﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acharis.Game_Elements
{
    public enum DrawType
    {
        Animation,
        Texture,
        Circle,
        Box,
        Pie,
        Line,
        Text,
        Back
    }
    public enum AttachmentPoint
    {
        Top,
        Bottom,
        Center
    }
    public abstract class DrawableComponent
    {
        public DrawableComponent()
        {
            SizeScale = 1.0f;
            zorder = ZOrder.Foreground;
        }
        public DrawType Type { get; protected set; }
        public float SizeScale { get; set; } //For scaling the width / height of an image or primitive
        public ZOrder zorder { get; set; }
    }
    public class BoundedComponent : DrawableComponent
    {
        public Rectangle TransformedBounds { get; set; }
        public Rectangle Bounds { get; set; }
        public BoundedComponent Attachment { get; set; }
    }
    public class MetaTexture : BoundedComponent
    {
        public MetaTexture() 
        { 
            Type = DrawType.Texture; 
        }
        public string Texture { get; set; }
        public float Rotation { get; set; } //In degrees
        public byte Transparency { get; set; }
    }
    public class AnimatedTexture : MetaTexture
    {
        public AnimatedTexture() 
        { 
            Type = DrawType.Animation; 
        }
        public AnimatedTexture(long start) : this() { StartRender = start; }
        public long StartRender { get; set; }
        public long LastRender { get; set; }
    }
    public class Box : BoundedComponent
    {
        public Box() { Type = DrawType.Box; }
        public uint Color { get; set; }
    }
    public class CutBackground : DrawableComponent
    {
        public CutBackground() { Type = DrawType.Back; zorder = ZOrder.Background; } 
        public string Texture { get; set; }
    }
    public class FloatingText : DrawableComponent
    {
        public FloatingText() { Type = DrawType.Text; }
        public string Text { get; set; }
        public BoundedComponent Attachment { get; set; }
        public AttachmentPoint Point { get; set; }
        public uint Color { get; set; }
    }
    public class Line : DrawableComponent
    {
        public Line() { Type = DrawType.Line; }
        public Point StartPoint { get; set; }//Only needed if you are drawing a line
        public Point EndPoint { get; set; }//Only needed if you are drawing a line
        public uint Color { get; set; }
    }
    public class Circle : BoundedComponent
    {
        public Circle() { Points = 64; Type = DrawType.Circle; Width = 1; }
        public int Points { get; set; }
        public uint Color { get; set; }
        public int Width { get; set; }
    }
    public class PieShape : BoundedComponent
    {
        public PieShape() { Points = 720; Type = DrawType.Pie; AnglesToRemove = new List<Point>(); }
        public int Points { get; set; }
        public uint Color { get; set; }
        //There is a 90 degree offset on the drawn shape.
        public List<Point> AnglesToRemove { get; set; }
    }
}
