﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Microsoft.Xna.Framework;
using Acharis.Game_Elements.Combat;
using Acharis.Core_Elements;

namespace Acharis.Game_Elements
{
    public class Player
    {
        public delegate void MoneyUpdate(int newamount);
        public event MoneyUpdate money_change;

        public const int MaxShipCount = 1000;
        public object fleet_lock = new object();
        public List<Fleet> Fleets = new List<Fleet>();
        public object ship_lock = new object();
        public List<Ship> Ships = new List<Ship>();
        public uint PlayerColor { get; set; }
        public int PlayerKey { get; set; }
        public string PlayerName { get; set; }
        public Race PlayerRace { get; set; }
        public int PlayerTeam { get { return (int)PlayerRace; } }
        public int Money { get; private set; }
        public bool AutomatedPlayer { get; set; }
        public static Random r = new Random();

        private object ob_lock = new object();
        private object new_fl_lock = new object();
        private List<FleetUpdated> m_observers = new List<FleetUpdated>();
        private List<FleetAdded> m_nextFleetObservers = new List<FleetAdded>();
        public delegate void FleetAdded(Fleet context);
        public delegate void FleetUpdated(Fleet context, Map lastMap);
        public void Subscribe(FleetUpdated observer)
        {
            lock (ob_lock)
            {
                m_observers.Add(observer);
            }
        }
        public void Subscribe(FleetAdded added)
        {
            lock (new_fl_lock)
            {
                m_nextFleetObservers.Add(added);
            }
        }
        public void AddShip(Ship ship, bool assignToMap)
        {
            ship.Owner = this;
            ship.SetShipID(NextShipID());

            Fleet smallest = null;
            lock (fleet_lock)
            {
                foreach (Fleet f in Fleets)
                {
                    if (smallest == null || f.Ships.Count < smallest.Ships.Count)
                    {
                        smallest = f;
                    }
                }
            }
            ship.Fleet = smallest;
            lock (ship_lock)
            {
                smallest.Ships.Add(ship);
            }

            ship.Damage = 5;
            Point move;
            lock (ship_lock)
            {
                lock (ship.Fleet.shipLock)
                {
                    if (ship.Fleet.Ships.Count > 0)
                    {
                        Point p1 = ship.Fleet.Ships[ship.Fleet.Ships.Count - 1].Target,
                              p2 = ship.Fleet.Ships[ship.Fleet.Ships.Count - 1].CurrentPosition.ToPoint();

                        if (p1.Valid)
                            move = p1;
                        else
                            move = p2;
                    }
                    else
                        move = ship.CurrentPosition.ToPoint();
                }
                Ships.Add(ship);
            }

            if (move.X == 0 || move.Y == 0)
            {
                Random rand = new Random();
                move = new Point(rand.Next(100, 500), rand.Next(100, 500));
            }

            ship.Target = move;
            ApplyShipToGame(ship, move, this, assignToMap);
        }
        private static void ApplyShipToGame(Ship s, Point nextMove, Player pl, bool assignToMap)
        {
            Map map = s.AssociatedMap;
            GlobalNetwork.SendToAll(s.ShipStat);

            if (map != null)
            {
                map.AddShip(ref s);

                if (assignToMap)
                {
                    RegisterShipToMap sr = new RegisterShipToMap();
                    sr.mapId = s.Fleet.Location.MapKey;
                    sr.playerid = s.Owner.PlayerKey;
                    sr.register = true;
                    sr.shipid = s.UniqueID;
                    GlobalNetwork.SendToAll(sr);
                }

                MoveShip mvs = new MoveShip();
                mvs.movePoint = nextMove;
                mvs.shipid = s.UniqueID;
                mvs.currentmapid = s.AssociatedMap.MapKey;
                mvs.add = true;
                GlobalNetwork.SendToAll(mvs);
            }
            else
            {
                Logger.WriteLine("Could not find map to register ship #" + s.UniqueID + " for player " + pl.PlayerName + "(#" + pl.PlayerKey + ")", Logger.LogType.WARN);
            }
        }
        public void AddFleet(Fleet fleet)
        {
            lock (fleet_lock)
            lock (ob_lock)
            {
                Fleets.Add(fleet);
            }
            Update(fleet);
            Update(fleet, null);
        }
        private void Update(Fleet newFleet)
        {
            UpdateListeners(new_fl_lock, m_nextFleetObservers, delegate(FleetAdded update)
            {
                update(newFleet);
            });
        }
        public void Update(Fleet fleet, Map lastMap)
        {
            UpdateListeners(ob_lock, m_observers, delegate (FleetUpdated update)
            {
                update(fleet, lastMap);
            });
        }
        private void UpdateListeners<T>(object lockOb, List<T> observers, Action<T> run)
        {
            lock (lockOb)
            {
                foreach (T update in observers)
                {
                    run(update);
                }
            }
        }

        public PlayerStats PlayerStats
        {
            get
            {
                PlayerStats st = new PlayerStats()
                {
                    npc = AutomatedPlayer,
                    playerColour = PlayerColor,
                    playerName = PlayerName,
                    key = PlayerKey,
                    playerrace = PlayerRace
                };
                return st;
            }
        }
        public PlayerIncome PlayerIncome
        {
            get
            {
                PlayerIncome income = new PlayerIncome()
                {
                    money = Money,
                    playerid = PlayerKey
                };
                return income;
            }
        }
        public Player()
        {
            AutomatedPlayer = false;
            PlayerColor = (uint)(r.Next(0, 255) + r.Next(0, 255) << 8 + r.Next(0, 255) << 16 + 255 << 24);
            Money = 0;
        }
        public void SetMoney(int amount)
        {
            Money = amount;
            if(money_change != null)
                money_change(Money);
        }
        public bool OwnsShip(Ship s)
        {
            return OwnsShip(s.UniqueID);
        }
        public bool OwnsShip(int shipid)
        {
            return shipid >= PlayerKey * MaxShipCount && shipid < (PlayerKey + 1) * MaxShipCount;
        }
        public void ClaimShip(Ship s)
        {
            s.Owner = this;
            lock (s.Owner.ship_lock)
            {
                s.Owner.Ships.Add(s);
            }

            if (s.UniqueID < PlayerKey * Player.MaxShipCount || s.UniqueID >= (PlayerKey+1) * Player.MaxShipCount)
            {
                s.UniqueID = NextShipID();
                Logger.WriteLine("Renumbering ship #" + s.UniqueID + " required but feature not yet implemented.", Logger.LogType.WARN);
                //TODO: Add ship renumbering if needed
            }
        }
        public int NextShipID()
        {
            lock (ship_lock)
            {
                if (Ships.Count == 0)
                    return PlayerKey * Player.MaxShipCount;
                else
                {
                    Ship last = Ships[Ships.Count - 1];
                    return last.UniqueID + 1;
                }
            }
        }
        public void ProcessMessage(object s, Socket st)
        {
            if (s.GetType() == typeof(string))
            {
                string tempS = (string)s;
                if (tempS == "player_request")
                {
                    var tb = new PlayerStats()
                    {
                        playerName = PlayerName,
                        playerColour = PlayerColor,
                        key = PlayerKey
                    };
                    st.Send(tb.ToByteArray());
                }
            }
        }
        public void Cleanup()
        {
            lock (ship_lock)
            {
                foreach (Ship s in Ships)
                {
                    if (GlobalNetwork.Server && s.AssociatedMap != null)
                    {
                        RegisterShipToMap sr = new RegisterShipToMap();
                        sr.mapId = s.AssociatedMap.MapKey;
                        sr.playerid = s.Owner.PlayerKey;
                        sr.register = false;
                        sr.shipid = s.UniqueID;
                        GlobalNetwork.SendToAll(sr);
                    }
                    if (s.AssociatedMap != null)
                    {
                        s.AssociatedMap.RemoveShip(s);
                    }
                }
            }

            if(GlobalNetwork.Server)
            {
                PlayerStats t = PlayerStats;
                t.add = false;
                GlobalNetwork.SendToAll(t);
            }
        }
    }
}
