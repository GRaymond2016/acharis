### What is this repository for? ###

This is a simple 2D galaxy control game based in original IP from a long time ago. The concept is you have fleets which fly around capturing worlds and then you can build units / buildings on the world to build your empire. You can go down into the worlds to combat against other players fleets too.

### How do I set it up? ###

Configuration:

Environmental Variables (User):

ACH_BUILD_PATH : Used by the server, this folder should contain multiple folders, with four numbers seperated by . and (can have peripheral text like 'Build' too) it will take the latest version from this folder and use it as the distribution.

ACH_ERROR_PATH : Used by both the server and Acharis.exe, along with Error Reporter. This value will have logs written to it, and if a crash occurs it will look for the error reporter here (log the critical error here). This should be pointed at the Acharis-ErrorReporter.exe deployment site. The path should contain the exe, the logger will use the path with the exe in it. But this allows you to define a new EXE name for the reporter.

ACH_RES_PATH : Used by both Server and Acharis.exe this should point to a folder that contains a 'Resources' folder that contains the resources that the application needs. It should be pointed at '\Dropbox\AM Work\Acharis 2D\Releases\'.

e.g.

ACH_BUILD_PATH : E:\Acharis_Builds\Acharis_2D\

ACH_ERROR_PATH : C:\Users\Lord Nyson\Acharis\

ACH_RES_PATH : E:\Dropbox\AM Work\Acharis 2D\Releases\ <- Contains a folder called Resources