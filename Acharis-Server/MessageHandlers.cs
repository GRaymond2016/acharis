﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acharis.Game_Elements;
using System.Reflection;
using System.Threading;
using System.Net.Sockets;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Security.Cryptography;
using Acharis.Core_Elements;
using System.IO;
using Microsoft.Win32;
using Acharis;
using Acharis.Core_Elements.Network;
using Acharis.Core_Elements.Configuration;
using Acharis.Game_Elements.Combat;
using Acharis.Game_Elements.Development;

namespace Acharis_Base.Core_Elements.Network
{
    class MessageHandlers : CoreMessageHandlers
    {
        private static Dictionary<string, DateTime> recs = new Dictionary<string, DateTime>();
        private static Race rc = Race.None;
        public MessageHandlers()
            : base()
        {
        }
        private static void HandleDisconnect(ConnectionStates connected, string address, Exception error)
        {

        }
        private static Race GetNewRace()
        {
            switch (rc)
            {
                case Race.None: rc = Race.Human; break;
                case Race.Human: rc = Race.Endlorian; break;
                case Race.Endlorian: rc = Race.Tchuan; break;
                case Race.Tchuan: rc = Race.Human; break;
            }
            return rc;
        }
        protected override bool Handler(PlayerStats stat, Client sender)
        {
            if (!CredSupplied && GlobalNetwork.Server)
            {
                sender.Send(new ClosingConnection("Credentials not supplied."));
                sender.Cleanup();
                return false;
            }
            if (stat.playerName == GalaxyController.CurrentPlayer.PlayerName)
            {
                Logger.WriteLine("Something attempted to add / relay the server player init message.", Logger.LogType.WARN);
                return false;
            }
            if (sender.ContainsPlayer(stat.playerName))
            {
                Player player = null;
                using (LockingWrapper<List<Player>> playl = GalaxyController.Players)
                {
                    player = playl.Get().Where(x => x.PlayerName == stat.playerName).FirstOrDefault();
                }
                if (player == null)
                {
                    Logger.WriteLine("Could not find specified player: " + stat.playerName + ". Cannot assign starting ships to player.", Logger.LogType.WARN);
                    return false;
                }

                player.PlayerRace = stat.playerrace;

                using (var mp = GalaxyController.Planets)
                {
                    int map = -1;
                    foreach (var val in mp.Get())
                    {
                        if (stat.playerrace == val.AssociatedPlanet.RacialHome)
                        {
                            map = val.AssociatedMap;
                            break;
                        }
                    }
                    if (map == -1)
                    {
                        Logger.WriteLine("No racial home found for " + player.PlayerRace.ToString(), Logger.LogType.CRITICAL);
                        return false;
                    }
                    Map realMap = null;
                    using (var maps = GalaxyController.Maps)
                    {
                        var dict = maps.Get();
                        if (!dict.ContainsKey(map))
                        {
                            Logger.WriteLine("No racial home found for " + player.PlayerRace.ToString() + " with mapId of " + map.ToString(), Logger.LogType.CRITICAL);
                            return false;
                        }
                        realMap = maps.Get()[map];
                    }

                    GlobalNetwork.SendToAll(player.PlayerStats);
                    for (int i = 0; i < 3; ++i)
                    {
                        Fleet fleet = new Fleet()
                        {
                            Location = realMap,
                            Name = player.PlayerName + " - Fleet #" + (i + 1),
                            Owner = player,
                            Ships = new List<Ship>()
                        };
                        player.AddFleet(fleet);

                        AddFleet afl = new AddFleet()
                        {
                            FleetName = fleet.Name,
                            MapId = fleet.Location.MapKey,
                            PlayerName = fleet.Owner.PlayerName
                        };
                        GlobalNetwork.SendToAll(afl);

                        Ship template = ShipFactory.GetShip(stat.playerrace, ShipClass.Frigate);
                        if (template != null)
                        {
                            ShipFactory.UpdateNonNetworkStats(ref template);
                            template.AssociatedMap = realMap;
                            template.CurrentPosition = new PointF(realMap.Bounds.Center);
                            player.AddShip(template, true);
                        };
                    }
                }
                return false;
            }
            if (stat.add)
            {
                Player player = new Player();
                player.PlayerName = stat.playerName;
                player.PlayerKey = Players.GetFreePlayerNumber();
                player.PlayerColor = PlayerColours.GetColourByIndex(player.PlayerKey);
                using (var playl = GalaxyController.Players)
                {
                    playl.Get().Add(player);
                }
                sender.AddPlayer(player.PlayerName);
                sender.Send(player.PlayerStats);
            }
            else
            {
                using (var playl = GalaxyController.Players)
                {
                    var coll = playl.Get().Where(x => x.PlayerKey == stat.key);
                    foreach (var pl in coll)
                        pl.Cleanup();
                }
            }
            return false;
        }
        protected bool Handler(BuyMeAShip ship, Client sender)
        {
            Player player = null;
            using(var playlock = GalaxyController.Players)
            {
                foreach(Player p in playlock.Get())
                {
                    if(p.PlayerKey == ship.playerid)
                        player = p;
                }
            }
            int cost = ShipFactory.GetShipCost(player.PlayerRace, ship.ship_class);
            if (cost == -1)
            {
                Logger.WriteLine("Could not get a cost for ship of race " + player.PlayerRace + " and type " + ship.ship_class, Logger.LogType.ERROR);  
                return false;
            }
            if (cost > player.Money)
            {
                Logger.WriteLine("Cost is greater than the money this player has, cannot purchase ship. Player: " + player.PlayerName + " Ship Class: " + ship.ship_class, Logger.LogType.INFO);
                return false;
            }
            using(var planlock = GalaxyController.Planets)
            {
                foreach(GalaxyPlanetBinding p in planlock.Get())
                {
                    if(p.AssociatedPlanet.RacialHome == player.PlayerRace)
                    {
                        Map m = null;
                        using(var maplock = GalaxyController.Maps)
                        {
                            var dict = maplock.Get();
                            if (!dict.ContainsKey(p.AssociatedMap))
                            {
                                Logger.WriteLine("Could not add ship, associated ship was not found.", Logger.LogType.WARN);
                                break;
                            }
                            m = dict[p.AssociatedMap];
                        }
                        debug_AddingShips.AddShip(m, player.PlayerRace, ship.ship_class, player);
                        break;
                    }
                }
            }
            player.SetMoney(player.Money - cost);
            GlobalNetwork.SendToAll(player.PlayerIncome);
            return false;
        }
        protected override bool Handler(MoveFleet fleet, Client sender)
        {
            Map mp;
            using (var maps = GalaxyController.Maps)
            {
                var dict = maps.Get();
                if (!dict.ContainsKey(fleet.MapId))
                {
                    Logger.WriteLine("Warning, the map specified when attempting to add a fleet was invalid. The fleet may not be viewable.", Logger.LogType.WARN);
                    return false;
                }
                mp = dict[fleet.MapId];
            }
            using (var plays = GalaxyController.Players)
            {
                Player p = plays.Get().Where(x => x.PlayerName == fleet.PlayerName).FirstOrDefault();
                if (p != null)
                {
                    lock (p.fleet_lock)
                    {
                        Fleet f = p.Fleets.Where(x => x.Name == fleet.FleetName).FirstOrDefault();
                        if (f != null)
                            f.ChangeMap(mp, null);
                    }
                }
                else
                {
                    Logger.WriteLine("Error moving fleet for player, player \"" + fleet.PlayerName + "\" does not exist.", Logger.LogType.ERROR);
                }
            }
            return false;
        }
        protected bool Handler(CheckUser cu, Client sender)
        {
            if (!cu.accept && !GlobalNetwork.Server)
            {
                sender.DisconnectClient("Incorrect credentials were supplied");
                return false;
            }
            if (!GlobalNetwork.Server) 
                return false; // Not a server AND not accepting

            //Disconnect any current players of that name
            using (var playl = GalaxyController.Players)
            {
                if (playl.Get().Count(x => x.PlayerName == cu.username) > 0)
                {
                    cu.accept = false;
                    sender.Send(cu);
                    return false;
                }
            }

            RegistryKey ru = Registry.CurrentUser.CreateSubKey("Software\\Acharis\\Users");
            object ret = ru.GetValue(cu.username, null);

            cu.accept = false;
            if (ret != null)
                if (MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(ret.ToString())).SequenceEqual(cu.password))
                    cu.accept = true;
            cu.accept = true; //No authentication for the moment
            sender.Send(cu);

            CredSupplied = true;

            //Complete process by passing them the galaxy map
            var gmap = GalaxyConfigurer.GetMap(GalaxyController.GalaxyMap);
            sender.Send(new GalaxyTransfer(gmap.XMLNode, InitSync.Files));

            return false;
        }
        protected bool Handler(RequestInitData cu, Client sender)
        {
            if (!GlobalNetwork.Server) return false;

            List<MoveShip> lms = new List<MoveShip>();
            using (var playl = GalaxyController.Players)
            {
                foreach (Player player in playl.Get())
                {
                    if (!sender.ContainsPlayer(player.PlayerName))
                    {
                        sender.Send(player.PlayerStats);
                        lock (player.ship_lock)
                        {
                            foreach (Ship shipFP in player.Ships)
                            {
                                sender.Send(shipFP.ShipStat);
                                for (int i = 0; i < shipFP.TravelPath.Count; i++)
                                {
                                    MoveShip ms = new MoveShip();
                                    ms.movePoint = shipFP.TravelPath.ElementAt(i).MovePoint;
                                    ms.shipid = shipFP.UniqueID;
                                    ms.add = true;
                                    lms.Add(ms);
                                }
                            }
                        }
                    }
                }
            }
            using (var mapl = GalaxyController.Maps)
            {
                foreach (Map m in mapl.Get().Values)
                {
                    List<RegisterShipToMap> rt = m.GetShipRegistrations();
                    for (int i = 0; i < rt.Count; i++)
                    {
                        RegisterShipToMap sr = rt[i];
                        sr.mapId = m.MapKey;
                        sr.register = true;
                        sender.Send(sr);
                        foreach (MoveShip sh in lms.Where(x => x.shipid == sr.shipid))
                        {
                            sh.currentmapid = m.MapKey;
                            sender.Send(sh);
                        }
                    }
                    m.SendStateToClient(sender); 
                }
            }
            return false;
        }
    }
}
