﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Acharis.Core_Elements;
using Acharis_ErrorReportingBase;
using System.Diagnostics;
using System.IO;

namespace Acharis_Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            //We set the current directory so that we don't use redundant Resources and DLLs, it essentially runs off the same as Acharis-XNA
            //Except that it is in a Server subfolder
#if DEBUG
            string defaultdir = @"..\..\..\Acharis-XNA\Acharis-XNA\bin\x86\Debug";
            if (Directory.Exists(defaultdir))
                System.IO.Directory.SetCurrentDirectory(defaultdir);
#endif

            string dir = System.IO.Directory.GetCurrentDirectory();
            Logger.WriteLine("Current Directory - " + dir, Logger.LogType.INFO);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            if (args.IsTerminating)
                ExceptionThrown((Exception)args.ExceptionObject);
        }
        private static void ExceptionThrown(Exception args)
        {
            string exe = System.Environment.GetEnvironmentVariable("ACH_ERROR_PATH", EnvironmentVariableTarget.Process) ?? "";
            exe += "Acharis Error Reporter.exe";
            string id = ExceptionLoader.WriteException(args);
            try
            {
                Process.Start(exe, id);
            }
            catch (Exception ex)
            {
                Logger.WriteLine("Could not launch " + exe + "." +
                    Environment.NewLine + ex.ToString() +
                    Environment.NewLine + "An exception was not recorded: " + args.ToString(), Logger.LogType.CRITICAL);
            }
        }
    }
}
