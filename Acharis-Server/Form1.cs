﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Acharis.Core_Elements;
using Acharis.Core_Elements.Configuration;
using Acharis.Core_Elements.Network;
using Acharis_Base.Core_Elements.Network;
using achg = Acharis.Game_Elements;
using Acharis.Game_Elements.GalaxyMapNamespace;
using System.Threading;
using Acharis.Game_Elements.Development;
using Acharis.Game_Elements.Combat;

namespace Acharis_Server
{
    public partial class Form1 : Form
    {
        enum ButtonState
        {
            Start,
            Stop
        }
        private object m_lock = new object();
        private ButtonState currentState = ButtonState.Stop;
        private ServerSchedule m_schedule = new ServerSchedule();

        public void GlobalNetwork_m_client_connection_event(ConnectionStates state, string address, Exception err)
        {
            if (state == ConnectionStates.Connected)
            {
                string hostname = GlobalNetwork.ResolveHostname(address);

                if (player_box.InvokeRequired)
                    player_box.BeginInvoke(new Action(delegate { AddPlayer(hostname); }));
                else
                    AddPlayer(hostname);
            }
        }
        public void AddPlayer(string item)
        {
            if (item == null)
                return;

            int slind = player_box.SelectedIndex;
            player_box.Items.Add(item);
            if (slind < 0)
                player_box.SelectedIndex = 0;

            listBox1.Items.Add(item);
        }
        public void RemovePlayer(string item)
        {
            if (item == null)
                return;

            string host = GlobalNetwork.ResolveHostname(item);
            player_box.Items.Remove(host);
            listBox1.Items.Remove(host);
        }
        public void GlobalNetwork_m_client_disconnection_event(ConnectionStates state, string address, Exception err)
        {
            if (state == ConnectionStates.Ended)
            {
                if (player_box.InvokeRequired)
                    player_box.BeginInvoke(new Action(delegate { RemovePlayer(address); }));
                else
                    RemovePlayer(address);
            }
        }
        public Form1()
        {
            m_schedule.Initialize();

            InitializeComponent();
            GalaxyConfigurer config = new GalaxyConfigurer();
            foreach (string s in GalaxyConfigurer.GetMapNames())
                listBox2.Items.Add(s);

            race_box.Items.Add("Tchuan");
            race_box.Items.Add("Endlorian");
            race_box.Items.Add("Human");

            class_box.Items.Add("Frigate");
            class_box.Items.Add("Destroyer");
            class_box.Items.Add("Cruiser");

            GlobalNetwork.MakeDedicatedServer();
            GlobalNetwork.m_client_connection_event += new NetworkBase.NetworkEvent(GlobalNetwork_m_client_connection_event);
            GlobalNetwork.m_disconnection_event += new NetworkBase.NetworkEvent(GlobalNetwork_m_client_disconnection_event);

            FormClosing += new FormClosingEventHandler(Form1_FormClosing);

            if (listBox2.Items.Count > 0)
            {
                listBox2.Text = listBox2.Items[0] as string;
                button2_Click(null, null);
                button1_Click(null, null);
            }

            using (var maps = GalaxyController.Maps)
            {
                foreach (Map m in maps.Get().Values)
                    map_box.Items.Add(m.MapName);
            }
            race_box.SelectedIndex = 0;
            class_box.SelectedIndex = 0;
            map_box.SelectedIndex = 0;

#if DEBUG
            if(Screen.AllScreens.Length > 1)
            {
                try
                {
                    Rectangle bounds = Screen.AllScreens.Where(x => x != Screen.PrimaryScreen).First().WorkingArea;
                    StartPosition = FormStartPosition.Manual;
                    Location = new Point(bounds.Location.X + bounds.Width / 2, bounds.Location.Y + bounds.Height / 2);
                }
                catch { }
            }
#endif
            AddPlayer(GlobalNetwork.LocalName, 0, Race.None);
            AddPlayer("Neutral", 11, Race.None);
            Client.Handler = new MessageHandlers();
        }
        private void AddPlayer(string name, int key, Race race)
        {
            achg.Player me = new achg.Player();
            me.PlayerName = name;
            me.PlayerKey = key;
            me.PlayerRace = race;
            using (var playl = GalaxyController.Players)
            {
                playl.Get().Add(me);
            }
            GlobalNetwork.SendToAll(GalaxyController.CurrentPlayer.PlayerStats);
            AddPlayer(name);
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs args)
        {
            GlobalNetwork.Stop();
            m_schedule.End();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox2.Text != "")
            {
                InitSync synchronization = new InitSync();
                synchronization.LoadMap(listBox2.Text);
                mapLabel.Text = "Current Map: " + listBox2.Text;
                button1.Enabled = true;

                listBox2.Items.Clear();
                foreach (string s in GalaxyConfigurer.GetMapNames())
                    listBox2.Items.Add(s);

                map_box.Items.Clear();
                using (var maps = Acharis.Game_Elements.GalaxyMapNamespace.GalaxyController.Maps)
                {
                    foreach (Map mp in maps.Get().Values)
                        map_box.Items.Add(mp.MapName);
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            lock (m_lock)
            {
                if (currentState == ButtonState.Start)
                {
                    GlobalNetwork.Stop();
                    m_schedule.End();
                    button1.Text = "Start Server";
                    currentState = ButtonState.Stop;
                }
                else
                {
                    GlobalNetwork.StartServer();
                    m_schedule.Begin();
                    button1.Text = "Stop Server";
                    currentState = ButtonState.Start;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Map mp = null;
            using (var Maps = GalaxyController.Maps)
            {
                mp = Maps.Get().Values.Where(x => x.MapName == (string)map_box.Text).FirstOrDefault();
            }
            if (mp == null)
            {
                MessageBox.Show("Could not find map specified '" + map_box.Text + "'");
                return;
            }
            Acharis.Game_Elements.Player pl = null;
            using(var Players = GalaxyController.Players)
            {
                pl = Players.Get().Where(x => x.PlayerName == (string) player_box.Text).FirstOrDefault();
            }
            if (pl == null)
            {
                MessageBox.Show("Could not find the player specified '" + player_box.Text + "'");
                return;
            }
            debug_AddingShips.AddShip(mp, (Race)Enum.ToObject(typeof(Race), race_box.SelectedIndex), (ShipClass)Enum.ToObject(typeof(ShipClass), class_box.SelectedIndex), pl);
        }
    }
}
