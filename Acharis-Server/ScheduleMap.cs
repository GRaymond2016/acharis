﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Acharis.Core_Elements.Network;
using Acharis.Game_Elements.GalaxyMapNamespace;
using Acharis.Game_Elements;
using Acharis.Game_Elements.Development;
using Acharis.Core_Elements;
using Acharis.Game_Elements.Combat;

namespace Acharis_Server
{
    class ServerSchedule : FunctionMap
    {
        public ServerSchedule()
        {
            GalaxyController.map_added += new GalaxyController.MapAdded(add_map_handler);
        }
        private void add_map_handler(Map m)
        {
            m.planet_change += new Map.PlanetControlChange(planet_change);
        }
        private void planet_change(Race controlling, int mapid)
        {
            UpdatePlanet pl = new UpdatePlanet()
            {
                MapId = mapid,
                ControllingRace = controlling
            };
            GlobalNetwork.SendToAll(pl);
        }

        [Interval(10000)]
        public void RequestPositions()
        {
            LocationRequest req = new LocationRequest();
            using (var mapl = GalaxyController.Maps)
            {
                foreach (Map mb in mapl.Get().Values)
                {
                    List<LocationRequest> lcUp = mb.GetShipLocations();
                    lcUp.ForEach(
                        delegate(LocationRequest r) 
                        { 
                            GlobalNetwork.SendToAll(r); 
                        });
                }
            }
        }

        [Interval(60000)]
        public void UpdateMoney()
        {
            Dictionary<Race, int> incomeFromPlanets = new Dictionary<Race, int>();
            using (var mapl = GalaxyController.Maps)
            {
                foreach (Map mp in mapl.Get().Values)
                {
                    if (!incomeFromPlanets.ContainsKey(mp.ControllingRace))
                        incomeFromPlanets.Add(mp.ControllingRace, 0);
                    incomeFromPlanets[mp.ControllingRace] += mp.ControlIncome;
                }
            }
            using (var pla = GalaxyController.Players)
            {
                foreach (Player p in pla.Get())
                {
                    if (incomeFromPlanets.ContainsKey(p.PlayerRace))
                    {
                        p.SetMoney(p.Money + incomeFromPlanets[p.PlayerRace]);
                        PlayerIncome pl = p.PlayerIncome;
                        GlobalNetwork.SendToAll(pl);
                    }
                }
            }
        }

        class CreateShip
        {
            ShipClass shipClass;
            Map fromMap;
        }

        private Dictionary<Race, List<Map>> CountSpawnType(ShipClass sclass)
        {
            Dictionary<Race, List<Map>> spawnCount = new Dictionary<Race, List<Map>>();
            spawnCount.Add(Race.Human, new List<Map>());
            spawnCount.Add(Race.Endlorian, new List<Map>());
            spawnCount.Add(Race.Tchuan, new List<Map>());

            using (var mapl = GalaxyController.Maps)
            {
                foreach (Map mp in mapl.Get().Values)
                {
                    if (mp.SpawnType == sclass && spawnCount.ContainsKey(mp.ControllingRace))
                        spawnCount[mp.ControllingRace].Add(mp);
                }
            }
            return spawnCount;
        }
        private void AddShip(Race rc, ShipClass sc, Map mp, Player pl)
        {
            Ship template = ShipFactory.GetShip(rc, sc);
            if (template != null)
            {
                ShipFactory.UpdateNonNetworkStats(ref template);
                template.Owner = pl;
                template.AssociatedMap = mp;
                pl.AddShip(template, true);
            };
        }
        private void SpawnClasses(ShipClass sclass)
        {
            Dictionary<Race, List<Map>> spawn = CountSpawnType(sclass);
            Dictionary<Race, List<Player>> players = new Dictionary<Race, List<Player>>();
            players.Add(Race.Human, new List<Player>());
            players.Add(Race.Endlorian, new List<Player>());
            players.Add(Race.Tchuan, new List<Player>());

            using (var pla = GalaxyController.Players)
            {
                foreach (Player p in pla.Get())
                {
                    if(players.ContainsKey(p.PlayerRace))
                        players[p.PlayerRace].Add(p);
                }
            }

            foreach (Race r in Enum.GetValues(typeof(Race)))
            {
                if (r == Race.None)
                    continue;
                if (spawn[r].Count > 0 && players[r].Count > 0)
                {
                    int perPlayer = spawn[r].Count / players[r].Count;
                    Random rand = new Random();
                    foreach (Player p in players[r])
                    {
                        for (int i = 0; i < perPlayer; ++i)
                        {
                            AddShip(r, sclass, spawn[r].First(), p);
                            spawn[r].RemoveAt(0);
                        }
                    }
                    //Get rid of the remainder randomly
                    foreach (Map mp in spawn[r])
                    {
                        int index = rand.Next(players[r].Count);
                        AddShip(r, sclass, mp, players[r][index]);
                    }
                }
            }
        }
        [Interval(120000)]
        public void SpawnFrigates()
        {
            SpawnClasses(ShipClass.Frigate);
        }
        [Interval(160000)]
        public void SpawnDestroyers()
        {
            SpawnClasses(ShipClass.Destroyer);
        }
        [Interval(240000)]
        public void SpawnCruisers()
        {
            SpawnClasses(ShipClass.Cruiser);
        }

        [Interval(1000)]
        public void UpdateObjectives()
        {
            using (var mapl = GalaxyController.Maps)
            {
                foreach (Map mp in mapl.Get().Values)
                {
                    mp.UpdateObjectives();
                }
            }
        }

        [Interval(10)]
        public void InternalUpdateShipPositionAndObjectives()
        {
            var @lock = GalaxyController.Maps;
            var internal_buffer = @lock.Get();
            for (int i = 0; i < internal_buffer.Count; ++i)
            {
                Map mp = internal_buffer[i];
                @lock.Unlock();
                mp.Update();
                @lock.Lock();
            }
            @lock.Unlock();
        }

        [Interval(12000)]
        public void AddAIShip()
        {
            using (LockingWrapper<List<Player>> player_lock = GalaxyController.Players)
            using (var mml = GalaxyController.Maps)
            {
                var dict = mml.Get();
                if (dict.Count == 0)
                    return;
                Random r = new Random();
                int index = (int)r.Next(dict.Count);
                Map map = dict.ElementAt(index).Value;
                foreach (Player pl in player_lock.Get())
                {
                    if (pl.AutomatedPlayer)
                    {
                        debug_AddingShips.AddAIShip(map, pl);
                    }
                }
            }
        }
    }
}
